
//--------------------------------------------------------------------------------------
// Global variables
//--------------------------------------------------------------------------------------

float4x4 g_matWorlds[4];          // ワールド変換行列配列
float4x4 g_matView;               // 3D空間のView行列
float4x4 g_matProj;               // 3D空間のprojection行列
const int iBlendNum = 4;          // ブレンドする配列の数
texture  g_MeshTexture;           // メッシュのテクスチャ
texture  g_MeshNormalMap;         // 法線マッピングテクスチャ

float4 g_ambientLight;            // アンビエント光
float3 g_lightSource;             // 光源の座標

//--------------------------------------------------------------------------------------
// Texture samplers
//--------------------------------------------------------------------------------------
sampler MeshTextureSampler = 
sampler_state
{
  Texture = <g_MeshTexture>;
    MipFilter = LINEAR;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
};

sampler NormalMapSampler =
sampler_state
{
    Texture = <g_MeshNormalMap>;
    MipFilter = LINEAR;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
};


//--------------------------------------------------------------------------------------
// Vertex shader output structure
//--------------------------------------------------------------------------------------
struct VS_OUTPUT
{
    float4 Position  : POSITION;  // 頂点座標
    float2 TextureUV : TEXCOORD0; // メッシュ,法線マップ用UV座標
    float4 Diffuse   : COLOR0;    // 頂点カラー
    float3 Light     : NORMAL;    // ライトベクトルの接空間
};

// 接空間行列の逆行列を算出
float4x4 InvTangentMatrix(float3 tangent, float3 binormal, float3 normal)
{
  float4x4 mat = { float4(tangent, 0.0f), float4(binormal, 0.0f), float4(normal, 0.0f), { 0.0f, 0.0f, 0.0f, 1.0f } };
  return transpose(mat); // 転置
}

//--------------------------------------------------------------------------------------
// This shader computes standard transform and lighting
//--------------------------------------------------------------------------------------
VS_OUTPUT VertexBlendingVS(float4 vPos      : POSITION,
                           float4 vWeight   : BLENDWEIGHT,
                           float2 vTexCoord : TEXCOORD0,
                           float3 vNormal   : NORMAL,
                           float3 vTangent  : TANGENT,
                           float3 vBinormal : BINORMAL)
{
  VS_OUTPUT Output;
	float    weight[4] = (float[4])vWeight;  // 重みをfloatに分割
	float    totalBlendWeight = 0.0f;        // 最後の行列に掛けられる重み
	float4x4 matCombWorld = 0.0f;            // 合成ワールド変換行列
  float4   v4Diffuse;
  float4   worldPosition;
  float3   vLight;

  //接空間行列の逆行列を算出
  float4x4 invTangentMat = InvTangentMatrix(vTangent, vBinormal, vNormal);

	// ワールド変換行列をブレンド
	for (int i = 0; i < iBlendNum - 1; i++) {
    totalBlendWeight += weight[i]; // 最後の重みをここで計算しておく
    matCombWorld += g_matWorlds[i] * weight[i];
  }
	// 最後の重みを足し算
  matCombWorld += g_matWorlds[iBlendNum - 1] * (1.0f - totalBlendWeight);

	// ワールド変換後の頂点座標を計算
  worldPosition = mul(vPos, matCombWorld);

  //頂点から光源への単位ベクトルを計算
  vLight = normalize(g_lightSource - worldPosition);

  //ライトベクトルを接空間に移動
  vLight = mul(vLight, invTangentMat);

  //色の計算
  v4Diffuse.r = g_ambientLight.r; // + max(0.0f, dotProduct) * 0.5f;
  v4Diffuse.g = g_ambientLight.g; // + max(0.0f, dotProduct) * 0.5f;
  v4Diffuse.b = g_ambientLight.b; // + max(0.0f, dotProduct) * 0.5f;
  v4Diffuse.a = 1.0f;

  //出力
  Output.Position  = mul(worldPosition, mul(g_matView, g_matProj));
  Output.Light     = vLight;
  Output.Diffuse   = v4Diffuse;
  Output.TextureUV = vTexCoord;

    return Output;    
}


//--------------------------------------------------------------------------------------
// Pixel shader output structure
//--------------------------------------------------------------------------------------
struct PS_OUTPUT
{
    float4 RGBColor : COLOR0;  // Pixel color    
};


//--------------------------------------------------------------------------------------
// テクスチャ付きPS
//--------------------------------------------------------------------------------------
PS_OUTPUT VertexBlendingPS(VS_OUTPUT In)
{ 
  PS_OUTPUT Output;
  float4 v4Diffuse;
  float3 normalMap;
  float dotProduct;

  //法線マップからUV座標上の情報を読み取る
  normalMap = tex2D(NormalMapSampler, In.TextureUV);
  normalMap = normalize(normalMap);

  //内積から反射光を求める
  dotProduct = dot(In.Light, normalMap);

  //反射を色に適応
  v4Diffuse.r = In.Diffuse + max(0.0f, dotProduct) * 0.8f;
  v4Diffuse.g = In.Diffuse + max(0.0f, dotProduct) * 0.8f;
  v4Diffuse.b = In.Diffuse + max(0.0f, dotProduct) * 0.8f;
  v4Diffuse.a = 1.0f;

  // Lookup mesh texture
  Output.RGBColor = tex2D(MeshTextureSampler, In.TextureUV) * v4Diffuse;
  //Output.RGBColor = v4Diffuse;

  return Output;
}


//--------------------------------------------------------------------------------------
// Renders scene to render target
//--------------------------------------------------------------------------------------
technique VertexBlendingTechnique
{
    pass P0 //本体を描画
    {      
		VertexShader = compile vs_3_0 VertexBlendingVS();
		PixelShader = compile ps_3_0 VertexBlendingPS();
    }
}

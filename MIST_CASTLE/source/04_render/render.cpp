
#include <d3dx9.h>
#include "render.h"
#include "../00_application/lib/myDx9Lib.h"
#include "../00_application/app/app.h"
#include "../01_data/stageEdit/stageEdit.h"
#include "../05_ingame/camera/camera.h"
#include "../05_ingame/object/object.h"
#include "../05_ingame/debug/debug.h"
#include "../05_ingame/fog/fog.h"
#include "../05_ingame/userInterface/userInterface.h"
#include "../06_outGame/title/title.h"
#include "../06_outGame/opening/opening.h"
#include "../06_outGame/gameover/gameover.h"
#include "../06_outGame/clear/clear.h"

/// <summary>
/// 描画
/// </summary>
void Render()
{
  //描画開始
  if(BeginRender())
  {
    //カメラ情報のセット
    CAMERA cam = GetCamera();
    SetupLights();  //光源処理
    SetupMatrices(cam.vEyePt_,
                  cam.vLookatPt_,
                  cam.vUpVec_);//頂点行列処理

    //ゲームの描画
    switch(GetGameState())
    {
    case GAME_STATE_TITLE:
      RenderTitle();
      break;
    case GAME_STATE_OPENING:
      RenderOpening();
      break;
    case GAME_STATE_INGAME:
      RenderObject();
      RenderMist();
      RenderUserInterface();
#ifdef DEBUG_MODE
      //あたり判定のためUIの後にデバッグ情報を描画
      if(GetGameMode() == GAMEMODE_DEBUG_HIT_JUDGE)
        RenderDebug();
      if(GetGameMode() == GAMEMODE_MAP_CREATE)
        RenderEdit();
#endif
      break;
    case GAME_STATE_GAMEOVER:
      RenderGameover();
      break;
    case GAME_STATE_CLEAR:
      RenderClear();
      break;
    }
  }
  //描画終了
  EndRender();
}
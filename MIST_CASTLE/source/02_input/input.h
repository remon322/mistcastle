#pragma once
#include "../00_application/lib/myDx9Lib.h"

//入力情報の更新
void InputUpdate();

//キーボードの情報の取得
KEY_BOARD GetKeyBoard();
//キーボードを押した瞬間の情報の取得
KEY_BOARD GetTrgInKeyBoard();
//キーボードを離した瞬間の情報の取得
KEY_BOARD GetTrgOutKeyBoard();

//マウスの情報の取得
MOUSE GetMouse();
//マウスボタンを押した瞬間の情報の取得
MOUSE GetTrgInMouseButton();
//マウスボタンを離した瞬間の情報の取得
MOUSE GetTrgOutMouseButton();

//ゲームパッドの情報の取得
JOY_STICK GetJoyStickButton();
//ゲームパッドボタンを押した瞬間の情報の取得
JOY_STICK GetTrgInJoyStickButton();
//ゲームパッドボタンを離した瞬間の情報の取得
JOY_STICK GetTrgOutJoyStickButton();
#pragma once
#include <d3dx9.h>

//カメラ情報
struct CAMERA
{
  float theta_;          //カメラの向きの上下 0~PAI
  float phi_;            //カメラの向きの左右 0~2*PAI
  D3DXVECTOR3 vEyePt_;   //カメラ座標
  D3DXVECTOR3 vLookatPt_;//注視するものの座標
  D3DXVECTOR3 vUpVec_;   //カレントの上方
};

//カメラの初期化
void InitCamera();

//カメラ情報の更新
void UpdateCamera();

//カメラ情報の取得
CAMERA GetCamera();

// カメラ情報アドレスの取得
CAMERA* GetCameraAddress();
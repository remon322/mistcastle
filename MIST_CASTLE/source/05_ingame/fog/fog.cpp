#include "../camera/camera.h"
#include "../object/player/player.h"
#include "../object/skeleton/skeleton.h"
#include "../../00_application/app/app.h"
#include "../../00_application/lib/myDx9Lib.h"
#include "../../00_application/function/easing.h"
#include "../../01_data/sound/dslib.h"
#include "../../01_data/sound/loadSound.h"
#include "../../01_data/status/loadStatus.h"
#include "../../01_data/texture/loadTexture.h"

//霧の濃さ変更にかかる時間
#define FOG_CHANGE_TIME (60)
//霧の濃さレベル指数
#define NEAR (0)
#define FAR  (1)

//霧の濃さレベルの列挙
enum FOG_LEVEL
{
  FOG_LEVEL_1,
  FOG_LEVEL_2,
  FOG_LEVEL_3,
  FOG_LEVEL_4,
  FOG_LEVEL_5,
  FOG_LEVEL_MAX,
};

//霧の距離テーブル 開始距離,終点距離の順
//GetStatus関数によって上書きされる
float g_fogLevelTable[FOG_LEVEL_MAX][2] = 
{
  {50.0f, 100.0f},
  {45.0f, 90.0f},
  {40.0f, 80.0f},
  {35.0f, 75.0f},
  {200.0f, 300.0f},
};

static FOG_LEVEL g_nowFogLevel;    //現在の霧の濃度レベル
static FOG_LEVEL g_prevFogLevel;   //前の霧の濃度レベル
static float g_easingAdvanceTime;  //濃度切り替え時のイージングの進行時間
static float g_prevFogDistance[2]; //前のフォグの値   [0]:near [1]:far
static float g_nowFogDistance[2];  //現在のフォグの値 [0]:near [1]:far
static D3DXVECTOR3 g_mistPos[30];    //霧ビルボードエフェクトの座標

/// <summary>
/// フォグの情報を取得
/// </summary>
/// <param name="g_fogLevel">(out)霧の濃度レベル</param>
/// <param name="nearFog">(out)霧の現在の開始距離</param>
/// <param name="farFog">(out)霧の現在の終了距離</param>
/// <returns>霧の進行時間</returns>
int GetFogInfomation(int* fogLevel, float* nearFog, float* farFog)
{
  if(fogLevel != NULL)
    *fogLevel = g_nowFogLevel;
  if(nearFog != NULL)
    *nearFog = g_nowFogDistance[NEAR];
  if(farFog != NULL)
    *farFog = g_nowFogDistance[FAR];
  return g_easingAdvanceTime;
}

/// <summary>
/// フォグの初期化
/// </summary>
void InitFog()
{
  //霧レベルの初期化
  g_nowFogLevel = FOG_LEVEL_3;
  g_prevFogLevel = FOG_LEVEL_3;
  g_easingAdvanceTime = 0;

  //霧の距離詳細読み込み
  STATUS st = *GetStatus();
  g_fogLevelTable[0][NEAR] = st.fogNear1_;
  g_fogLevelTable[1][NEAR] = st.fogNear2_;
  g_fogLevelTable[2][NEAR] = st.fogNear3_;
  g_fogLevelTable[3][NEAR] = st.fogNear4_;
  g_fogLevelTable[4][NEAR] = st.fogNear5_;
  g_fogLevelTable[0][FAR] = st.fogFar1_;
  g_fogLevelTable[1][FAR] = st.fogFar2_;
  g_fogLevelTable[2][FAR] = st.fogFar3_;
  g_fogLevelTable[3][FAR] = st.fogFar4_;
  g_fogLevelTable[4][FAR] = st.fogFar5_;

  //霧ビルボードの初期化
  for(int i = 0; i < 30; i++)
  {
    g_mistPos[i] = D3DXVECTOR3(1000 + rand() % 5000,
                             10,
                             rand() % 5000);
  }
}

/// <summary>
/// 霧の濃さを設定する
/// </summary>
/// <param name="level">設定したい霧濃度レベル</param>
void SetFogLevel(int level)
{
  //イージングの進行時間のリセット
  g_easingAdvanceTime = 0;
  //濃さレベルの設定
  g_prevFogLevel = g_nowFogLevel;
  g_nowFogLevel = (FOG_LEVEL)(CLUMP(0, level, FOG_LEVEL_MAX - 1));
  //現在の濃さを保存
  g_prevFogDistance[NEAR] = g_nowFogDistance[NEAR];
  g_prevFogDistance[FAR]  = g_nowFogDistance[FAR];
  //スケルトンの当たり判定を更新
  SkeletonLinkFogLevel();
}

/// <summary>
/// 霧の濃さを1段階上げる
/// </summary>
void UpFogLevel()
{
  //イージングの進行時間のリセット
  g_easingAdvanceTime = 0;
  //濃さレベルの設定
  g_prevFogLevel = g_nowFogLevel;
  g_nowFogLevel = (FOG_LEVEL)(CLUMP(0, g_nowFogLevel + 1, FOG_LEVEL_MAX - 1));
  //現在の濃さを保存
  g_prevFogDistance[NEAR] = g_nowFogDistance[NEAR];
  g_prevFogDistance[FAR]  = g_nowFogDistance[FAR];
  //スケルトンの当たり判定を更新
  SkeletonLinkFogLevel();
  //濃さが上がる音を鳴らす
  DSoundStop(g_soundHandles[FOG_UP_SE]);
  DSoundPlay(g_soundHandles[FOG_UP_SE], false);
}

/// <summary>
/// 霧の濃さを1段階下げる
/// </summary>
void DownFogLevel()
{
  //イージングの進行時間のリセット
  g_easingAdvanceTime = 0;
  //濃さレベルの設定
  g_prevFogLevel = g_nowFogLevel;
  g_nowFogLevel = (FOG_LEVEL)(CLUMP(0, g_nowFogLevel - 1, FOG_LEVEL_MAX - 1));
  //現在の濃さを保存
  g_prevFogDistance[NEAR] = g_nowFogDistance[NEAR];
  g_prevFogDistance[FAR]  = g_nowFogDistance[FAR];
  //スケルトンの当たり判定を更新
  SkeletonLinkFogLevel();
  //濃さが下がる音を鳴らす
  DSoundStop(g_soundHandles[FOG_DOWN_SE]);
  DSoundPlay(g_soundHandles[FOG_DOWN_SE], false);
}

/// <summary>
/// 霧の更新
/// </summary>
void UpdateFog()
{
  //イージングを用いて前と今の濃度レベルの切り替えを補間する
  g_nowFogDistance[0] = JudgeAndEasing(IN_QUAD, g_easingAdvanceTime,
                                     g_prevFogDistance[NEAR],
                                     g_fogLevelTable[g_nowFogLevel][NEAR] - g_prevFogDistance[NEAR],
                                     FOG_CHANGE_TIME);
  g_nowFogDistance[1] = JudgeAndEasing(IN_QUAD, g_easingAdvanceTime,
                                     g_prevFogDistance[FAR],
                                     g_fogLevelTable[g_nowFogLevel][FAR] - g_prevFogDistance[FAR],
                                     FOG_CHANGE_TIME);
  //進行時間の更新
  g_easingAdvanceTime += GetSpeedRate();
  //霧の距離をライブラリ側に設定する
  SetFogDistance(g_nowFogDistance[NEAR], g_nowFogDistance[FAR]);

  D3DXVECTOR3 cameraPos = GetCamera().vEyePt_;
  //カメラからの距離で並び替え
  for(int i = 1; i <= 25; i++)
  {
    for(int j = 0; j < i; j++)
    {
      if(cameraPos - g_mistPos[i] < cameraPos - g_mistPos[j])
      {
        D3DXVECTOR3 temp = g_mistPos[j];
        g_mistPos[j] = g_mistPos[i];
        g_mistPos[i] = temp;
      }
    }
  }
}

/// <summary>
/// ビルボードによって霧エフェクトを出す
/// </summary>
void RenderMist()
{
  D3DXVECTOR3 plPos = GetPlayerData().pos_;
  for(int i = 0; i < 25; i++)
  {
    //RenderBillboard(g_mistPos[i], GetCamera().vEyePt_, g_textureHandles[FOG], CLUMP(0, GetDistance(g_mistPos[i], plPos), 255));
  }
}
/*------------------------------------------------------------------
ファイル名 = effect.cpp
動作       = エフェクトの管理
制作者     = 高柳俊一
/------------------------------------------------------------------*/

#include "effect.h"
#include "../../00_application/app/app.h"
#include "../../00_application/lib/myDx9Lib.h"
#include "../../01_data/texture/loadTexture.h"
#include "../../05_inGame/camera/camera.h"

static EFFECT* g_headEffectPointer = NULL;//エフェクト先頭ポインタ
static EFFECT* g_endEffectPointer = NULL;//エフェクト最後尾ポインタ

void UpdateParameter(PARAMETER* parameter, int startFrame, int keepFrame_);
EFFECT* DeleteEffect(EFFECT* nowEffectPointer);

/// <summary>
/// エフェクトの生成
/// </summary>
/// <param name="x">座標の推移情報</param>
/// <param name="y">座標の推移情報</param>
/// <param name="z_">座標の推移情報</param>
/// <param name="alpha_">不透明度の推移情報</param>
/// <param name="angle_">回転角度の推移情報</param>
/// <param name="size_">拡大倍率の推移情報</param>
/// <param name="picture_">画像識別子</param>
/// <param name="time">エフェクト継続時間</param>
void EffectGenerate(PARAMETER x, PARAMETER y, PARAMETER z, PARAMETER alpha, PARAMETER angle, PARAMETER size, TEXTURE_NAME picture_, int time)
{
  //メモリの確保と前後のリンク
  if(g_endEffectPointer == NULL)
  {
    g_headEffectPointer = (EFFECT*)malloc(sizeof(EFFECT));
    memset(g_headEffectPointer, 0, sizeof(EFFECT));
    g_headEffectPointer->prev_ = NULL;
    g_headEffectPointer->next_ = NULL;
    g_endEffectPointer = g_headEffectPointer;
  }
  else
  {
    g_endEffectPointer->next_ = (EFFECT*)malloc(sizeof(EFFECT));
    memset(g_endEffectPointer->next_, 0, sizeof(EFFECT));
    g_endEffectPointer->next_->prev_ = g_endEffectPointer;
    g_endEffectPointer = g_endEffectPointer->next_;
  }

  //情報の代入
  g_endEffectPointer->x_ = x;
  g_endEffectPointer->y_ = y;
  g_endEffectPointer->z_ = z;
  g_endEffectPointer->alpha_ = alpha;
  g_endEffectPointer->angle_ = angle;
  g_endEffectPointer->size_ = size;
  g_endEffectPointer->picture_ = picture_;
  g_endEffectPointer->advanceFrame_ = 0.0f;
  g_endEffectPointer->keepFrame_ = time;
}

/// <summary>
/// エフェクトの更新
/// </summary>
void UpdateEffect()
{
  EFFECT* nowEffectPointer = g_headEffectPointer;
  while(nowEffectPointer != NULL)
  {
    //維持時間を超えたらエフェクト削除
    if(nowEffectPointer->keepFrame_ <= nowEffectPointer->advanceFrame_)
    {
      nowEffectPointer = DeleteEffect(nowEffectPointer);
      continue;
    }

    int advanceFrame_ = nowEffectPointer->advanceFrame_;
    int keepFrame_ = nowEffectPointer->keepFrame_;

    UpdateParameter(&nowEffectPointer->x_, advanceFrame_, keepFrame_);
    UpdateParameter(&nowEffectPointer->y_, advanceFrame_, keepFrame_);
    UpdateParameter(&nowEffectPointer->alpha_, advanceFrame_, keepFrame_);
    UpdateParameter(&nowEffectPointer->angle_, advanceFrame_, keepFrame_);
    UpdateParameter(&nowEffectPointer->size_, advanceFrame_, keepFrame_);

    nowEffectPointer = nowEffectPointer->next_;
  }
}

/// <summary>
/// 指定エフェクトの消去とメモリ開放
/// </summary>
/// <param name="nowEffectPointer">消去したいエフェクトのポンタ</param>
/// <returns>リストの次のエフェクトのポインタ</returns>
EFFECT* DeleteEffect(EFFECT* nowEffectPointer)
{
  //双方向リストの前後のつなぎ ■:現在のポインタ □:それ以外のポインタ
  if(nowEffectPointer != g_headEffectPointer)
  {
    if(nowEffectPointer != g_endEffectPointer)
    {
      //□■□□ or □□■□ の場合のリンク
      nowEffectPointer->prev_->next_ = nowEffectPointer->next_;
      nowEffectPointer->next_->prev_ = nowEffectPointer->prev_;
    }
    else
    {
      // □□□■ の場合のリンク
      g_endEffectPointer = nowEffectPointer->prev_;
      nowEffectPointer->prev_->next_ = NULL;
    }
  }
  else
  {
    if(nowEffectPointer != g_endEffectPointer)
    {
      //■□□□ の場合のリンク
      g_headEffectPointer = nowEffectPointer->next_;
      nowEffectPointer->next_->prev_ = NULL;
    }
    else
    {
      //前後に何もない場合のリンク
      g_headEffectPointer = NULL;
      g_endEffectPointer = NULL;
    }
  }

  //次のポインタを返す
  EFFECT* doFreePointer = nowEffectPointer;
  EFFECT* nextPointer = nowEffectPointer->next_;
  if(doFreePointer != NULL)
  {
    free(doFreePointer);
  }
  return nextPointer;
}

/// <summary>
/// エフェクトの描画
/// </summary>
void RenderEffect()
{
  EFFECT* nowEffectPointer = g_headEffectPointer;
  while(nowEffectPointer != NULL)
  {
    //RenderBillboard();
    nowEffectPointer = nowEffectPointer->next_;
  }
}

/// <summary>
/// エフェクトの全消去
/// </summary>
void AllDeleteEffect()
{
  //先頭ポインタを取得
  EFFECT* nowEffectPointer = g_headEffectPointer;
  //NULLになるまで回す
  while(nowEffectPointer)
  {
    EFFECT* nextPointer = nowEffectPointer->next_;
    free(nowEffectPointer);
    nowEffectPointer = nextPointer;
  }
  //先頭と最後尾をNULLにする
  g_headEffectPointer = NULL;
  g_endEffectPointer = NULL;
}

/// <summary>
/// パラメータの値の更新
/// </summary>
/// <param name="parameter">(in.out)更新させるパラメータ</param>
/// <param name="advanceFrame">パラメータの進行時間</param>
/// <param name="keepFrame">パラメータの継続時間</param>
void UpdateParameter(PARAMETER* parameter, int advanceFrame, int keepFrame)
{
  //遅延がある場合は何もしない
  if(advanceFrame - parameter->delayFrame < 0) { return; }
  //値の更新
  parameter->value = JudgeAndEasing(parameter->easing,
                                    advanceFrame - parameter->delayFrame,
                                    parameter->start,
                                    parameter->stop - parameter->start,
                                    keepFrame);
}
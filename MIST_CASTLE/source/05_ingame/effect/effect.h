#pragma once

#include "../../01_data/texture/loadTexture.h"
#include "../../00_application/function/easing.h"

//変化するパラメーター値用構造体
struct PARAMETER
{
  EASING easing;     //使用するイージングの識別子
  double start;      //開始時点の値(座標やスケールなど)
  double stop;       //終了時点の値(座標やスケールなど)
  int    delayFrame; //遅延時間
  double value;      //現在の値
};

//エフェクト管理用構造体
struct EFFECT
{
  TEXTURE_NAME picture_;      //画像識別子
  float        advanceFrame_; //進行時間
  float        keepFrame_;    //終了までの継続時間
  PARAMETER    x_;            //座標
  PARAMETER    y_;            //座標
  PARAMETER    z_;            //座標
  PARAMETER    alpha_;        //アルファ値
  PARAMETER    angle_;        //回転角度
  PARAMETER    size_;         //拡大倍率
  int          animeSpeed_;   //アニメーション速度
  EFFECT*      prev_;         //前のリンク
  EFFECT*      next_;         //次のリンク
};

//エフェクトの生成
void EffectGenerate(PARAMETER x, PARAMETER y, PARAMETER z, PARAMETER alpha, PARAMETER angle, PARAMETER size, TEXTURE_NAME picture_, int time);
//エフェクトの更新
void UpdateEffect();
//エフェクトの描画
void RenderEffect();
//エフェクトの全消去
void AllDeleteEffect();
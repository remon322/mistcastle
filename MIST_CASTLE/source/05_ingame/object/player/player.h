#pragma once
#include "../object.h"

#define KNOCK_BACK_MAX_TIME       (30) //ノックバック時間

//プレイヤーの状態
enum E_PLAYER_STATE
{
  PL_STATE_IDLE,
  PL_STATE_MOVE,
  PL_STATE_KNOCK_BACK,
  PL_STATE_DEATH,
};

//プレイヤーの詳細
struct PLAYER_OPTION
{
  E_PLAYER_STATE prevState_;                  //プレイヤーの状態
  E_PLAYER_STATE state_;                      //プレイヤーの状態
  float          nextYawAngle_;               //XZ平面の目標方向
  float          velocity_;                   //移動時のスカラー量
  float          gravityVecY_;                //重力
  bool           isGroundHit_;                //地面の接触 接触時true
  bool           isSlow_;                     //プレイヤーの鈍足状態異常
  int            invincibleTime_;             //プレイヤーの無敵時間
  int            healthMax_;                  //プレイヤーの最大体力
  int            health_;                     //プレイヤーの体力
  int            prevHealth_;                 //プレイヤーのひとつ前の体力
  int            magicPoint_;                 //プレイヤーの魔法力
  float          stamina_;                    //プレイヤーのスタミナ
  int            staminaMax_;                 //プレイヤーの最大スタミナ
  float          staminaInfinityTime_;        //スタミナが減らない時間
  float          staminaRegenerationCoolTime_;//スタミナが回復するまでのクールタイム
  int            healthEnergy_;               //体力回復エネルギーの量
  float          deathAdvanceTime_;           //死亡時の進行時間
  float          knockBackAdvanceTime_;       //ノックバックの進行時間
  D3DXVECTOR3    knockBackDir_;               //ノックバック方向
  int            treasureGetNum_;             //宝箱の取得数
};

//プレイヤーのデータの初期化
//オプションはメモリ確保済みとする
void InitPlayer(OBJ_DATA* player);

//プレイヤーの更新
void UpdatePlayer(OBJ_DATA* pPlayer);

//プレイヤーの情報の取得
OBJ_DATA GetPlayerData();

//プレイヤーのデータアドレスの取得
OBJ_DATA* GetPlayerAddress();
#include "../object.h"
#include "../player/player.h"
#include "../skeleton/skeleton.h"
#include "../launcher/arrow.h"
#include "../../hitJudge/hitJudge.h"
#include "../../../00_application/app/app.h"
#include "../../../01_data/sound/dslib.h"
#include "../../../01_data/sound/loadSound.h"
#include "../../../01_data/model/loadModel.h"
#include "../../../01_data/status/loadStatus.h"

//矢の発射
void ArrowDamageToTarget(OBJ_DATA* arrow, OBJ_DATA* target);

//矢の削除
void DeleteArrow(OBJ_DATA* arrow);

/// <summary>
/// 矢の生成
/// </summary>
/// <param name="pos_">矢オブジェクトを生成する座標</param>
/// <param name="moveVec">矢オブジェクトの移動ベクトル</param>
/// <returns>生成された矢オブジェクトポインタ</returns>
OBJ_DATA* GenerateArrow(D3DXVECTOR3 pos, D3DXVECTOR3 moveVec)
{
  //メモリの確保
  OBJ_DATA* arrow = AddObjData(OBJ_KIND_ARROW, pos, g_meshScaleTable[ARROW], 0);
  ARROW_OPTION* arrowOp = (ARROW_OPTION*)arrow->option_;
  arrow->yawAngle_ = atan2f(moveVec.z, moveVec.x) - PI / 2.0f;
  arrowOp->moveVec = moveVec;
  //あたり判定の設定
  AddHitListSphere(BODY, 0, INFINITY_FRAME, arrow, D3DXVECTOR3(0.0f, 0.0f, 0.0f), 5.0f, NULL);
  AddHitListSphere(ATTACK, OBJ_KIND_PLAYER | OBJ_KIND_SKELETON, INFINITY_FRAME, arrow, D3DXVECTOR3(0.0f, 0.0f, 0.0f), 10.0f, ArrowDamageToTarget);
  return arrow;
}

/// <summary>
/// 矢の更新
/// </summary>
/// <param name="arrow">更新したい矢オブジェクト</param>
void UpdateArrow(OBJ_DATA* arrow)
{
  if(MoveObject(arrow, ((ARROW_OPTION*)arrow->option_)->moveVec * GetSpeedRate()))
  {
    DeleteArrow(arrow);
  }
}

/// <summary>
/// 矢の削除
/// </summary>
/// <param name="arrow">削除したい矢オブジェクトポインタ</param>
void DeleteArrow(OBJ_DATA* arrow)
{
  DeleteListObjData(arrow);
}

/// <summary>
/// 矢による攻撃
/// </summary>
/// <param name="arrow">攻撃する矢オブジェクト</param>
/// <param name="target">攻撃する対象オブジェクト</param>
void ArrowDamageToTarget(OBJ_DATA* arrow, OBJ_DATA* target)
{
  if(target->kind_ == OBJ_KIND_PLAYER)
  {
    PLAYER_OPTION* playerOp = (PLAYER_OPTION*)target->option_;
    //プレイヤーが無敵時間でなければHPを減らして無敵にする
    if(playerOp->invincibleTime_ <= 0)
    {
      //HPを減らして無敵時間を設定
      playerOp->health_ -= GetStatus()->arrowDamage_;
      playerOp->knockBackAdvanceTime_ = KNOCK_BACK_MAX_TIME;
      playerOp->invincibleTime_ = GetStatus()->playerInvincibleTime_;
    }
  }
  else if(target->kind_ == OBJ_KIND_SKELETON)
  {
    //スケルトンを死亡させる
    DeleteSkeleton(target);
  }
}
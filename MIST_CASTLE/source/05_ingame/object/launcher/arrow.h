#pragma once
#include <d3dx9.h>
#include "../object.h"

//矢の詳細情報
struct ARROW_OPTION
{
  D3DXVECTOR3 moveVec; //移動ベクトル
  float velocity;      //移動速度
};

//矢の生成
OBJ_DATA* GenerateArrow(D3DXVECTOR3 pos, D3DXVECTOR3 moveVec);

//矢の更新
void UpdateArrow(OBJ_DATA* arrow);
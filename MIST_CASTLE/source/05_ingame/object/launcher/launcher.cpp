#include "../object.h"
#include "../player/player.h"
#include "../launcher/launcher.h"
#include "../launcher/arrow.h"
#include "../../hitJudge/hitJudge.h"
#include "../../../00_application/app/app.h"
#include "../../../01_data/model/loadModel.h"
#include "../../../01_data/status/loadStatus.h"

//矢の発射
void ShotArrow(OBJ_DATA* launcher, OBJ_DATA* target);

/// <summary>
/// 発射装置の生成
/// </summary>
/// <param name="pos_">発射装置を生成する座標</param>
/// <param name="yawAngle_">発射装置の回転角度</param>
/// <returns>追加されたオブジェクトポインタ</returns>
OBJ_DATA* GenerateLauncher(D3DXVECTOR3 pos, float yawAngle)
{
  //メモリの確保
  OBJ_DATA* launcher = AddObjData(OBJ_KIND_LAUNCHER, pos, g_meshScaleTable[LAUNCHER], 0);
  //あたり判定の設定
  AddHitListSphere(BODY, 0, INFINITY_FRAME, launcher, D3DXVECTOR3(0.0f, 0.0f, 0.0f), 3.0f, NULL);
  AddHitListFanPillar(SEARCH, OBJ_KIND_PLAYER, INFINITY_FRAME, launcher, D3DXVECTOR3(0.0f, 0.0f, 0.0f), 10.0f, 1000.0f, PI / 90.0f, D3DXVECTOR3(cos(yawAngle + PI / 2.0f), 0.0f, sin(yawAngle + PI / 2.0f)), ShotArrow);
  return launcher; 
}

/// <summary>
/// 発射装置の更新
/// </summary>
/// <param name="launcher">更新したい発射装置オブジェクト</param>
void UpdateLauncher(OBJ_DATA* launcher)
{
  LAUNCHER_OPTION* launcherOp = (LAUNCHER_OPTION*)launcher->option_;
  //クールタイムの更新
  launcherOp->shotCoolTime -= GetSpeedRate();;
}

/// <summary>
/// 矢の発射
/// </summary>
/// <param name="launcher">矢を発射する発射装置オブジェクト</param>
/// <param name="target">ターゲットオブジェクト(ダミー)</param>
void ShotArrow(OBJ_DATA* launcher, OBJ_DATA* target)
{
  LAUNCHER_OPTION* launcherOp = (LAUNCHER_OPTION*)launcher->option_;
  if(launcherOp->shotCoolTime <= 0)
  {
    //矢を打ち出す
    GenerateArrow(launcher->pos_, D3DXVECTOR3(cos(launcher->yawAngle_ + PI / 2.0f) * 30.0f, 0.0f, sin(launcher->yawAngle_ + PI / 2.0f) * 30.0f));
    //クールタイムの設定
    launcherOp->shotCoolTime = GetStatus()->launcherCoolTime_;
  }
}
#pragma once
#include <d3dx9.h>
#include "../object.h"

//発射装置の詳細
struct LAUNCHER_OPTION
{
  int shotCoolTime; //再発射までの時間
};

//発射装置の生成
OBJ_DATA* GenerateLauncher(D3DXVECTOR3 pos, float yawAngle);

//発射装置の更新
void UpdateLauncher(OBJ_DATA* launcher);
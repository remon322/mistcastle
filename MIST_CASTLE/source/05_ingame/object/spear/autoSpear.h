#pragma once
#include "../object.h"

//自動槍の詳細
struct AUTO_SPEAR_OPTION
{
  int prepareTime_;//行動管理時間
  float spearPosY_;//槍部分の描画座標
  bool attackFlag_;//攻撃時あたり判定が重複しないためのflag
};

//槍の生成
OBJ_DATA* GenerateAutoSpear(D3DXVECTOR3 pos);

//槍の更新
void UpdateAutoSpear(OBJ_DATA* autoSpear);
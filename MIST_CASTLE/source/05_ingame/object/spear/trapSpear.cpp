#include "../object.h"
#include "../player/player.h"
#include "../skeleton/skeleton.h"
#include "../spear/trapSpear.h"
#include "../../hitJudge/hitJudge.h"
#include "../../../00_application/app/app.h"
#include "../../../00_application/function/easing.h"
#include "../../../01_data/sound/dslib.h"
#include "../../../01_data/sound/loadSound.h"
#include "../../../01_data/model/loadModel.h"
#include "../../../01_data/status/loadStatus.h"

#define SPEAR_WAIT_POS_Y   (-80.0f) //非攻撃時の槍のY座標
#define SPEAR_ATTACK_POS_Y (0.0f) //攻撃時の槍のY座標
#define SPEAR_STORE_TIME   (90)     //格納にかかる時間

//罠槍の撃ち出し準備
void preparationSpear(OBJ_DATA* spear, OBJ_DATA* target);

//罠槍が当たったときの処理
void TrapSpearDamageToPlayer(OBJ_DATA* spear, OBJ_DATA* target);

//罠槍の削除
void DeleteTrapSpear(OBJ_DATA* arrow);

/// <summary>
/// 罠槍の生成
/// </summary>
/// <param name="pos_">罠槍の生成座標</param>
/// <returns>追加されたオブジェクトアドレス</returns>
OBJ_DATA* GenerateTrapSpear(D3DXVECTOR3 pos)
{
  //メモリの確保
  OBJ_DATA* trapSpear = AddObjData(OBJ_KIND_TRAP_SPEAR, pos, g_meshScaleTable[TRAP_SPEAR], 0);
  //あたり判定の設定
  AddHitListBox(SEARCH, OBJ_KIND_PLAYER | OBJ_KIND_SKELETON, INFINITY_FRAME, trapSpear, D3DXVECTOR3(0.0f, 0.0f, 0.0f), 100.0f, 100.0f, 100.0f, preparationSpear);
  return trapSpear;
}

/// <summary>
/// 罠槍の更新
/// </summary>
/// <param name="trapSpear">罠槍オブジェクト</param>
void UpdateTrapSpear(OBJ_DATA* trapSpear)
{
  TRAP_SPEAR_OPTION* spearOp = (TRAP_SPEAR_OPTION*)trapSpear->option_;
  spearOp->delayPenetrateTime_ -= GetSpeedRate();

  //準備時間が0になったら槍ををとび出させる
  if(spearOp->delayPenetrateTime_ <= 0 && spearOp->attackFlag_ == 0)
  {
    AddHitListBox(ATTACK, OBJ_KIND_PLAYER | OBJ_KIND_SKELETON, 10, trapSpear, D3DXVECTOR3(0.0f, 0.0f, 0.0f), 125.0f, 100.0f, 125.0f, TrapSpearDamageToPlayer);
    spearOp->attackFlag_ = 1;
  }

 //槍本体の高さ指定
  //槍飛び出し時のY座標設定
  if(spearOp->delayPenetrateTime_ >= -SPEAR_STORE_TIME)
    spearOp->spearPosY_ = JudgeAndEasing(OUT_QUINT,
                                        -spearOp->delayPenetrateTime_,
                                        SPEAR_WAIT_POS_Y,
                                        SPEAR_ATTACK_POS_Y - SPEAR_WAIT_POS_Y,
                                        30);
  //槍収納時のY座標設定
  if(spearOp->delayPenetrateTime_ < -SPEAR_STORE_TIME)
    spearOp->spearPosY_ = JudgeAndEasing(LINEAR,
                                        -spearOp->delayPenetrateTime_ - SPEAR_STORE_TIME,
                                        SPEAR_ATTACK_POS_Y,
                                        SPEAR_WAIT_POS_Y - SPEAR_ATTACK_POS_Y,
                                        30);
}

/// <summary>
/// 罠槍の削除
/// </summary>
/// <param name="trapSpear"></param>
void DeleteTrapSpear(OBJ_DATA* trapSpear)
{
  DeleteListObjData(trapSpear);
}

/// <summary>
/// 罠槍の撃ち出し準備
/// </summary>
/// <param name="spear">罠槍オブジェクト</param>
/// <param name="target">対象(ダミー)</param>
void preparationSpear(OBJ_DATA* spear, OBJ_DATA* target)
{
  TRAP_SPEAR_OPTION* spearOp = (TRAP_SPEAR_OPTION*)spear->option_;
  if(spearOp->delayPenetrateTime_ < -SPEAR_STORE_TIME - 30)
  {
    //準備時間を設定する
    spearOp->delayPenetrateTime_ = GetStatus()->trapSpearDelayTime_;
    spearOp->attackFlag_ = 0;
  }
}

/// <summary>
/// 罠槍が当たったときの処理
/// </summary>
/// <param name="spear">罠槍オブジェクト(ダミー)</param>
/// <param name="target">攻撃対象オブジェクト</param>
void TrapSpearDamageToPlayer(OBJ_DATA* spear, OBJ_DATA* target)
{
  if(target->kind_ == OBJ_KIND_PLAYER)
  {
    PLAYER_OPTION* playerOp = (PLAYER_OPTION*)target->option_;
    //プレイヤーが無敵時間でなければHPを減らして無敵にする
    if(playerOp->invincibleTime_ <= 0)
    {
      playerOp->health_ -= GetStatus()->spearDamage_;
      playerOp->invincibleTime_ = GetStatus()->playerInvincibleTime_;
      playerOp->knockBackAdvanceTime_ = KNOCK_BACK_MAX_TIME;
    }
  }
  else if(target->kind_ == OBJ_KIND_SKELETON)
  {
    DeleteSkeleton(target);
  }
}
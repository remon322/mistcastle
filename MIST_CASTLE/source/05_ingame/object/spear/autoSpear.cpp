#include "../object.h"
#include "../player/player.h"
#include "../skeleton/skeleton.h"
#include "../spear/autoSpear.h"
#include "../../hitJudge/hitJudge.h"
#include "../../../00_application/function/easing.h"
#include "../../../00_application/app/app.h"
#include "../../../01_data/sound/dslib.h"
#include "../../../01_data/sound/loadSound.h"
#include "../../../01_data/model/loadModel.h"
#include "../../../01_data/status/loadStatus.h"

#define SPEAR_WAIT_POS_Y   (-80.0f) //非攻撃時の槍のY座標
#define SPEAR_ATTACK_POS_Y (0.0f)   //攻撃時の槍のY座標
#define SPEAR_STORE_TIME   (90)     //格納にかかる時間

//自動槍が当たったときの処理
void AutoSpearDamageToPlayer(OBJ_DATA* spear, OBJ_DATA* target);

//自動槍の削除
void DeleteAutoSpear(OBJ_DATA* arrow);

/// <summary>
/// 自動槍の生成
/// </summary>
/// <param name="pos_">生成する座標</param>
/// <returns>追加されたオブジェクトアドレス</returns>
OBJ_DATA* GenerateAutoSpear(D3DXVECTOR3 pos)
{
  //メモリの確保
  OBJ_DATA* autoSpear = AddObjData(OBJ_KIND_AUTO_SPEAR, pos, g_meshScaleTable[AUTO_SPEAR], 0);
  //あたり判定の設定
  AddHitListSphere(BODY, 0, INFINITY_FRAME, autoSpear, D3DXVECTOR3(0.0f, 0.0f, 0.0f), 5.0f, NULL);
  return autoSpear;
}

/// <summary>
/// 自動槍の更新
/// </summary>
/// <param name="autoSpear">更新する自動槍オブジェクト</param>
void UpdateAutoSpear(OBJ_DATA* autoSpear)
{
  AUTO_SPEAR_OPTION* autSpearOp = (AUTO_SPEAR_OPTION*)autoSpear->option_;
  autSpearOp->prepareTime_ -= GetSpeedRate();

  //準備時間が0になったら槍ををとび出させる
  if(autSpearOp->prepareTime_ <= 0 && autSpearOp->attackFlag_ == 0)
  {
    AddHitListBox(ATTACK, OBJ_KIND_PLAYER | OBJ_KIND_SKELETON, 10, autoSpear, D3DXVECTOR3(0.0f, 0.0f, 0.0f), 125.0f, 100.0f, 125.0f, AutoSpearDamageToPlayer);
    autSpearOp->attackFlag_ = 1;
  }
  //槍本体の格納が終わったら準備時間に戻る
  if(autSpearOp->prepareTime_ < -SPEAR_STORE_TIME - 30)
  {
    autSpearOp->prepareTime_ = GetStatus()->spearPrepareTime_;
    autSpearOp->attackFlag_ = 0;
  }

 //槍本体の高さ指定
  //槍飛び出し時のY座標設定
  if(autSpearOp->prepareTime_ >= -SPEAR_STORE_TIME)
    autSpearOp->spearPosY_ = JudgeAndEasing(OUT_QUINT,
                                        -autSpearOp->prepareTime_,
                                        SPEAR_WAIT_POS_Y,
                                        SPEAR_ATTACK_POS_Y - SPEAR_WAIT_POS_Y,
                                        30);
  //槍収納時のY座標設定
  if(autSpearOp->prepareTime_ < -SPEAR_STORE_TIME)
    autSpearOp->spearPosY_ = JudgeAndEasing(LINEAR,
                                        -autSpearOp->prepareTime_ - SPEAR_STORE_TIME,
                                        SPEAR_ATTACK_POS_Y,
                                        SPEAR_WAIT_POS_Y - SPEAR_ATTACK_POS_Y,
                                        30);
}

/// <summary>
/// 自動槍の削除
/// </summary>
/// <param name="autoSpear">削除する自動槍オブジェクト</param>
void DeleteAutoSpear(OBJ_DATA* autoSpear)
{
  DeleteListObjData(autoSpear);
}

/// <summary>
/// 自動槍が当たったときの処理
/// </summary>
/// <param name="spear">槍オブジェクト(ダミー)</param>
/// <param name="target">攻撃対象オブジェクト</param>
void AutoSpearDamageToPlayer(OBJ_DATA* spear, OBJ_DATA* target)
{
  //プレイヤーに対する攻撃
  if(target->kind_ == OBJ_KIND_PLAYER)
  {
    PLAYER_OPTION* playerOp = (PLAYER_OPTION*)target->option_;
    //プレイヤーが無敵時間でなければHPを減らして無敵にする
    if(playerOp->invincibleTime_ <= 0)
    {
      playerOp->health_ -= GetStatus()->spearDamage_;
      playerOp->invincibleTime_ = GetStatus()->playerInvincibleTime_;
      playerOp->knockBackAdvanceTime_ = KNOCK_BACK_MAX_TIME;
    }
  }
  //スケルトンに対する攻撃
  else if(target->kind_ == OBJ_KIND_SKELETON)
  {
    DeleteSkeleton(target);
  }
}
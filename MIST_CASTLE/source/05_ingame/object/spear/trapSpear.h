#pragma once
#include <d3dx9.h>
#include "../object.h"

//罠槍の詳細
struct TRAP_SPEAR_OPTION
{
  int   delayPenetrateTime_;//行動管理時間
  float spearPosY_;         //槍部分の描画座標
  bool  attackFlag_;        //攻撃時あたり判定が重複しないためのflag
};

//罠槍の生成
OBJ_DATA* GenerateTrapSpear(D3DXVECTOR3 pos);

//罠槍の更新
void UpdateTrapSpear(OBJ_DATA* trapSpear);
#include "../object.h"
#include "../mistCompressor/mistCompressor.h"
#include "../../fog/fog.h"
#include "../../camera/camera.h"
#include "../../hitJudge/hitJudge.h"
#include "../../../00_application/app/app.h"
#include "../../../00_application/lib/myDx9Lib.h"
#include "../../../00_application/function/math.h"
#include "../../../01_data/model/loadModel.h"
#include "../../../01_data/sound/loadSound.h"
#include "../../../01_data/texture/loadTexture.h"
#include "../../../01_data/status/loadStatus.h"

#define MIST_COMP_LIGHT_COLOR (D3DXVECTOR4(0.8f, 0.8f, 1.0f, 1.0f))
#define EFFECT_ROOP_TIME (120) //アニメーションのループ時間
#define EFFECT_TOTAL_NUM (8)   //スプライト枚数

/// <summary>
/// ミストコンプレッサーの生成
/// </summary>
/// <param name="pos_">ミストコンプレッサーを設置する座標</param>
/// <param name="isSaveUp">霧をためている状態ならtrue</param>
/// <returns>追加されたオブジェクトポインタ</returns>
OBJ_DATA* GenerateMistCompressor(D3DXVECTOR3 pos, bool isSaveUp)
{
  //メモリの確保
  OBJ_DATA* mistComp = AddObjData(OBJ_KIND_MIST_COMPRESSOR, pos, g_meshScaleTable[MIST_COMPRESSOR], 0);

  //オプションの設定
  MIST_COMPRESSOR_OPTION* mistCompOp = (MIST_COMPRESSOR_OPTION*)mistComp->option_;
  mistCompOp->isSaveUp_ = isSaveUp;

  //あたり判定の設定
  AddHitListSphere(BODY, 0, INFINITY_FRAME, mistComp, D3DXVECTOR3(0.0f, 0.0f, 0.0f), 1.0f, NULL);

  return mistComp;
}

/// <summary>
/// ミストコンプレッサーの更新
/// </summary>
/// <param name="mistComp">ミストコンプレッサーオブジェクト</param>
void UpdateMistCompressor(OBJ_DATA* mistComp)
{
  MIST_COMPRESSOR_OPTION* mistCompOp = (MIST_COMPRESSOR_OPTION*)mistComp->option_;
  //進行時間の更新
  mistCompOp->advanceTime_ += GetSpeedRate();
  mistCompOp->accessBlockTime_ -= GetSpeedRate();
  //値が大きくなりすぎないようにEFFECT_ROOP_TIME以下に留める
  if(mistCompOp->advanceTime_ > EFFECT_ROOP_TIME)
  {
    mistCompOp->advanceTime_ -= EFFECT_ROOP_TIME;
  }

  //使用する連番が何枚目かをチェックする
  mistCompOp->useTexNum_ = mistCompOp->advanceTime_ / (EFFECT_ROOP_TIME / EFFECT_TOTAL_NUM);

  if(mistCompOp->isSaveUp_ == true)
  {
    //光源をセットする
    PushLightSource(mistComp->pos_, MIST_COMP_LIGHT_COLOR, GetStatus()->mistCompLightRange_, GetDistance(mistComp->pos_, GetCamera().vEyePt_));
  }
}

/// <summary>
/// ミストコンプレッサーの描画
/// </summary>
/// <param name="mistComp">ミストコンプレッサーオブジェクト</param>
void RenderMistCompresssor(OBJ_DATA* mistComp)
{
  //霧の濃度取得
  float farFog = 0;
  GetFogInfomation(NULL, NULL, &farFog);
  //霧の最大濃さより遠くにあればはじく
  if(GetDistance(mistComp->pos_, GetCamera().vEyePt_) > farFog)
    return;

  //オプションの設定
  MIST_COMPRESSOR_OPTION* mistCompOp = (MIST_COMPRESSOR_OPTION*)mistComp->option_;
  if(mistCompOp->isSaveUp_)
    RenderBlockBillboardRot(mistComp->pos_, GetCamera().vEyePt_, 0.0f, g_textureHandles[MISTCOMPRESSOR_ON], 255, mistCompOp->useTexNum_);
  else
    RenderBlockBillboardRot(mistComp->pos_, GetCamera().vEyePt_, 0.0f, g_textureHandles[MISTCOMPRESSOR_OFF], 255, mistCompOp->useTexNum_);
}

/// <summary>
/// ミストコンプレッサーの消去
/// </summary>
/// <param name="mistComp">ミストコンプレッサーオブジェクト</param>
void DeleteMistCompressor(OBJ_DATA* mistComp)
{
  DeleteListObjData(mistComp);
}
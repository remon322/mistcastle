#include <windows.h>
#include "object.h"
#include "player/player.h"
#include "skeleton/skeleton.h"
#include "dropItem/dropItems.h"
#include "mistCompressor/mistCompressor.h"
#include "altar/altar.h"
#include "launcher/launcher.h"
#include "launcher/arrow.h"
#include "spear/autoSpear.h"
#include "spear/trapSpear.h"
#include "lightSource/lightSource.h"
#include "treasure/treasure.h"
#include "../camera/camera.h"
#include "../hitJudge/hitJudge.h"
#include "../../00_application/app/app.h"
#include "../../01_data/model/loadModel.h"
#include "../../01_data/texture/loadTexture.h"
#include "../../02_input/input.h"

static OBJ_DATA* g_headObjData = NULL;
static OBJ_DATA* g_endObjData = NULL;

/// <summary>
/// オブジェクトリストの先頭ポインタの取得
/// </summary>
/// <returns>先頭ポインタ</returns>
OBJ_DATA* GetHeadObjData()
{
  return g_headObjData;
}

/// <summary>
/// オブジェクトリストの最後尾ポインタの取得
/// </summary>
/// <returns>最後尾ポインタ</returns>
OBJ_DATA* GetEndObjData()
{
  return g_endObjData;
}

/// <summary>
/// リストへのオブジェクトデータの追加
/// </summary>
/// <returns>追加されたオブジェクトポインタ</returns>
OBJ_DATA* AddListObjData()
{
  if(g_endObjData == NULL)
  {
    //リストにデータがないとき
    //メモリの確保
    OBJ_DATA *newPointer = (OBJ_DATA*)malloc(sizeof(OBJ_DATA));
    memset(newPointer, 0, sizeof(OBJ_DATA));
    //前後をNULLに
    newPointer->pNext_ = NULL;
    newPointer->pPrev_ = NULL;
    //リストの最前と最後尾を追加されたポインタに
    g_headObjData = newPointer;
    g_endObjData = newPointer;
    return newPointer;
  }

  //メモリの確保と確保メモリの前後へのリンク
  OBJ_DATA *newPointer = (OBJ_DATA*)malloc(sizeof(OBJ_DATA));
  OBJ_DATA *prevPointer = g_endObjData;

  //前後のメモリのリンク
  prevPointer->pNext_ = newPointer;
  newPointer->pPrev_ = prevPointer;
  newPointer->pNext_ = NULL;

  //リストの最後尾に確保したメモリを設定
  g_endObjData = newPointer;
  return newPointer;
}

/// <summary>
/// 引数のアドレスのオブジェクトデータのメモリを解放し、前後のリストをつなげる
/// </summary>
/// <param name="deletePointer">削除したいオブジェクト</param>
void DeleteListObjData(OBJ_DATA* deletePointer)
{
  if(deletePointer == NULL)
  {
    return;
  }

  //オプションのメモリがある場合解放する
  switch(deletePointer->kind_)
  {
  case OBJ_KIND_PLAYER:
  case OBJ_KIND_SKELETON:
  case OBJ_KIND_DROP_ITEM:
  case OBJ_KIND_MIST_COMPRESSOR:
  case OBJ_KIND_ALTER:
  case OBJ_KIND_LAUNCHER:
  case OBJ_KIND_ARROW:
  case OBJ_KIND_AUTO_SPEAR:
  case OBJ_KIND_TRAP_SPEAR:
  case OBJ_KIND_LIGHT_SOURCE:
    free(deletePointer->option_);
    deletePointer->option_ = NULL;
    break;
  }

  //リストの前後をつなぎ合わせる
  OBJ_DATA* prevPointer = deletePointer->pPrev_;
  OBJ_DATA* nextPointer = deletePointer->pNext_;

  //削除対象がリストの先頭か？
  if(deletePointer == g_headObjData)
    g_headObjData = g_headObjData->pNext_;//先頭をひとつ次に
  else
    prevPointer->pNext_ = nextPointer;//前の次に、現在の次を

  //削除対象がリストの最後尾か？
  if(deletePointer == g_endObjData)
    g_endObjData = g_endObjData->pPrev_;//最後尾をひとつ前に
  else
    nextPointer->pPrev_ = prevPointer;//次の前に、現在の前を

  //メモリ解放
  free(deletePointer);
  deletePointer = NULL;
}

//
//
/// <summary>
/// オブジェクトの追加
/// </summary>
/// <param name="objKind">追加するオブジェクトの種類(OBJ_KIND)</param>
/// <param name="pos">生成座標</param>
/// <param name="scale">オブジェクトのスケール</param>
/// <param name="yawAngle">回転角度</param>
/// <returns>追加したオブジェクトのポインタ</returns>
OBJ_DATA* AddObjData(int objKind, D3DXVECTOR3 pos, D3DXVECTOR3 scale, float yawAngle)
{
  //リストに追加
  OBJ_DATA* newObject = AddListObjData();
  //データの代入
  newObject->kind_ = objKind;
  newObject->pos_ = pos;
  newObject->scale_ = scale;
  newObject->yawAngle_ = yawAngle;
  newObject->pMeshHandle_ = NULL;
  //当たり判定のNULL化
  InitHitList(newObject);

  //fbx用のアニメーション情報
  newObject->animationData_.frame = 0;
  newObject->animationData_.motionName = "default";

  //メッシュデータハンドルを設定
  switch(objKind)
  {
  case OBJ_KIND_PLAYER:
    newObject->pMeshHandle_ = g_meshHandles[PLAYER];
    newObject->animationData_ = GetAnimationData(newObject->pMeshHandle_);
    break;
  case OBJ_KIND_FOG:
    newObject->pMeshHandle_ = g_meshHandles[FOG_TUBE];
    newObject->animationData_ = GetAnimationData(newObject->pMeshHandle_);
    break;
  //case OBJ_KIND_GROUND:
  //  newObject->pMeshHandle_ = meshHandles[TILE];
  //  newObject->animationData_ = GetAnimationData(newObject->pMeshHandle_);
  //  break;
  case OBJ_KIND_SKELETON:
    newObject->pMeshHandle_ = g_meshHandles[SKELETON];
    newObject->animationData_ = GetAnimationData(newObject->pMeshHandle_);
    break;
  case OBJ_KIND_DROP_ITEM:
    newObject->pMeshHandle_ = g_meshHandles[DROP_ITEMS];
    newObject->animationData_ = GetAnimationData(newObject->pMeshHandle_);
    break;
  case OBJ_KIND_MAP:
    newObject->pMeshHandle_ = g_meshHandles[MAP];
    newObject->animationData_ = GetAnimationData(newObject->pMeshHandle_);
    break;
  case OBJ_KIND_TERRAIN:
    newObject->pMeshHandle_ = g_meshHandles[TERRAIN];
    newObject->animationData_ = GetAnimationData(newObject->pMeshHandle_);
    break;
  case OBJ_KIND_MIST_COMPRESSOR:
    newObject->pMeshHandle_ = g_meshHandles[MIST_COMPRESSOR];
    newObject->animationData_ = GetAnimationData(newObject->pMeshHandle_);
    break;
  case OBJ_KIND_ALTER:
    newObject->pMeshHandle_ = g_meshHandles[ALTER];
    newObject->animationData_ = GetAnimationData(newObject->pMeshHandle_);
    break;
  case OBJ_KIND_LAUNCHER:
    newObject->pMeshHandle_ = g_meshHandles[LAUNCHER];
    newObject->animationData_ = GetAnimationData(newObject->pMeshHandle_);
    break;
  case OBJ_KIND_ARROW:
    newObject->pMeshHandle_ = g_meshHandles[ARROW];
    newObject->animationData_ = GetAnimationData(newObject->pMeshHandle_);
    break;
  case OBJ_KIND_SPIDERWEB:
    newObject->pMeshHandle_ = g_meshHandles[SPIDERWEB];
    newObject->animationData_ = GetAnimationData(newObject->pMeshHandle_);
    break;
  case OBJ_KIND_AUTO_SPEAR:
    newObject->pMeshHandle_ = g_meshHandles[AUTO_SPEAR];
    newObject->animationData_ = GetAnimationData(newObject->pMeshHandle_);
    break;
  case OBJ_KIND_TRAP_SPEAR:
    newObject->pMeshHandle_ = g_meshHandles[TRAP_SPEAR];
    newObject->animationData_ = GetAnimationData(newObject->pMeshHandle_);
    break;
  case OBJ_KIND_LIGHT_SOURCE:
    newObject->pMeshHandle_ = NULL;
    break;
  case OBJ_KIND_TREASURE:
    newObject->pMeshHandle_ = g_meshHandles[TREASURE];
    newObject->animationData_ = GetAnimationData(newObject->pMeshHandle_);
    break;
  }

  //オプション部のメモリ確保
  switch(objKind)
  {
  case OBJ_KIND_PLAYER:
    newObject->option_ = (PLAYER_OPTION*)malloc(sizeof(PLAYER_OPTION));
    memset(newObject->option_, 0, sizeof(PLAYER_OPTION));
    InitPlayer(newObject);
    break;
  case OBJ_KIND_SKELETON:
    newObject->option_ = (SKELETON_OPTION*)malloc(sizeof(SKELETON_OPTION));
    memset(newObject->option_, 0, sizeof(SKELETON_OPTION));
    break;
  case OBJ_KIND_DROP_ITEM:
    newObject->option_ = (ITEM_OPTION*)malloc(sizeof(ITEM_OPTION));
    memset(newObject->option_, 0, sizeof(ITEM_OPTION));
    break;
  case OBJ_KIND_MIST_COMPRESSOR:
    newObject->option_ = (MIST_COMPRESSOR_OPTION*)malloc(sizeof(MIST_COMPRESSOR_OPTION));
    memset(newObject->option_, 0, sizeof(MIST_COMPRESSOR_OPTION));
    break;
  case OBJ_KIND_ALTER:
    newObject->option_ = (ALTER_OPTION*)malloc(sizeof(ALTER_OPTION));
    memset(newObject->option_, 0, sizeof(ALTER_OPTION));
    break;
  case OBJ_KIND_LAUNCHER:
    newObject->option_ = (LAUNCHER_OPTION*)malloc(sizeof(LAUNCHER_OPTION));
    memset(newObject->option_, 0, sizeof(LAUNCHER_OPTION));
    break;
  case OBJ_KIND_ARROW:
    newObject->option_ = (ARROW_OPTION*)malloc(sizeof(ARROW_OPTION));
    memset(newObject->option_, 0, sizeof(ARROW_OPTION));
    break;
  case OBJ_KIND_AUTO_SPEAR:
    newObject->option_ = (AUTO_SPEAR_OPTION*)malloc(sizeof(AUTO_SPEAR_OPTION));
    memset(newObject->option_, 0, sizeof(AUTO_SPEAR_OPTION));
    break;
  case OBJ_KIND_TRAP_SPEAR:
    newObject->option_ = (TRAP_SPEAR_OPTION*)malloc(sizeof(TRAP_SPEAR_OPTION));
    memset(newObject->option_, 0, sizeof(TRAP_SPEAR_OPTION));
    break;
  case OBJ_KIND_LIGHT_SOURCE:
    newObject->option_ = (LIGHT_SOURCE_OPTION*)malloc(sizeof(LIGHT_SOURCE_OPTION));
    memset(newObject->option_, 0, sizeof(LIGHT_SOURCE_OPTION));
    break;
  }

  //描画優先度の設定
  switch(objKind)
  {
  case OBJ_KIND_PLAYER:
  case OBJ_KIND_SKELETON:
  case OBJ_KIND_MAP:
  case OBJ_KIND_MIST_COMPRESSOR:
  case OBJ_KIND_ALTER:
  case OBJ_KIND_LAUNCHER:
  case OBJ_KIND_ARROW:
  case OBJ_KIND_SPIDERWEB:
  case OBJ_KIND_AUTO_SPEAR:
  case OBJ_KIND_TRAP_SPEAR:
    newObject->renderPriority_ = PRIORITY_NO_ALPHA;
    break;
  case OBJ_KIND_FOG:
  case OBJ_KIND_DROP_ITEM:
  case OBJ_KIND_TREASURE:
    newObject->renderPriority_ = PRIORITY_ALPHA;
    break;
  case OBJ_KIND_TERRAIN:
    newObject->renderPriority_ = 3;
    break;
  }

  //陰をつけるか設定
  switch(objKind)
  {
  case OBJ_KIND_PLAYER:
  case OBJ_KIND_SKELETON:
  case OBJ_KIND_DROP_ITEM:
  case OBJ_KIND_TERRAIN:
  case OBJ_KIND_MIST_COMPRESSOR:
  case OBJ_KIND_ALTER:
  case OBJ_KIND_LAUNCHER:
  case OBJ_KIND_SPIDERWEB:
  case OBJ_KIND_AUTO_SPEAR:
  case OBJ_KIND_TRAP_SPEAR:
  case OBJ_KIND_MAP:
  case OBJ_KIND_TREASURE:
    newObject->isShade_ = true;
    break;
  case OBJ_KIND_FOG:
  case OBJ_KIND_ARROW:
    newObject->isShade_ = false;
    break;
  }
  return newObject;
}

/// <summary>
/// オブジェクトの初期設置
/// </summary>
void InitObject()
{
  //スケルトンのステータス初期化
  InitSkeletonStatus();

  D3DXVECTOR3 pos;
  D3DXVECTOR3 scale;

  //地形オブジェクトの追加
  pos = {0.0f, 0.0f, 0.0f};
  scale = g_meshScaleTable[TERRAIN];
  AddObjData(OBJ_KIND_TERRAIN, pos, scale, 0.0f);
  //描画マップオブジェクトの追加
  scale = g_meshScaleTable[MAP];
  AddObjData(OBJ_KIND_MAP, pos, scale, 0.0f);
  //プレイヤーオブジェクトの追加
  pos = {0.0f, 10.0f, 0.0f};
  scale = g_meshScaleTable[PLAYER];
  AddObjData(OBJ_KIND_PLAYER, pos, scale, PI);
}

/// <summary>
/// オブジェクトの更新
/// </summary>
void UpdateObject()
{
  //先頭ポインタの取得
  OBJ_DATA* nowPointer = GetHeadObjData();
  if(!nowPointer) { return; }
  OBJ_DATA* nextPointer = nowPointer->pNext_;

  while(nowPointer)
  {
    //オブジェクトの種類ごとの処理
    switch(nowPointer->kind_)
    {
    case OBJ_KIND_PLAYER:
      UpdatePlayer(nowPointer);
      break;
    case OBJ_KIND_FOG:
      //プレイヤーを中心に回転
      nowPointer->pos_ = GetCamera().vEyePt_;
      nowPointer->yawAngle_ -= 0.005f;
      nowPointer->yawAngle_ = NormalizeAngle(nowPointer->yawAngle_);
      break;
    case OBJ_KIND_SKELETON:
      if(GetGameMode() != GAMEMODE_MAP_CREATE)
        UpdateSkeleton(nowPointer);
      break;
    case OBJ_KIND_MIST_COMPRESSOR:
      UpdateMistCompressor(nowPointer);
      break;
    case OBJ_KIND_DROP_ITEM:
      UpdateDropItems(nowPointer);
      break;
    case OBJ_KIND_ALTER:
      UpdateAlter(nowPointer);
      break;
    case OBJ_KIND_LAUNCHER:
      UpdateLauncher(nowPointer);
      break;
    case OBJ_KIND_ARROW:
      UpdateArrow(nowPointer);
      break;
    case OBJ_KIND_SPIDERWEB:
      //UpdateSpiderweb(nowPointer):
      break;
    case OBJ_KIND_AUTO_SPEAR:
      if(GetGameMode() != GAMEMODE_MAP_CREATE)
        UpdateAutoSpear(nowPointer);
      break;
    case OBJ_KIND_TRAP_SPEAR:
      if(GetGameMode() != GAMEMODE_MAP_CREATE)
        UpdateTrapSpear(nowPointer);
      break;
    case OBJ_KIND_TERRAIN:
      //当たり判定描画モードのときに描画優先度を変更する
      if(GetGameMode() == GAMEMODE_DEBUG_HIT_JUDGE)
        nowPointer->renderPriority_ = 0; //通常描画物
      else
        nowPointer->renderPriority_ = 2; //描画適応外
      break;
    case OBJ_KIND_LIGHT_SOURCE:
      UpdateLightSource(nowPointer);
      break;
    }

    //次のリストを参照
    nowPointer = nextPointer;
    if(!nowPointer) { return; }
    nextPointer = nowPointer->pNext_;
  }
}

/// <summary>
/// すべてのオブジェクトの描画
/// </summary>
void RenderObject()
{
  //リストの先頭を取得
  OBJ_DATA* nowPointer = GetHeadObjData();
  //オブジェクトの描画
  for(int i = 0; i < PRIORITY_MAX; i++)
  {
    while(nowPointer)
    {
      //優先度順に描画
      if(nowPointer->renderPriority_ == i)
      {
        //スケルトンが一定距離以上離れていたら描画しない
        if(nowPointer->kind_ == OBJ_KIND_SKELETON && GetDistance(nowPointer->pos_, GetCamera().vEyePt_) > 2500)
        {
          //次へ
          nowPointer = nowPointer->pNext_;
          continue;
        }

        //槍部分の描画
        if(nowPointer->kind_ == OBJ_KIND_AUTO_SPEAR
           || nowPointer->kind_ == OBJ_KIND_TRAP_SPEAR)
        {
          Render3D(nowPointer->pos_.x,
                   nowPointer->pos_.y + ((TRAP_SPEAR_OPTION*)nowPointer->option_)->spearPosY_,
                   nowPointer->pos_.z,
                   g_meshScaleTable[SPEAR].x,
                   g_meshScaleTable[SPEAR].y,
                   g_meshScaleTable[SPEAR].z,
                   nowPointer->yawAngle_,
                   g_meshHandles[SPEAR],
                   &nowPointer->animationData_,
                   nowPointer->isShade_);
        }

        //オブジェクトの描画
        Render3D(nowPointer->pos_.x,
                 nowPointer->pos_.y,
                 nowPointer->pos_.z,
                 nowPointer->scale_.x,
                 nowPointer->scale_.y,
                 nowPointer->scale_.z,
                 nowPointer->yawAngle_,
                 nowPointer->pMeshHandle_,
                 &nowPointer->animationData_,
                 nowPointer->isShade_);

        //ドロップアイテムのエフェクト描画
        if(nowPointer->kind_ == OBJ_KIND_DROP_ITEM)
        {
          RenderDropItems(nowPointer);
        }
        //宝箱のエフェクト描画
        if(nowPointer->kind_ == OBJ_KIND_TREASURE)
        {
          RenderTreasure(nowPointer);
        }

        //光源の描画
        if(nowPointer->kind_ == OBJ_KIND_LIGHT_SOURCE && GetGameMode() == GAMEMODE_MAP_CREATE)
        {
          RenderLightSource(nowPointer);
        }
        //ミストコンプレッサーのエフェクト描画
        if(nowPointer->kind_ == OBJ_KIND_MIST_COMPRESSOR)
        {
          RenderMistCompresssor(nowPointer);
        }
      }
      //次へ
      nowPointer = nowPointer->pNext_;
    }
    //もう一度オブジェクトの頭から
    nowPointer = GetHeadObjData();
  }
}

/// <summary>
/// オブジェクトの個数を求める
/// </summary>
/// <returns>オブジェクトの個数</returns>
int GetObjectNum()
{
  int result = 0;
  OBJ_DATA* nowObject = GetHeadObjData();
  //オブジェクトリストを回してカウント
  while(nowObject)
  {
    result++;
    nowObject = nowObject->pNext_;
  }
  return result;
}

/// <summary>
/// 全オブジェクトの開放
/// </summary>
void ReleaseObject()
{
  //最後尾から削除していく
  OBJ_DATA* endData = GetEndObjData();
  while(endData)
  {
    DeleteListObjData(endData);
    endData = GetEndObjData();
  }
}

/// <summary>
/// メッシュハンドルの読み込み直し
/// </summary>
void ReloadMeshHandle()
{
  OBJ_DATA* nowObject = GetHeadObjData();
  //オブジェクトの種類ごとにハンドルを判断
  while (nowObject)
  {
    switch (nowObject->kind_)
    {
    case OBJ_KIND_PLAYER:
      nowObject->pMeshHandle_ = g_meshHandles[PLAYER];
      break;
    case OBJ_KIND_FOG:
      nowObject->pMeshHandle_ = g_meshHandles[FOG_TUBE];
      break;
    case OBJ_KIND_SKELETON:
      nowObject->pMeshHandle_ = g_meshHandles[SKELETON];
      break;
    case OBJ_KIND_DROP_ITEM:
      nowObject->pMeshHandle_ = g_meshHandles[DROP_ITEMS];
      break;
    case OBJ_KIND_MAP:
      nowObject->pMeshHandle_ = g_meshHandles[MAP];
      break;
    case OBJ_KIND_TERRAIN:
      nowObject->pMeshHandle_ = g_meshHandles[TERRAIN];
      break;
    case OBJ_KIND_MIST_COMPRESSOR:
      nowObject->pMeshHandle_ = g_meshHandles[MIST_COMPRESSOR];
      break;
    case OBJ_KIND_ALTER:
      nowObject->pMeshHandle_ = g_meshHandles[ALTER];
      break;
    case OBJ_KIND_LAUNCHER:
      nowObject->pMeshHandle_ = g_meshHandles[LAUNCHER];
      break;
    case OBJ_KIND_ARROW:
      nowObject->pMeshHandle_ = g_meshHandles[ARROW];
      break;
    case OBJ_KIND_SPIDERWEB:
      nowObject->pMeshHandle_ = g_meshHandles[SPIDERWEB];
      break;
    case OBJ_KIND_AUTO_SPEAR:
      nowObject->pMeshHandle_ = g_meshHandles[AUTO_SPEAR];
      break;
    case OBJ_KIND_TRAP_SPEAR:
      nowObject->pMeshHandle_ = g_meshHandles[TRAP_SPEAR];
      break;
    }
    nowObject = nowObject->pNext_;
  }
}
#include "../object.h"
#include "../altar/altar.h"
#include "../../hitJudge/hitJudge.h"
#include "../../../00_application/lib/myDx9Lib.h"
#include "../../../00_application/function/easing.h"
#include "../../../01_data/model/loadModel.h"
#include "../../../01_data/sound/loadSound.h"
#include "../../../01_data/status/loadStatus.h"

/// <summary>
/// 祭壇の生成
/// </summary>
/// <param name="pos_">祭壇の設置座標</param>
/// <param name="yawAngle_">祭壇の回転角度</param>
/// <returns>追加されたオブジェクトポインタ</returns>
OBJ_DATA* GenerateAlter(D3DXVECTOR3 pos, float yawAngle)
{
  //メモリの確保
  OBJ_DATA* alter = AddObjData(OBJ_KIND_ALTER, pos, g_meshScaleTable[ALTER], yawAngle);

  //オプションの設定
  ALTER_OPTION* alterOp = (ALTER_OPTION*)alter->option_;
  alterOp->isMove_ = false;
  alterOp->moveAdvanceTime_ = 0;
  alterOp->startPos_ = pos;

  //あたり判定の設定
  AddHitListSphere(BODY, 0, INFINITY_FRAME, alter, D3DXVECTOR3(0.0f, 0.0f, 0.0f), 3.0f, NULL);

  return alter;
}

/// <summary>
/// 祭壇の更新
/// </summary>
/// <param name="alter">更新する祭壇オブジェクトポインタ</param>
void UpdateAlter(OBJ_DATA* alter)
{
  //オプションの設定
  ALTER_OPTION* alterOp = (ALTER_OPTION*)alter->option_;

  //祭壇の移動処理
  if(alterOp->isMove_ == true && alterOp->moveAdvanceTime_ < GetStatus()->alterMoveTime_)
  {
    //移動時間の更新
    alterOp->moveAdvanceTime_++;

    alter->pos_.x = JudgeAndEasing(LINEAR, alterOp->moveAdvanceTime_, alterOp->startPos_.x, GetStatus()->alterMoveDistance_ * cos(alter->yawAngle_), GetStatus()->alterMoveTime_);
    alter->pos_.z = JudgeAndEasing(LINEAR, alterOp->moveAdvanceTime_, alterOp->startPos_.z, GetStatus()->alterMoveDistance_ * sin(alter->yawAngle_), GetStatus()->alterMoveTime_);
  }
}

/// <summary>
/// 祭壇の消去
/// </summary>
/// <param name="alter">削除したい祭壇オブジェクトポインタ</param>
void DeleteAlter(OBJ_DATA* alter)
{
  DeleteListObjData(alter);
}
#include "skeleton.h"
#include "../object.h"
#include "../player/player.h"
#include "../../hitJudge/hitJudge.h"
#include "../../fog/fog.h"
#include "../../../00_application/function/math.h"
#include "../../../00_application/lib/myDx9Lib.h"
#include "../../../00_application/app/app.h"
#include "../../../01_data/model/loadModel.h"
#include "../../../01_data/sound/loadSound.h"
#include "../../../01_data/status/loadStatus.h"
#include "../../../01_data/sound/dslib.h"

//スケルトンの振り向き処理
float TurningSkeleton(float nowAngle, float nextAngle);
//スケルトンの状態変更
void ChangeStateSkeleton(OBJ_DATA* skeleton, int nextState);
//スケルトンの攻撃
void AttackSkeleton(OBJ_DATA* skeleton);
//プレイヤーに対する攻撃
void SkeletonAttackToPlayer(OBJ_DATA* skeleton, OBJ_DATA* target);
// スケルトンの索敵中止処理
void SkeletonStateChangeIdlePrepare(OBJ_DATA* skeleton, OBJ_DATA* target);
//スケルトンの索敵成功処理
void SkeletonStateChangeApproach(OBJ_DATA* skeleton, OBJ_DATA* target);
//スケルトンの攻撃範囲にプレイヤーが入ったときの処理
void SkeletonStateChangeAttack(OBJ_DATA* skeleton, OBJ_DATA* target);
//スケルトンのアニメーション切り替え
void SkeletonAnimationChange(OBJ_DATA* pSkeleton);

enum SKELETON_STATUS_RATE_NAME
{
  SPEED_RATE = 0,
  ATTACK_RATE = 1,
  SEARCH_RATE = 2,
};

//霧の濃さに応じたスケルトンのステータス倍率
float g_skeletonStatusRate[5][3]
{//移動速度倍率,攻撃力倍率,索敵範囲倍率
  {1.4f, 1.8f, 1.0f},
  {1.2f, 1.4f, 1.0f},
  {1.0f, 1.0f, 1.0f},
  {0.8f, 0.6f, 1.0f},
  {0.6f, 0.4f, 1.0f},
};

float g_normalSearchRange;  //通常スケルトンの索敵範囲の標準値
float g_normalAttackRange;  //通常スケルトンの攻撃範囲の標準値
float g_halfSearchRange;    //上半身スケルトンの索敵範囲の標準値
float g_armorSearchRange;   //装備有スケルトンの索敵範囲の標準値
float g_armorAttackRange;   //装備有スケルトンの攻撃範囲の標準値

/// <summary>
/// スケルトンのステータス初期化
/// </summary>
void InitSkeletonStatus()
{
  STATUS st = *GetStatus();
  //移動速度倍率
  g_skeletonStatusRate[0][0] = st.skeletonSpeedRate1_;
  g_skeletonStatusRate[1][0] = st.skeletonSpeedRate2_;
  g_skeletonStatusRate[2][0] = st.skeletonSpeedRate3_;
  g_skeletonStatusRate[3][0] = st.skeletonSpeedRate4_;
  g_skeletonStatusRate[4][0] = st.skeletonSpeedRate5_;
  //攻撃力倍率
  g_skeletonStatusRate[0][1] = st.skeletonAttackRate1_;
  g_skeletonStatusRate[1][1] = st.skeletonAttackRate2_;
  g_skeletonStatusRate[2][1] = st.skeletonAttackRate3_;
  g_skeletonStatusRate[3][1] = st.skeletonAttackRate4_;
  g_skeletonStatusRate[4][1] = st.skeletonAttackRate5_;
  //索敵範囲倍率
  g_skeletonStatusRate[0][2] = st.skeletonSearchRate1_;
  g_skeletonStatusRate[1][2] = st.skeletonSearchRate2_;
  g_skeletonStatusRate[2][2] = st.skeletonSearchRate3_;
  g_skeletonStatusRate[3][2] = st.skeletonSearchRate4_;
  g_skeletonStatusRate[4][2] = st.skeletonSearchRate5_;

  //範囲
  g_normalSearchRange = st.normalSkeletonSearchRange_;
  g_normalAttackRange = st.normalSkeletonAttackRange_;
  g_halfSearchRange   = st.halfSkeletonSearchRange_;
  g_armorSearchRange  = st.armorSkeletonSearchRange_;
  g_armorAttackRange  = st.armorSkeletonAttackRange_;
}

/// <summary>
/// スケルトンの当たり判定と霧濃度の関連付け
/// </summary>
void SkeletonLinkFogLevel()
{
  //全オブジェクトを参照
  OBJ_DATA* now = GetHeadObjData();
  while(now != NULL)
  {
    //スケルトンでなければ次のポインタへ
    if(now->kind_ != OBJ_KIND_SKELETON)
    {
      now = now->pNext_;
      continue;
    }//if

    //スケルトンであれば、そのスケルトンのすべての当たり判定を参照
    HIT_LIST* hitNow = now->headHitList_;
    HIT_LIST* hitNext;
    while(hitNow != NULL)
    {
      //次の当たり判定を保管
      hitNext = hitNow->pNext;
      //索敵系当たり判定であれば消去
      if(hitNow->kind == SEARCH)
      {
        //今の当たり判定を消去
        DeleteHitList(now, hitNow);
      }//if
      //次の当たり判定に移行
      hitNow = hitNext;
    }//while

    //オプションがヌルポであれば弾く
    if(now->option_ == NULL)
    {
      now = now->pNext_;
      continue;
    }//if

    //霧濃度の取得
    int fogLevel;
    GetFogInfomation(&fogLevel, NULL, NULL);

    //スケルトンの種類に応じて当たり判定の付け直し
    E_SKELETON_KIND skeletonKind = ((SKELETON_OPTION*)now->option_)->skeKind_;
    switch(skeletonKind)
    {
    //通常スケルトン
    case SKELETON_KIND_NORMAL:
      AddHitListSphere(SEARCH, OBJ_KIND_PLAYER, INFINITY_FRAME, now, D3DXVECTOR3(0.0f, 0.0f, 0.0f), g_normalSearchRange * g_skeletonStatusRate[fogLevel][SEARCH_RATE] + 100.0f, SkeletonStateChangeIdlePrepare);
      AddHitListSphere(SEARCH, OBJ_KIND_PLAYER, INFINITY_FRAME, now, D3DXVECTOR3(0.0f, 0.0f, 0.0f), g_normalSearchRange * g_skeletonStatusRate[fogLevel][SEARCH_RATE], SkeletonStateChangeApproach);
      AddHitListSphere(SEARCH, OBJ_KIND_PLAYER, INFINITY_FRAME, now, D3DXVECTOR3(0.0f, 0.0f, 0.0f), g_normalAttackRange * g_skeletonStatusRate[fogLevel][SEARCH_RATE], SkeletonStateChangeAttack);
      break;
    //上半身スケルトン
    case SKELETON_KIND_HALF:
      AddHitListSphere(SEARCH, OBJ_KIND_PLAYER, INFINITY_FRAME, now, D3DXVECTOR3(0.0f, 0.0f, 0.0f), g_halfSearchRange * g_skeletonStatusRate[fogLevel][SEARCH_RATE] + 100.0f, SkeletonStateChangeIdlePrepare);
      AddHitListSphere(SEARCH, OBJ_KIND_PLAYER, INFINITY_FRAME, now, D3DXVECTOR3(0.0f, 0.0f, 0.0f), g_halfSearchRange * g_skeletonStatusRate[fogLevel][SEARCH_RATE], SkeletonStateChangeApproach);
      break;
    //装備有スケルトン
    case SKELETON_KIND_ARMOR:
      AddHitListSphere(SEARCH, OBJ_KIND_PLAYER, INFINITY_FRAME, now, D3DXVECTOR3(0.0f, 0.0f, 0.0f), g_armorSearchRange * g_skeletonStatusRate[fogLevel][SEARCH_RATE] + 100.0f, SkeletonStateChangeIdlePrepare);
      AddHitListSphere(SEARCH, OBJ_KIND_PLAYER, INFINITY_FRAME, now, D3DXVECTOR3(0.0f, 0.0f, 0.0f), g_armorSearchRange * g_skeletonStatusRate[fogLevel][SEARCH_RATE], SkeletonStateChangeApproach);
      AddHitListSphere(SEARCH, OBJ_KIND_PLAYER, INFINITY_FRAME, now, D3DXVECTOR3(0.0f, 0.0f, 0.0f), g_armorAttackRange * g_skeletonStatusRate[fogLevel][SEARCH_RATE], SkeletonStateChangeAttack);
      break;
      //値が不正なら通常スケルトンとして扱う
    default:
      AddHitListSphere(SEARCH, OBJ_KIND_PLAYER, INFINITY_FRAME, now, D3DXVECTOR3(0.0f, 0.0f, 0.0f), g_normalSearchRange * g_skeletonStatusRate[fogLevel][SEARCH_RATE] + 100.0f, SkeletonStateChangeIdlePrepare);
      AddHitListSphere(SEARCH, OBJ_KIND_PLAYER, INFINITY_FRAME, now, D3DXVECTOR3(0.0f, 0.0f, 0.0f), g_normalSearchRange * g_skeletonStatusRate[fogLevel][SEARCH_RATE], SkeletonStateChangeApproach);
      AddHitListSphere(SEARCH, OBJ_KIND_PLAYER, INFINITY_FRAME, now, D3DXVECTOR3(0.0f, 0.0f, 0.0f), g_normalAttackRange * g_skeletonStatusRate[fogLevel][SEARCH_RATE], SkeletonStateChangeAttack);
      break;
    }//switch

    //一通り終わったので次のオブジェクトへ
    now = now->pNext_;

  }//while
}//func

/// <summary>
/// スケルトンの生成
/// </summary>
/// <param name="pos">スケルトンを生成する座標</param>
/// <param name="kind">スケルトンの種類(E_SKELETON_KINDを参照)</param>
/// <returns>追加されたオブジェクトアドレス</returns>
OBJ_DATA* GenerateSkeleton(D3DXVECTOR3 pos, int kind)
{
  OBJ_DATA* skeleton = NULL;
  //メモリの確保
  switch(kind)
  {
  //通常スケルトン
  case SKELETON_KIND_NORMAL:
    skeleton = AddObjData(OBJ_KIND_SKELETON, pos, g_meshScaleTable[SKELETON], 0);
    //あたり判定の設定
    AddHitListSphere(BODY, 0, INFINITY_FRAME, skeleton, D3DXVECTOR3(0.0f, 0.0f, 0.0f), 25.0f, NULL);
    AddHitListSphere(SEARCH, OBJ_KIND_PLAYER, INFINITY_FRAME, skeleton, D3DXVECTOR3(0.0f, 0.0f, 0.0f), g_normalSearchRange + 100.0f, SkeletonStateChangeIdlePrepare);
    AddHitListSphere(SEARCH, OBJ_KIND_PLAYER, INFINITY_FRAME, skeleton, D3DXVECTOR3(0.0f, 0.0f, 0.0f), g_normalSearchRange, SkeletonStateChangeApproach);
    AddHitListSphere(SEARCH, OBJ_KIND_PLAYER, INFINITY_FRAME, skeleton, D3DXVECTOR3(0.0f, 0.0f, 0.0f), g_normalAttackRange, SkeletonStateChangeAttack);
    //初期モーションの設定
    skeleton->pMeshHandle_ = g_meshHandles[SKELETON];
    ChangeFbxAnimation(&skeleton->animationData_, g_animNameTable[ANIM_SKELETON_MOVE], skeleton->pMeshHandle_);
    //スケルトンの種類の設定
    ((SKELETON_OPTION*)skeleton->option_)->skeKind_ = SKELETON_KIND_NORMAL;
    break;

  //上半身スケルトン
  case SKELETON_KIND_HALF:
    skeleton = AddObjData(OBJ_KIND_SKELETON, pos, g_meshScaleTable[SKELETON], 0);
    //あたり判定の設定
    AddHitListSphere(BODY, 0, INFINITY_FRAME, skeleton, D3DXVECTOR3(0.0f, 0.0f, 0.0f), 40.0f, NULL);
    AddHitListSphere(SEARCH, OBJ_KIND_PLAYER, INFINITY_FRAME, skeleton, D3DXVECTOR3(0.0f, 0.0f, 0.0f), g_halfSearchRange + 100.0f, SkeletonStateChangeIdlePrepare);
    AddHitListSphere(SEARCH, OBJ_KIND_PLAYER, INFINITY_FRAME, skeleton, D3DXVECTOR3(0.0f, 0.0f, 0.0f), g_halfSearchRange, SkeletonStateChangeApproach);
    //初期モーションの設定
    skeleton->pMeshHandle_ = g_meshHandles[SKELETON_HALF];
    ChangeFbxAnimation(&skeleton->animationData_, g_animNameTable[ANIM_HALF_SKELETON_IDLE], skeleton->pMeshHandle_);
    //スケルトンの種類の設定
    ((SKELETON_OPTION*)skeleton->option_)->skeKind_ = SKELETON_KIND_HALF;
    break;
  //装備有スケルトン
  case SKELETON_KIND_ARMOR:
    skeleton = AddObjData(OBJ_KIND_SKELETON, pos, g_meshScaleTable[SKELETON], 0);
    //あたり判定の設定
    AddHitListSphere(BODY, 0, INFINITY_FRAME, skeleton, D3DXVECTOR3(0.0f, 0.0f, 0.0f), 35.0f, NULL);
    AddHitListSphere(SEARCH, OBJ_KIND_PLAYER, INFINITY_FRAME, skeleton, D3DXVECTOR3(0.0f, 0.0f, 0.0f), g_armorSearchRange + 100.0f, SkeletonStateChangeIdlePrepare);
    AddHitListSphere(SEARCH, OBJ_KIND_PLAYER, INFINITY_FRAME, skeleton, D3DXVECTOR3(0.0f, 0.0f, 0.0f), g_armorSearchRange, SkeletonStateChangeApproach);
    AddHitListSphere(SEARCH, OBJ_KIND_PLAYER, INFINITY_FRAME, skeleton, D3DXVECTOR3(0.0f, 0.0f, 0.0f), g_armorAttackRange, SkeletonStateChangeAttack);
    //初期モーションの設定
    skeleton->pMeshHandle_ = g_meshHandles[SKELETON_ARMOR];
    ChangeFbxAnimation(&skeleton->animationData_, g_animNameTable[ANIM_ARMOR_SKELETON_MOVE], skeleton->pMeshHandle_);
    //スケルトンの種類の設定
    ((SKELETON_OPTION*)skeleton->option_)->skeKind_ = SKELETON_KIND_ARMOR;
    break;
  //値が不正なら通常スケルトンとして召喚
  default:
    skeleton = AddObjData(OBJ_KIND_SKELETON, pos, g_meshScaleTable[SKELETON], 0);
    //あたり判定の設定
    AddHitListSphere(BODY, 0, INFINITY_FRAME, skeleton, D3DXVECTOR3(0.0f, 0.0f, 0.0f), 25.0f, NULL);
    AddHitListSphere(SEARCH, OBJ_KIND_PLAYER, INFINITY_FRAME, skeleton, D3DXVECTOR3(0.0f, 0.0f, 0.0f), g_normalSearchRange + 100.0f, SkeletonStateChangeIdlePrepare);
    AddHitListSphere(SEARCH, OBJ_KIND_PLAYER, INFINITY_FRAME, skeleton, D3DXVECTOR3(0.0f, 0.0f, 0.0f), g_normalSearchRange, SkeletonStateChangeApproach);
    AddHitListSphere(SEARCH, OBJ_KIND_PLAYER, INFINITY_FRAME, skeleton, D3DXVECTOR3(0.0f, 0.0f, 0.0f), g_normalAttackRange, SkeletonStateChangeAttack);
    //初期モーションの設定
    skeleton->pMeshHandle_ = g_meshHandles[SKELETON];
    ChangeFbxAnimation(&skeleton->animationData_, g_animNameTable[ANIM_SKELETON_MOVE], skeleton->pMeshHandle_);
    //スケルトンの種類の設定
    ((SKELETON_OPTION*)skeleton->option_)->skeKind_ = SKELETON_KIND_NORMAL;
    break;
  }
  //メモリ確保失敗したら何もせず終わる
  if(skeleton == NULL) { return NULL; }

  //オプション設定
  SKELETON_OPTION* skeletonOp = (SKELETON_OPTION*)skeleton->option_;
  skeletonOp->nowState_ = SKELETON_STATE_IDLE;
  skeletonOp->startPos_ = pos;
  skeletonOp->searchRadius_ = 1000.0f;

  return skeleton;
}

/// <summary>
/// スケルトンの更新
/// </summary>
/// <param name="skeleton">更新したいスケルトンオブジェクト</param>
void UpdateSkeleton(OBJ_DATA* skeleton)
{
  SKELETON_OPTION* skeletonOp = (SKELETON_OPTION*)skeleton->option_;
  //進行時間の更新
  skeletonOp->moveAdvanceTime_ += GetSpeedRate();

  //アニメーションの更新
  UpdateFbxAnimation(&skeleton->animationData_, skeleton->pMeshHandle_, 1.0f);

  ////スケルトンの死亡処理
  //if(skeletonOp->health_ <= 0)
  //{
  //  //TODO:スケルトンの死亡モーションに切り替えるため変更予定
  //  skeletonOp->nowState_ = SKELETON_STATE_DEATH;
  //  skeletonOp->moveAdvanceTime_ = 0;
  //  DeleteSkeleton(skeleton);
  //  return;
  //}

  if(skeletonOp->nowState_ == SKELETON_STATE_DEATH
     && skeletonOp->moveAdvanceTime_ >= GetStatus()->skeletonRevivalTime_)
  {
    skeletonOp->nowState_ = SKELETON_STATE_IDLE;
  }

  if(skeletonOp->nowState_ == SKELETON_STATE_IDLE_PREPARE)
  {
    //待機準備中なら待機にする
    skeletonOp->nowState_ = SKELETON_STATE_IDLE;
    //スケルトンの歩き音を止める
    DSoundStop(g_soundHandles[SKELETON_WALK_SE]);
  }
  if(skeletonOp->nowState_ == SKELETON_STATE_APPROACH)
  {
    //プレイヤーの座標情報の取得
    OBJ_DATA player = GetPlayerData();
    //プレイヤーの方向へ向き設定
    D3DXVECTOR3 dir = player.pos_ - skeleton->pos_;
    skeletonOp->nextYawAngle_ = atan2f(dir.z, dir.x) - PI / 2.0f;
    //スケルトンの振り向き
    skeleton->yawAngle_ = TurningSkeleton(skeleton->yawAngle_, skeletonOp->nextYawAngle_);
    //スケルトンの移動
    D3DXVECTOR3 unitDir = UnitVector(dir);
    //霧濃度の取得
    int fogLevel = 3;
    GetFogInfomation(&fogLevel, NULL, NULL);
    //移動ベクトルの設定
    dir = unitDir * GetStatus()->skeletonVelocity_ * GetSpeedRate() * g_skeletonStatusRate[fogLevel][SPEED_RATE];
    MoveObject(skeleton, dir);
    //オブジェクトの押し出し
    PushObjectCharacter(skeleton);
  }
  if(skeletonOp->nowState_ == SKELETON_STATE_ATTACK)
  {
    //プレイヤーの座標情報の取得
    OBJ_DATA player = GetPlayerData();
    //プレイヤーの方向へ向き設定
    D3DXVECTOR3 dir = player.pos_ - skeleton->pos_;
    skeletonOp->nextYawAngle_ = atan2f(dir.z, dir.x) - PI / 2.0f;
    //スケルトンの振り向き
    skeleton->yawAngle_ = TurningSkeleton(skeleton->yawAngle_, skeletonOp->nextYawAngle_);
    //スケルトンの攻撃
    AttackSkeleton(skeleton);
  }

  //アニメーション切り替え
  SkeletonAnimationChange(skeleton);
  //前フレームの状態を記録
  skeletonOp->prevState_ = skeletonOp->nowState_;
}

/// <summary>
/// スケルトンの消去
/// </summary>
/// <param name="skeleton">消去するスケルトンオブジェクト</param>
void DeleteSkeleton(OBJ_DATA* skeleton)
{
  DeleteListObjData(skeleton);
}

/// <summary>
/// スケルトンの振り向き処理
/// </summary>
/// <param name="nowAngle">現在の向き</param>
/// <param name="nextAngle">振り向き後の目標向き</param>
/// <returns>新しいスケルトンの向き</returns>
float TurningSkeleton(float nowAngle, float nextAngle)
{
  float result = nowAngle;
  float leftOrRight = sinf(nextAngle - nowAngle);//今の向きを基準として次の向きが右か左かを求める　左 0~PI 右0~-PI
  if(nowAngle == nextAngle)
  {
    return result;
  }
  if(leftOrRight > 0)
  {
    //左振り向き
    result += GetStatus()->skeletonTurnSpeed_;
    //振り向きが目標向きを超えた時結果を目標と一致させる
    if(sinf(nextAngle - result) <= 0.0f)
      result = nextAngle;
  }
  else
  {
    //右振り向き
    result -= GetStatus()->skeletonTurnSpeed_;
    //振り向きが目標向きを超えた時結果を目標と一致させる
    if(sinf(nextAngle - result) >= 0.0f)
      result = nextAngle;
  }
  return result;
}

/// <summary>
/// スケルトンの状態変更
/// </summary>
/// <param name="skeleton">状態変更させたいスケルトンオブジェクト</param>
/// <param name="nextState">設定させる状態</param>
void ChangeStateSkeleton(OBJ_DATA* skeleton, int nextState)
{
  SKELETON_OPTION* skeletonOp = (SKELETON_OPTION*)skeleton->option_;
  skeletonOp->moveAdvanceTime_ = 0;
  skeletonOp->nowState_ = (E_SKELETON_STATE)nextState;
}

/// <summary>
/// スケルトンの攻撃
/// </summary>
/// <param name="skeleton">スケルトンオブジェクト</param>
void AttackSkeleton(OBJ_DATA* skeleton)
{
  SKELETON_OPTION* skeletonOp = (SKELETON_OPTION*)skeleton->option_;
  if(skeletonOp->moveAdvanceTime_ == 0)
  {
    //攻撃アニメーションに切り替え
    ChangeFbxAnimation(&skeleton->animationData_, g_animNameTable[ANIM_SKELETON_ATTACK], skeleton->pMeshHandle_);
    skeletonOp->attackFlag_ = 0;
  }
  if(skeletonOp->moveAdvanceTime_ >= 100 && skeletonOp->attackFlag_ == 0)
  {
    //攻撃あたり判定の生成
    D3DXVECTOR3 attackCenterPos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
    //装備スケルトンの場合攻撃範囲を広げる
    if(skeletonOp->skeKind_ == SKELETON_KIND_ARMOR)
    {
      //スケルトンの攻撃音を鳴らす
      DSoundStop(g_soundHandles[SKELETON_ATTACK2_SE]);
      DSoundPlay(g_soundHandles[SKELETON_ATTACK2_SE], false);
      AddHitListFanPillar(ATTACK, OBJ_KIND_PLAYER, 10, skeleton, attackCenterPos, 10.0f, 240.0f, PI / 4.0f, D3DXVECTOR3(cosf(skeleton->yawAngle_ + PI / 2.0f), 0.0f, sinf(skeleton->yawAngle_ + PI / 2.0f)), SkeletonAttackToPlayer);
    }
    else
    {
      //スケルトンの攻撃音を鳴らす
      DSoundStop(g_soundHandles[SKELETON_ATTACK_SE]);
      DSoundPlay(g_soundHandles[SKELETON_ATTACK_SE], false);
      AddHitListFanPillar(ATTACK, OBJ_KIND_PLAYER, 10, skeleton, attackCenterPos, 10.0f, 160.0f, PI / 4.0f, D3DXVECTOR3(cosf(skeleton->yawAngle_ + PI / 2.0f), 0.0f, sinf(skeleton->yawAngle_ + PI / 2.0f)), SkeletonAttackToPlayer);
    }
    skeletonOp->attackFlag_ = 1;
  }
  if (skeletonOp->moveAdvanceTime_ >= 160 && skeletonOp->attackFlag_ == 1)
  {
    ChangeStateSkeleton(skeleton, SKELETON_STATE_IDLE);
    skeletonOp->moveAdvanceTime_ = 0;
    skeletonOp->attackFlag_ = 0;
  }
}

/// <summary>
/// プレイヤーに対する攻撃処理
/// </summary>
/// <param name="skeleton">スケルトンオブジェクト</param>
/// <param name="target">攻撃対象オブジェクト</param>
void SkeletonAttackToPlayer(OBJ_DATA* skeleton, OBJ_DATA* target)
{
  //引数の情報が正しくなければ弾く
  if(skeleton == NULL) { return; }
  if(skeleton->option_ == NULL) { DeleteObject(skeleton); return; }
  if(target == NULL) { return; }
  if(target->option_ == NULL) { DeleteObject(target); return; }

  //オプションアドレスのキャスト
  SKELETON_OPTION* skeletonOp = (SKELETON_OPTION*)skeleton->option_;
  PLAYER_OPTION* playerOp = (PLAYER_OPTION*)target->option_;

  //プレイヤーが無敵時間でなければHPを減らして無敵にする
  if(playerOp->invincibleTime_ <= 0)
  {
    //霧濃度の取得
    int fogLevel = 3;
    GetFogInfomation(&fogLevel, NULL, NULL);
    playerOp->health_ -= GetStatus()->skeletonDamage_ * g_skeletonStatusRate[fogLevel][ATTACK_RATE];
    playerOp->invincibleTime_ = GetStatus()->playerInvincibleTime_;
    //ノックバック情報の設定
    playerOp->knockBackAdvanceTime_ = KNOCK_BACK_MAX_TIME;
    playerOp->knockBackDir_ = UnitVector(skeleton->pos_ - target->pos_);
  }
}

/// <summary>
/// スケルトンの索敵中止処理
/// </summary>
/// <param name="skeleton">スケルトンオブジェクト</param>
/// <param name="target">索敵対象(ダミー)</param>
void SkeletonStateChangeIdlePrepare(OBJ_DATA* skeleton, OBJ_DATA* target)
{
  //待機状態の時接近状態に移行
  SKELETON_OPTION* skeletonOp = (SKELETON_OPTION*)skeleton->option_;
  if(skeletonOp->nowState_ == SKELETON_STATE_IDLE
     || skeletonOp->nowState_ == SKELETON_STATE_APPROACH)
  {
    skeletonOp->nowState_ = SKELETON_STATE_IDLE_PREPARE;
  }
}

/// <summary>
/// スケルトンの索敵成功処理
/// </summary>
/// <param name="skeleton">スケルトンオブジェクト</param>
/// <param name="target">索敵対象(ダミー)</param>
void SkeletonStateChangeApproach(OBJ_DATA* skeleton, OBJ_DATA* target)
{
  //待機状態の時接近状態に移行
  SKELETON_OPTION* skeletonOp = (SKELETON_OPTION*)skeleton->option_;
  if(skeletonOp->nowState_ == SKELETON_STATE_IDLE_PREPARE)
  {
    skeletonOp->nowState_ = SKELETON_STATE_APPROACH;
    //スケルトンの歩き音を鳴らす
    if(DSoundIsStop(g_soundHandles[SKELETON_WALK_SE]))
      ;
      //DSoundPlay(g_soundHandles[SKELETON_WALK_SE], true);
  }
}

/// <summary>
/// スケルトンの攻撃範囲にプレイヤーが入ったときの処理
/// </summary>
/// <param name="skeleton">スケルトンオブジェクト</param>
/// <param name="target">攻撃対象(ダミー)</param>
void SkeletonStateChangeAttack(OBJ_DATA* skeleton, OBJ_DATA* target)
{
  //接近状態時、攻撃状態に移行
  SKELETON_OPTION* skeletonOp = (SKELETON_OPTION*)skeleton->option_;
  if(skeletonOp->nowState_ == SKELETON_STATE_APPROACH)
  {
    ChangeStateSkeleton(skeleton, SKELETON_STATE_ATTACK);
    //スケルトンの歩き音を止める
    DSoundStop(g_soundHandles[SKELETON_WALK_SE]);
  }
}

/// <summary>
/// スケルトンのアニメーション切り替え
/// </summary>
/// <param name="pSkeleton">スケルトンオブジェクト</param>
void SkeletonAnimationChange(OBJ_DATA* pSkeleton)
{
  if(!pSkeleton) { return; }
  if(!pSkeleton->option_) { return; }
  SKELETON_OPTION* skeletonOp = (SKELETON_OPTION*)pSkeleton->option_;
  if(skeletonOp->nowState_ == skeletonOp->prevState_)
  {
    return;
  }

  //通常のスケルトン
  if(skeletonOp->skeKind_ == SKELETON_KIND_NORMAL)
  {
    //状態ごとに使用するアニメーションを変更
    switch(skeletonOp->nowState_)
    {
    case SKELETON_STATE_APPROACH:
      ChangeFbxAnimation(&pSkeleton->animationData_, g_animNameTable[ANIM_SKELETON_MOVE], pSkeleton->pMeshHandle_);
      break;
    case SKELETON_STATE_ATTACK:
      ChangeFbxAnimation(&pSkeleton->animationData_, g_animNameTable[ANIM_SKELETON_ATTACK], pSkeleton->pMeshHandle_);
      break;
    }
  }
  //上半身スケルトン
  if(skeletonOp->skeKind_ == SKELETON_KIND_HALF)
  {
    //状態ごとに使用するアニメーションを変更
    switch(skeletonOp->nowState_)
    {
    case SKELETON_STATE_IDLE:
      ChangeFbxAnimation(&pSkeleton->animationData_, g_animNameTable[ANIM_HALF_SKELETON_IDLE], pSkeleton->pMeshHandle_);
      break;
    case SKELETON_STATE_APPROACH:
      ChangeFbxAnimation(&pSkeleton->animationData_, g_animNameTable[ANIM_HALF_SKELETON_MOVE], pSkeleton->pMeshHandle_);
      break;
    }
  }
  //防具スケルトン
  if(skeletonOp->skeKind_ == SKELETON_KIND_ARMOR)
  {
    //状態ごとに使用するアニメーションを変更
    switch(skeletonOp->nowState_)
    {
    case SKELETON_STATE_APPROACH:
      ChangeFbxAnimation(&pSkeleton->animationData_, g_animNameTable[ANIM_ARMOR_SKELETON_MOVE], pSkeleton->pMeshHandle_);
      break;
    case SKELETON_STATE_ATTACK:
      ChangeFbxAnimation(&pSkeleton->animationData_, g_animNameTable[ANIM_ARMOR_SKELETON_ATTACK], pSkeleton->pMeshHandle_);
      break;
    }
  }
}
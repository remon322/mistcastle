
#include "dropItems.h"
#include "../object.h"
#include "../player/player.h"
#include "../../fog/fog.h"
#include "../../hitJudge/hitJudge.h"
#include "../../camera/camera.h"
#include "../../../00_application/lib/myDx9Lib.h"
#include "../../../00_application/app/app.h"
#include "../../../01_data/sound/dslib.h"
#include "../../../01_data/sound/loadSound.h"
#include "../../../01_data/model/loadModel.h"
#include "../../../01_data/texture/loadTexture.h"
#include "../../../01_data/status/loadStatus.h"

#define EFFECT_ROOP_TIME (120) //アイテムアニメーションのループ時間
#define EFFECT_TOTAL_NUM (40)  //スプライトシートの枚数

//アイテムの消去
void DeleteDropItems(OBJ_DATA* item);
//アイテムを拾った時の効果を実行
void UseDropItem(OBJ_DATA* dropItem, OBJ_DATA* player);

/// <summary>
/// アイテムの生成
/// </summary>
/// <param name="pos_">アイテムを設置する座標</param>
/// <param name="itemID">設置するアイテムの種類ID</param>
/// <returns>生成されたオブジェクトポインタ</returns>
OBJ_DATA* GenerateDropItems(D3DXVECTOR3 pos, int itemID)
{
  OBJ_DATA* item = NULL;
  ITEM_OPTION* itemOp = NULL;

  //アイテムオブジェクトのメモリの確保
  item = AddObjData(OBJ_KIND_DROP_ITEM, pos, g_meshScaleTable[DROP_ITEMS], 0.0f);

  //確保できているか確認
  if(!item) { return NULL; }
  if(!item->option_) { DeleteListObjData(item); return NULL; }

  //アイテムIDを上書き
  itemOp = (ITEM_OPTION*)item->option_;
  itemOp->itemID = itemID;
  itemOp->advanceTime = 0.0f;

  //あたり判定の追加
  AddHitListSphere(ACCESS, OBJ_KIND_PLAYER, INFINITY_FRAME, item, D3DXVECTOR3(0.0f, 0.0f, 0.0f), 50.0f, UseDropItem);

  return item;
}

/// <summary>
/// アイテムの更新
/// </summary>
/// <param name="item">更新するアイテムオブジェクトポインタ</param>
void UpdateDropItems(OBJ_DATA* item)
{
  if(!item) { return; }
  if(!item->option_) { return; }

  ITEM_OPTION* itemOp = (ITEM_OPTION*)item->option_;
  //進行時間の更新
  itemOp->advanceTime += GetSpeedRate();
  //値が大きくなりすぎないようにEFFECT_ROOP_TIME以下に留める
  if(itemOp->advanceTime > EFFECT_ROOP_TIME)
  {
    itemOp->advanceTime -= EFFECT_ROOP_TIME;
  }

  //使用する連番が何枚目かをチェックする
  itemOp->useTexNum = itemOp->advanceTime / (EFFECT_ROOP_TIME / EFFECT_TOTAL_NUM);
}

/// <summary>
/// ドロップアイテムの描画
/// </summary>
/// <param name="item">描画するアイテムオブジェクトポインタ</param>
void RenderDropItems(OBJ_DATA* item)
{
  if(item->kind_ == OBJ_KIND_DROP_ITEM)
  {
    //霧の濃度取得
    float farFog = 0;
    GetFogInfomation(NULL, NULL, &farFog);
    //霧の最大濃さより遠くにあればはじく
    if(GetDistance(item->pos_, GetCamera().vEyePt_) > farFog)
      return;

    //HPポーションの描画
    if(((ITEM_OPTION*)item->option_)->itemID == ITEM_HEAL_POTION)
      RenderBlockBillboardRot(item->pos_, GetCamera().vEyePt_, 0.0f, g_textureHandles[DROP_ITEM_HP], 255, ((ITEM_OPTION*)item->option_)->useTexNum);
    //SPポーションの描画
    else
      RenderBlockBillboardRot(item->pos_, GetCamera().vEyePt_, 0.0f, g_textureHandles[DROP_ITEM_SP], 255, ((ITEM_OPTION*)item->option_)->useTexNum);
  }
}

/// <summary>
/// アイテムの消去
/// </summary>
/// <param name="item">消去したいアイテムオブジェクトポインタ</param>
void DeleteDropItems(OBJ_DATA* item)
{
  DeleteListObjData(item);
}

/// <summary>
/// アイテムを拾った時の効果を実行
/// </summary>
/// <param name="dropItem">使用されたアイテムオブジェクトポインタ</param>
/// <param name="player">使用したプレイヤーオブジェクトポインタ</param>
void UseDropItem(OBJ_DATA* dropItem, OBJ_DATA* player)
{
  //情報が不正であればはじく
  if(dropItem == NULL || player == NULL) { return; }

  //アイテム入手音を鳴らす
  DSoundStop(g_soundHandles[ITEM_GET_SE]);
  DSoundPlay(g_soundHandles[ITEM_GET_SE], false);

  ITEM_OPTION* itemOp = (ITEM_OPTION*)dropItem->option_;
  PLAYER_OPTION* playerOp = (PLAYER_OPTION*)player->option_;
  switch(itemOp->itemID)
  {
  case ITEM_HEAL_POTION:
    //HPポーション使用
    playerOp->health_ = CLUMP(0, playerOp->health_ + GetStatus()->hpHealValue_, playerOp->healthMax_);
    break;
  case ITEM_STAMINA_POTION:
    //SPポーション使用
    playerOp->staminaInfinityTime_ = 60 * 10;//60フレーム * 10秒
    break;
  }
  //アイテムを削除する
  DeleteListObjData(dropItem);
}
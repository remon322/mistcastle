#include "../camera/camera.h"
#include "../fog/fog.h"
#include "../object/object.h"
#include "../object/player/player.h"
#include "../object/treasure/treasure.h"
#include "../../00_application/app/app.h"
#include "../../00_application/lib/myDx9Lib.h"
#include "../../01_data/texture/loadTexture.h"

#define HP_X (103)
#define HP_Y (38)
#define SP_X (103)
#define SP_Y (81)
#define LEFT_WIND_X (540)
#define LEFT_WIND_Y (330)
#define TENS_PLACE_X (652)
#define TENS_PLACE_Y (340)
#define ONES_PLACE_X (675)
#define ONES_PLACE_Y (340)

static int g_healthMax  = 0;        //プレイヤーのHP最大値
static int g_health     = 0;        //プレイヤーのHP
static int g_magicPoint = 0;        //プレイヤーのMP
static int g_fogLevel = 1;          //霧の濃さレベル
static int g_healthGuageShave = 0;  //HPゲージの削るpix数
static int g_staminaGuageShave = 0; //SPゲージの削るpix数
static int g_treasureTensPlaceNum;  //のこり財宝数の10の位の数字
static int g_treasureOnesPlaceNum;  //のこり財宝数の1の位の数字

/// <summary>
/// UIの情報の更新
/// </summary>
void UpdateUserInterface()
{
  OBJ_DATA player = GetPlayerData();
  if(!player.option_) { return; }
  PLAYER_OPTION* playerOp = (PLAYER_OPTION*)player.option_;

  //描画用データの更新(プレイヤーのデータから参照)
  g_healthMax = playerOp->healthMax_;
  g_health = playerOp->health_;
  g_magicPoint = CLUMP(0, playerOp->magicPoint_, 3);

  //霧の濃さ取得
  GetFogInfomation(&g_fogLevel, NULL, NULL);
  g_fogLevel = CLUMP(0, g_fogLevel, 4);

  //ゲージの削るpix数計算
  g_healthGuageShave = 360.0f * (1.0f - ((float)playerOp->health_ / (float)playerOp->healthMax_));
  g_staminaGuageShave = 245.0f * (1.0f - ((float)playerOp->stamina_ / (float)playerOp->staminaMax_));

  //残りの宝の数
  int treasureNum = GetTreasureNum();
  g_treasureTensPlaceNum = treasureNum / 10;
  g_treasureOnesPlaceNum = treasureNum % 10;
}

/// <summary>
/// UIの描画
/// </summary>
void RenderUserInterface()
{
  //UI背景
  RenderBlockTexture(0, 0, g_textureHandles[UI_BASE], 255, timeGetTime() / (1000 / 6) % 6);
  //HP
  RenderTextureShave(HP_X, HP_Y, g_textureHandles[UI_HP_COLOR], 255, g_healthGuageShave, 0);
  RenderTexture(0, 0, g_textureHandles[UI_HP_FRAME], 255);
  //SP
  RenderTextureShave(SP_X, SP_Y, g_textureHandles[UI_SP_COLOR], 255, g_staminaGuageShave, 0);
  RenderTexture(0, 0, g_textureHandles[UI_SP_FRAME], 255);
  //スキル
  RenderBlockTexture(0, 0, g_textureHandles[UI_SKILL_MINUS], 255, 0);
  RenderBlockTexture(0, 0, g_textureHandles[UI_SKILL_PLUS], 255, 0);
  //霧の濃さ
  RenderBlockTexture(0, 0, g_textureHandles[UI_MIST_LEVEL], 255, g_fogLevel);
  //残りの宝数
  RenderTexture(LEFT_WIND_X, LEFT_WIND_Y, g_textureHandles[LEFT_WINDOW], 255);
  RenderBlockTexture(TENS_PLACE_X, TENS_PLACE_Y, g_textureHandles[NUMBERS], 255, g_treasureTensPlaceNum);
  RenderBlockTexture(ONES_PLACE_X, ONES_PLACE_Y, g_textureHandles[NUMBERS], 255, g_treasureOnesPlaceNum);
  

  RenderBlockBillboardShiftRot(D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(0, 0, 500), GetCamera().vEyePt_, 0.0f, g_textureHandles[UI_HP_FRAME], 255, 0);
  RenderBlockBillboardShiftRot(D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(0, 0, 300), GetCamera().vEyePt_, 0.0f, g_textureHandles[UI_HP_FRAME], 255, 0);
  RenderBlockBillboardShiftRot(D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(0, 0, 100), GetCamera().vEyePt_, 0.0f, g_textureHandles[UI_HP_FRAME], 255, 0);
  RenderBlockBillboardShiftRot(D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(0, 0, 0), GetCamera().vEyePt_, 0.0f, g_textureHandles[UI_HP_FRAME], 255, 0);
  RenderBlockBillboardShiftRot(D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(0, 0, -500), GetCamera().vEyePt_, 0.0f, g_textureHandles[UI_HP_FRAME], 255, 0);
  RenderBlockBillboardShiftRot(D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(0, 0, -300), GetCamera().vEyePt_, 0.0f, g_textureHandles[UI_HP_FRAME], 255, 0);
  RenderBlockBillboardShiftRot(D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(0, 0, -100), GetCamera().vEyePt_, 0.0f, g_textureHandles[UI_HP_FRAME], 255, 0);

  RenderBlockBillboardShiftRot(D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(500, 0, 0), GetCamera().vEyePt_, 0.0f, g_textureHandles[UI_HP_FRAME], 255, 0);
  RenderBlockBillboardShiftRot(D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(300, 0, 0), GetCamera().vEyePt_, 0.0f, g_textureHandles[UI_HP_FRAME], 255, 0);
  RenderBlockBillboardShiftRot(D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(-300, 0, 0), GetCamera().vEyePt_, 0.0f, g_textureHandles[UI_HP_FRAME], 255, 0);
  RenderBlockBillboardShiftRot(D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(-500, 0, 0), GetCamera().vEyePt_, 0.0f, g_textureHandles[UI_HP_FRAME], 255, 0);

  RenderBlockBillboardShiftRot(D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(0, 500, 0), GetCamera().vEyePt_, 0.0f, g_textureHandles[UI_HP_FRAME], 255, 0);
  RenderBlockBillboardShiftRot(D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(0, 300, 0), GetCamera().vEyePt_, 0.0f, g_textureHandles[UI_HP_FRAME], 255, 0);

  int aaa = timeGetTime() / 200 % 20;
  RenderBlockBillboardShiftRot(D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(0, 0, 0), GetCamera().vEyePt_, 0.0f, g_textureHandles[FOG], 255, aaa);
  RenderBlockBillboardShiftRot(D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(10, 0, -100), GetCamera().vEyePt_, 0.0f, g_textureHandles[FOG], 255, aaa + 3);
  //アイテム欄
  //RenderTexture(980, 0, g_textureHandles[UI_ITEM_CHOOSE_BASE], 255);
  //RenderTexture(980, 0, g_textureHandles[UI_ITEM_CHOOSE_UP], 255);
  //RenderBlockTexture(980, 0, g_textureHandles[UI_ITEM_CHOOSE_LEFT], 255, 0);
  //RenderBlockTexture(980, 0, g_textureHandles[UI_ITEM_CHOOSE_RIGHT], 255, 1);
}
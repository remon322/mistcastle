#include "../title/title.h"
#include "../gameover/gameover.h"
#include "../../01_data/sound/dslib.h"
#include "../../00_application/app/app.h"
#include "../../01_data/model/loadModel.h"
#include "../../01_data/sound/loadSound.h"
#include "../../03_update/update.h"
#include "../../05_inGame/camera/camera.h"
#include "../../05_inGame/object/object.h"
#include "../../05_inGame/hitJudge/hitJudge.h"
#include "../../05_inGame/userInterface/userInterface.h"

/// <summary>
/// ポーズ画面の初期化
/// </summary>
void InitPause()
{
  ReleaseIngame();
  InitCamera();
  InitObject();
  DSoundPlay(g_soundHandles[INGAME_BGM], TRUE);
}

/// <summary>
/// ポーズ画面の更新
/// </summary>
void UpdatePause()
{

}

/// <summary>
/// ポーズ画面の描画
/// </summary>
void RenderPause()
{
  //オブジェクトとUIの表示
  RenderObject();
  RenderUserInterface();

  Render2DBox(0,0,1280,720,0xffffff00);
}
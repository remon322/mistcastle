#include "../../00_application/lib/myDx9Lib.h"
#include "../../00_application/app/app.h"
#include "../../01_data/texture/loadTexture.h"
#include "../../01_data/sound/loadSound.h"
#include "../../01_data/sound/dslib.h"
#include "../../02_input/input.h"

//タイトルのカーソル識別子
enum TITLE_CURSOR
{
  CURSOR_START = 0,
  CURSOR_END = 1,
};

static int g_titleTime = 0;         //タイトルの経過時間
static int g_titleCursor = 0;       //タイトルのカーソル位置
static int g_titleUnderLineNum = 0; //アンダーラインのスプライトの現在枚数

/// <summary>
/// タイトル画面の初期化
/// </summary>
void InitTitle()
{
  g_titleUnderLineNum = 0;
  g_titleTime = 0;
  g_titleCursor = CURSOR_START;
  DSoundPlay(g_soundHandles[TITLE_BGM], TRUE);
}

/// <summary>
/// タイトル画面の更新
/// </summary>
void UpdateTitle()
{
  KEY_BOARD keyTrg = GetTrgInKeyBoard();
  JOY_STICK joyTrg = GetTrgInJoyStickButton();

  //カーソルの移動
  if(keyTrg.up || joyTrg.up)
  {
    g_titleCursor = max(0, g_titleCursor - 1);
    g_titleUnderLineNum = 0;
    g_titleTime = 0;
    //カーソル移動音を鳴らす
    DSoundStop(g_soundHandles[MENU_CHOOSE_SE]);
    DSoundPlay(g_soundHandles[MENU_CHOOSE_SE], false);
  }
  if(keyTrg.down || joyTrg.down)
  {
    g_titleCursor = min(g_titleCursor + 1, CURSOR_END);
    g_titleUnderLineNum = 0;
    g_titleTime = 0;
    //カーソル移動音を鳴らす
    DSoundStop(g_soundHandles[MENU_CHOOSE_SE]);
    DSoundPlay(g_soundHandles[MENU_CHOOSE_SE], false);
  }

  //決定ボタンが押された
  if(keyTrg.rEnter || keyTrg.space || joyTrg.button[1])
  {
    //決定音を鳴らす
    DSoundPlay(MENU_DECIDE_SE, false);
    //オープニングに進む
    if(g_titleCursor == CURSOR_START)
    {
      ChangeGameState(GAME_STATE_OPENING);
    }
    //ゲームを終了する
    else if(g_titleCursor == CURSOR_END)
    {
      ChangeGameState(GAME_STATE_END);
    }
  }

  // 1/4秒で7枚分の連番を回す
  if(g_titleTime % (60 / (4 * 7)) == 0)
    g_titleUnderLineNum = min(g_titleUnderLineNum + 1, 6);

  g_titleTime++;
}

/// <summary>
/// タイトル画面の描画
/// </summary>
void RenderTitle()
{
  //タイトル背景の描画
  RenderTexture(0, 0, g_textureHandles[TITLE_BACK], 255);
  //タイトルロゴの描画
  RenderTexture(620, 0, g_textureHandles[TITLE_LOGO], 255);

  //スタート、エンドボタンの描画
  const int titleStartX = 790;
  const int titleStartY = 360;
  const int titleEndX = 790;
  const int titleEndY = 540;
  RenderBlockTexture(titleStartX, titleStartY, g_textureHandles[TITLE_START], 255, g_titleCursor);
  RenderBlockTexture(titleEndX, titleEndY, g_textureHandles[TITLE_END], 255, g_titleCursor);

  //アンダーラインの描画
  if(g_titleCursor == CURSOR_START)
    RenderBlockTexture(titleStartX, titleStartY + 130, g_textureHandles[UNDER_SIGN], 255, g_titleUnderLineNum);
  else if(g_titleCursor == CURSOR_END)
    RenderBlockTexture(titleEndX, titleEndY + 130, g_textureHandles[UNDER_SIGN], 255, g_titleUnderLineNum);
}
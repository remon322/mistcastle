#pragma once

//タイトル画面の初期化
void InitTitle();

//タイトル画面の更新
void UpdateTitle();

//タイトル画面の描画
void RenderTitle();
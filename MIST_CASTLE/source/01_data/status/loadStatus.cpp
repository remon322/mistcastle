#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <io.h>
#include "loadStatus.h"

//変数名を文字列に変換する
#define CONV_STR(str) #str
//読み込んだデータを格納する
//strとmemberが対応していない場合はスルー
#define DATA_READ(str,data,status,member)\
  if (strcmp(str, CONV_STR(member)) == 0)\
    status.member = data;
  
//情報管理用変数
static struct STATUS g_status;

void DefaultStatus(STATUS* status);
void CreateStatusFormat();

/// <summary>
/// 情報の取得
/// </summary>
/// <returns>情報</returns>
STATUS* GetStatus()
{
  return &g_status;
}

/// <summary>
/// 外部ファイルから情報の読み込み
/// </summary>
void LoadStatus()
{
  DefaultStatus(&g_status);
  FILE* fp = NULL;
  fp = fopen("config.txt", "r");
  //ファイルを開けなかったので作る
  if (fp == NULL){
    CreateStatusFormat();
    fp = fopen("config.txt", "r");
  }

  //g_statusにデータを読み込み格納
  char infoName[32];
  float data = 0;
  while (fscanf(fp, "%[^,],%f;\n", infoName, &data) != EOF)
  {
    DATA_READ(infoName, data, g_status, pageFlickTime_);
    DATA_READ(infoName, data, g_status, fogNear1_);
    DATA_READ(infoName, data, g_status, fogNear2_);
    DATA_READ(infoName, data, g_status, fogNear3_);
    DATA_READ(infoName, data, g_status, fogNear4_);
    DATA_READ(infoName, data, g_status, fogNear5_);
    DATA_READ(infoName, data, g_status, fogFar1_);
    DATA_READ(infoName, data, g_status, fogFar2_);
    DATA_READ(infoName, data, g_status, fogFar3_);
    DATA_READ(infoName, data, g_status, fogFar4_);
    DATA_READ(infoName, data, g_status, fogFar5_);
    DATA_READ(infoName, data, g_status, hpHealValue_);
    DATA_READ(infoName, data, g_status, spHealValue_);
    DATA_READ(infoName, data, g_status, playerVelocity_);
    DATA_READ(infoName, data, g_status, playerDashRate_);
    DATA_READ(infoName, data, g_status, playerSpLost_);
    DATA_READ(infoName, data, g_status, playerJumpTime_);
    DATA_READ(infoName, data, g_status, playerJumpHeight_);
    DATA_READ(infoName, data, g_status, playerTurnSpeed_);
    DATA_READ(infoName, data, g_status, playerInvincibleTime_);
    DATA_READ(infoName, data, g_status, cameraRoteSpeed_);
    DATA_READ(infoName, data, g_status, mistCompLightRange_);
    DATA_READ(infoName, data, g_status, alterMoveDistance_);
    DATA_READ(infoName, data, g_status, alterMoveTime_);
    DATA_READ(infoName, data, g_status, skeletonVelocity_);
    DATA_READ(infoName, data, g_status, skeletonRevivalTime_);
    DATA_READ(infoName, data, g_status, skeletonTurnSpeed_);
    DATA_READ(infoName, data, g_status, skeletonDamage_);
    DATA_READ(infoName, data, g_status, skeletonSpeedRate1_);
    DATA_READ(infoName, data, g_status, skeletonSpeedRate2_);
    DATA_READ(infoName, data, g_status, skeletonSpeedRate3_);
    DATA_READ(infoName, data, g_status, skeletonSpeedRate4_);
    DATA_READ(infoName, data, g_status, skeletonSpeedRate5_);
    DATA_READ(infoName, data, g_status, skeletonAttackRate1_);
    DATA_READ(infoName, data, g_status, skeletonAttackRate2_);
    DATA_READ(infoName, data, g_status, skeletonAttackRate3_);
    DATA_READ(infoName, data, g_status, skeletonAttackRate4_);
    DATA_READ(infoName, data, g_status, skeletonAttackRate5_);
    DATA_READ(infoName, data, g_status, skeletonSearchRate1_);
    DATA_READ(infoName, data, g_status, skeletonSearchRate2_);
    DATA_READ(infoName, data, g_status, skeletonSearchRate3_);
    DATA_READ(infoName, data, g_status, skeletonSearchRate4_);
    DATA_READ(infoName, data, g_status, skeletonSearchRate5_);
    DATA_READ(infoName, data, g_status, normalSkeletonSearchRange_);
    DATA_READ(infoName, data, g_status, normalSkeletonAttackRange_);
    DATA_READ(infoName, data, g_status, halfSkeletonSearchRange_);
    DATA_READ(infoName, data, g_status, armorSkeletonSearchRange_);
    DATA_READ(infoName, data, g_status, armorSkeletonAttackRange_);
    DATA_READ(infoName, data, g_status, spiderwebSlowRate_);
    DATA_READ(infoName, data, g_status, launcherCoolTime_);
    DATA_READ(infoName, data, g_status, arrowDamage_);
    DATA_READ(infoName, data, g_status, spearDamage_);
    DATA_READ(infoName, data, g_status, trapSpearDelayTime_);
    DATA_READ(infoName, data, g_status, spearPrepareTime_);
  }

  fclose(fp);
  return;
}

/// <summary>
/// 情報ファイルのフォーマットを作成
/// </summary>
void CreateStatusFormat()
{
  FILE* fp = NULL;
  fp = fopen("config.txt", "w");
  if (fp == NULL)
  {
    return;
  }

  STATUS st;
  DefaultStatus(&st);
  fprintf(fp, "%s,%d;\n", CONV_STR(pageFlickTime_), st.pageFlickTime_);
  fprintf(fp, "%s,%f;\n", CONV_STR(fogNear1_), st.fogNear1_);
  fprintf(fp, "%s,%f;\n", CONV_STR(fogNear2_), st.fogNear2_);
  fprintf(fp, "%s,%f;\n", CONV_STR(fogNear3_), st.fogNear3_);
  fprintf(fp, "%s,%f;\n", CONV_STR(fogNear4_), st.fogNear4_);
  fprintf(fp, "%s,%f;\n", CONV_STR(fogNear5_), st.fogNear5_);
  fprintf(fp, "%s,%f;\n", CONV_STR(fogFar1_), st.fogFar1_);
  fprintf(fp, "%s,%f;\n", CONV_STR(fogFar2_), st.fogFar2_);
  fprintf(fp, "%s,%f;\n", CONV_STR(fogFar3_), st.fogFar3_);
  fprintf(fp, "%s,%f;\n", CONV_STR(fogFar4_), st.fogFar4_);
  fprintf(fp, "%s,%f;\n", CONV_STR(fogFar5_), st.fogFar5_);
  fprintf(fp, "%s,%d;\n", CONV_STR(hpHealValue_), st.hpHealValue_);
  fprintf(fp, "%s,%d;\n", CONV_STR(spHealValue_), st.spHealValue_);
  fprintf(fp, "%s,%f;\n", CONV_STR(playerVelocity_), st.playerVelocity_);
  fprintf(fp, "%s,%f;\n", CONV_STR(playerDashRate_), st.playerDashRate_);
  fprintf(fp, "%s,%f;\n", CONV_STR(playerSpLost_), st.playerSpLost_);
  fprintf(fp, "%s,%d;\n", CONV_STR(playerJumpTime_), st.playerJumpTime_);
  fprintf(fp, "%s,%f;\n", CONV_STR(playerJumpHeight_), st.playerJumpHeight_);
  fprintf(fp, "%s,%f;\n", CONV_STR(playerTurnSpeed_), st.playerTurnSpeed_);
  fprintf(fp, "%s,%d;\n", CONV_STR(playerInvincibleTime_), st.playerInvincibleTime_);
  fprintf(fp, "%s,%f;\n", CONV_STR(cameraRoteSpeed_), st.cameraRoteSpeed_);
  fprintf(fp, "%s,%f;\n", CONV_STR(mistCompLightRange_), st.mistCompLightRange_);
  fprintf(fp, "%s,%f;\n", CONV_STR(alterMoveDistance_), st.alterMoveDistance_);
  fprintf(fp, "%s,%d;\n", CONV_STR(alterMoveTime_), st.alterMoveTime_);
  fprintf(fp, "%s,%f;\n", CONV_STR(skeletonVelocity_), st.skeletonVelocity_);
  fprintf(fp, "%s,%d;\n", CONV_STR(skeletonRevivalTime_), st.skeletonRevivalTime_);
  fprintf(fp, "%s,%f;\n", CONV_STR(skeletonTurnSpeed_), st.skeletonTurnSpeed_);
  fprintf(fp, "%s,%f;\n", CONV_STR(skeletonDamage_), st.skeletonDamage_);
  fprintf(fp, "%s,%f;\n", CONV_STR(skeletonSpeedRate1_), st.skeletonSpeedRate1_);
  fprintf(fp, "%s,%f;\n", CONV_STR(skeletonSpeedRate2_), st.skeletonSpeedRate2_);
  fprintf(fp, "%s,%f;\n", CONV_STR(skeletonSpeedRate3_), st.skeletonSpeedRate3_);
  fprintf(fp, "%s,%f;\n", CONV_STR(skeletonSpeedRate4_), st.skeletonSpeedRate4_);
  fprintf(fp, "%s,%f;\n", CONV_STR(skeletonSpeedRate5_), st.skeletonSpeedRate5_);
  fprintf(fp, "%s,%f;\n", CONV_STR(skeletonAttackRate1_), st.skeletonAttackRate1_);
  fprintf(fp, "%s,%f;\n", CONV_STR(skeletonAttackRate2_), st.skeletonAttackRate2_);
  fprintf(fp, "%s,%f;\n", CONV_STR(skeletonAttackRate3_), st.skeletonAttackRate3_);
  fprintf(fp, "%s,%f;\n", CONV_STR(skeletonAttackRate4_), st.skeletonAttackRate4_);
  fprintf(fp, "%s,%f;\n", CONV_STR(skeletonAttackRate5_), st.skeletonAttackRate5_);
  fprintf(fp, "%s,%f;\n", CONV_STR(skeletonSearchRate1_), st.skeletonSearchRate1_);
  fprintf(fp, "%s,%f;\n", CONV_STR(skeletonSearchRate2_), st.skeletonSearchRate2_);
  fprintf(fp, "%s,%f;\n", CONV_STR(skeletonSearchRate3_), st.skeletonSearchRate3_);
  fprintf(fp, "%s,%f;\n", CONV_STR(skeletonSearchRate4_), st.skeletonSearchRate4_);
  fprintf(fp, "%s,%f;\n", CONV_STR(skeletonSearchRate5_), st.skeletonSearchRate5_);
  fprintf(fp, "%s,%f;\n", CONV_STR(normalSkeletonSearchRange_), st.normalSkeletonSearchRange_);
  fprintf(fp, "%s,%f;\n", CONV_STR(normalSkeletonAttackRange_), st.normalSkeletonAttackRange_);
  fprintf(fp, "%s,%f;\n", CONV_STR(halfSkeletonSearchRange_), st.halfSkeletonSearchRange_);
  fprintf(fp, "%s,%f;\n", CONV_STR(armorSkeletonSearchRange_), st.armorSkeletonSearchRange_);
  fprintf(fp, "%s,%f;\n", CONV_STR(armorSkeletonAttackRange_), st.armorSkeletonAttackRange_);
  fprintf(fp, "%s,%f;\n", CONV_STR(spiderwebSlowRate_), st.spiderwebSlowRate_);
  fprintf(fp, "%s,%d;\n", CONV_STR(launcherCoolTime_), st.launcherCoolTime_);
  fprintf(fp, "%s,%d;\n", CONV_STR(arrowDamage_), st.arrowDamage_);
  fprintf(fp, "%s,%d;\n", CONV_STR(spearDamage_), st.spearDamage_);
  fprintf(fp, "%s,%d;\n", CONV_STR(trapSpearDelayTime_), st.trapSpearDelayTime_);
  fprintf(fp, "%s,%d;\n", CONV_STR(spearPrepareTime_), st.spearPrepareTime_);

  fclose(fp);
}

/// <summary>
/// 情報の初期設定化
/// </summary>
/// <param name="status">(out)初期設定化したいSTATUS変数</param>
void DefaultStatus(STATUS* status)
{
  status->pageFlickTime_ = 45;
  status->fogNear1_ = 1000.0f;
  status->fogNear2_ = 900.0f;
  status->fogNear3_ = 800.0f;
  status->fogNear4_ = 700.0f;
  status->fogNear5_ = 600.0f;
  status->fogFar1_ = 2000.0f;
  status->fogFar2_ = 1800.0f;
  status->fogFar3_ = 1600.0f;
  status->fogFar4_ = 1500.0f;
  status->fogFar5_ = 1400.0f;
  status->hpHealValue_ = 30;
  status->spHealValue_ = 30;
  status->playerVelocity_ = 5.0f;
  status->playerDashRate_ = 1.5f;
  status->playerSpLost_   = 0.5f;
  status->playerJumpTime_ = 60;
  status->playerJumpHeight_ = 3.0f;
  status->playerTurnSpeed_ = 3.1415f / 10.0f;
  status->playerInvincibleTime_ = 30;
  status->cameraRoteSpeed_ = 3.1415f / 45.0f;
  status->mistCompLightRange_ = 700.0f;
  status->alterMoveDistance_ = 2.0f;
  status->alterMoveTime_ = 180;
  status->skeletonVelocity_ = 2.0f;
  status->skeletonRevivalTime_ = 600;
  status->skeletonTurnSpeed_ = 3.1415f / 10.0f;
  status->skeletonDamage_ = 30.0f;
  status->skeletonSpeedRate1_  = 1.4f;
  status->skeletonSpeedRate2_  = 1.2f;
  status->skeletonSpeedRate3_  = 1.0f;
  status->skeletonSpeedRate4_  = 0.8f;
  status->skeletonSpeedRate5_  = 0.6f;
  status->skeletonAttackRate1_ = 1.8f;
  status->skeletonAttackRate2_ = 1.4f;
  status->skeletonAttackRate3_ = 1.0f;
  status->skeletonAttackRate4_ = 0.6f;
  status->skeletonAttackRate5_ = 0.4f;
  status->skeletonSearchRate1_ = 1.4f;
  status->skeletonSearchRate2_ = 1.2f;
  status->skeletonSearchRate3_ = 1.0f;
  status->skeletonSearchRate4_ = 0.7f;
  status->skeletonSearchRate5_ = 0.4f;
  status->normalSkeletonSearchRange_ = 1000.0f;
  status->normalSkeletonAttackRange_ = 125.0f;
  status->halfSkeletonSearchRange_   = 750.0f;
  status->armorSkeletonSearchRange_ = 1000.0f;
  status->armorSkeletonAttackRange_ = 140.0f;
  status->spiderwebSlowRate_ = 0.6f;
  status->launcherCoolTime_ = 60;
  status->arrowDamage_ = 30;
  status->spearDamage_ = 40;
  status->trapSpearDelayTime_ = 60;
  status->spearPrepareTime_ = 120;
}
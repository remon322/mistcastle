#pragma once

//情報
struct STATUS
{
  int   pageFlickTime_;       //OPとEDのページをめくるのにかかる時間
  float fogNear1_;            //フォグの開始距離
  float fogNear2_;            //フォグの開始距離
  float fogNear3_;            //フォグの開始距離
  float fogNear4_;            //フォグの開始距離
  float fogNear5_;            //フォグの開始距離
  float fogFar1_;             //フォグの終点距離
  float fogFar2_;             //フォグの終点距離
  float fogFar3_;             //フォグの終点距離
  float fogFar4_;             //フォグの終点距離
  float fogFar5_;             //フォグの終点距離
  int   hpHealValue_;         //HP回復アイテムの回復量
  int   spHealValue_;         //SP回復アイテムの回復量
  float playerVelocity_;      //プレイヤーの移動速度
  float playerDashRate_;      //プレイヤーのダッシュ時の速度倍率
  float playerSpLost_;        //プレイヤーがダッシュ時に1fあたり消費するスタミナ量
  int   playerJumpTime_;      //プレイヤーのジャンプにかかる時間
  float playerJumpHeight_;    //プレイヤーのジャンプの高さ
  float playerTurnSpeed_;     //プレイヤーの振り向き時間
  int   playerInvincibleTime_;//プレイヤーの無敵時間
  float cameraRoteSpeed_;     //カメラの回転時間
  float mistCompLightRange_;  //ミストコンプレッサーの光の届く距離
  float alterMoveDistance_;   //祭壇の移動距離
  int   alterMoveTime_;       //祭壇の移動時間
  float skeletonVelocity_;    //スケルトンの移動速度
  int   skeletonRevivalTime_; //スケルトンの復活までの時間
  float skeletonTurnSpeed_;   //プレイヤーの振り向き時間
  float skeletonDamage_;      //スケルトンがあたえるダメージ量
  float skeletonSpeedRate1_;  //スケルトンの移動速度倍率
  float skeletonSpeedRate2_;  //スケルトンの移動速度倍率
  float skeletonSpeedRate3_;  //スケルトンの移動速度倍率
  float skeletonSpeedRate4_;  //スケルトンの移動速度倍率
  float skeletonSpeedRate5_;  //スケルトンの移動速度倍率
  float skeletonAttackRate1_; //スケルトンの攻撃力倍率
  float skeletonAttackRate2_; //スケルトンの攻撃力倍率
  float skeletonAttackRate3_; //スケルトンの攻撃力倍率
  float skeletonAttackRate4_; //スケルトンの攻撃力倍率
  float skeletonAttackRate5_; //スケルトンの攻撃力倍率
  float skeletonSearchRate1_; //スケルトンの索敵範囲倍率
  float skeletonSearchRate2_; //スケルトンの索敵範囲倍率
  float skeletonSearchRate3_; //スケルトンの索敵範囲倍率
  float skeletonSearchRate4_; //スケルトンの索敵範囲倍率
  float skeletonSearchRate5_; //スケルトンの索敵範囲倍率
  float normalSkeletonSearchRange_; //通常スケルトンの索敵範囲
  float normalSkeletonAttackRange_; //通常スケルトンの攻撃範囲
  float halfSkeletonSearchRange_;   //上半身スケルトンの索敵範囲
  float armorSkeletonSearchRange_;  //装備有スケルトンの索敵範囲
  float armorSkeletonAttackRange_;  //装備有スケルトンの攻撃範囲
  float spiderwebSlowRate_;   //蜘蛛の巣にかかったときの移動速度倍率
  int   launcherCoolTime_;    //ランチャーの発射クールタイム
  int   arrowDamage_;         //矢に当たったときのダメージ量
  int   spearDamage_;         //槍に当たったときのダメージ量
  int   trapSpearDelayTime_;  //罠槍の撃ち出し遅延時間
  int   spearPrepareTime_;    //槍の撃ち出し準備時間
};

//情報の取得
STATUS* GetStatus();

//情報の読み込み
void LoadStatus();
#include <stdio.h>
#include "stageEdit.h"
#include "../model/loadModel.h"
#include "../../00_application/lib/myDx9Lib.h"
#include "../../00_application/app/app.h"
#include "../../02_input/input.h"
#include "../../05_ingame/object/object.h"
#include "../../05_ingame/object/player/player.h"
#include "../../05_ingame/object/skeleton/skeleton.h"
#include "../../05_ingame/object/spear/autoSpear.h"
#include "../../05_ingame/object/spear/trapSpear.h"
#include "../../05_ingame/object/launcher/launcher.h"
#include "../../05_ingame/object/spiderweb/spiderweb.h"
#include "../../05_ingame/object/dropItem/dropItems.h"
#include "../../05_ingame/object/mistCompressor/mistCompressor.h"
#include "../../05_ingame/object/altar/altar.h"
#include "../../05_ingame/object/lightSource/lightSource.h"
#include "../../05_ingame/object/treasure/treasure.h"

static OBJ_DATA* g_obj = NULL;
static int g_objNum = 0;            //オブジェクトの個数
static int g_objIndex = 0;          //操作中のオブジェクトの指数
static int g_placeObjIndex = 0;     //設置するオブジェクト指数
static int g_objInfoIndex = 0;      //オブジェクトの詳細指数
static int g_mapNameIndex = 0;      //現在のマップ指数
static int g_prevMapNameIndex = -5; //1つ前のマップ指数
const static int g_objTableMax = 10; //エディットで設置可能なオブジェクトの種類数
const static int g_stageMax = 2;    //ステージ数の最大個数
char* g_mapDataDirectry[g_stageMax][3] =
{
  //オブジェクトの配置, マップの見た目, あたり判定用マップ
  { "data/map1.dat", "res/fbx/map/map1/map1low.fbx", "res/fbx/map/map1/map1_atari.fbx"},
  { "data/map2.dat", "res/fbx/map/map2/map2.fbx"   , "res/fbx/map/map2/map2_atari.fbx"},
};

//設置可能オブジェクトの指数テーブル
static OBJ_KIND g_placeObjTable[g_objTableMax] = {
  OBJ_KIND_SKELETON,
  OBJ_KIND_DROP_ITEM,
  OBJ_KIND_MIST_COMPRESSOR,
  OBJ_KIND_ALTER,
  OBJ_KIND_LAUNCHER,
  OBJ_KIND_SPIDERWEB,
  OBJ_KIND_AUTO_SPEAR,
  OBJ_KIND_TRAP_SPEAR,
  OBJ_KIND_LIGHT_SOURCE,
  OBJ_KIND_TREASURE,
};

//オブジェクト情報
struct WRITE_INFO
{
  OBJ_KIND    objKind;
  D3DXVECTOR3 pos;
  float       angle;
  int         info1;
  int         info2;
};

//外部ファイルに書き込む為のデータ共用体
union WRITE_UNION
{
  int reset = 0;
  WRITE_INFO info;
  int data[7];
};

//オブジェクトデータの書き込み
void SaveObjects();

/// <summary>
/// オブジェクトの設置
/// </summary>
void Edit()
{
  KEY_BOARD keyTrg = GetTrgInKeyBoard();
  KEY_BOARD key = GetKeyBoard();
  g_objNum = GetObjectNum();

  //マップを切り替える
  if(keyTrg.num1)
  {
    g_mapNameIndex = 0;
    g_meshHandles[MAP] = ReloadXFile(g_meshHandles[MAP], g_mapDataDirectry[g_mapNameIndex][1], FBX_MESH);
    g_meshHandles[TERRAIN] = ReloadXFile(g_meshHandles[TERRAIN], g_mapDataDirectry[g_mapNameIndex][2], FBX_MESH);
    LoadObjData();
    g_objIndex = 1;
  }
  if(keyTrg.num2)
  {
    g_mapNameIndex = 1;
    g_meshHandles[MAP] = ReloadXFile(g_meshHandles[MAP], g_mapDataDirectry[g_mapNameIndex][1], FBX_MESH);
    g_meshHandles[TERRAIN] = ReloadXFile(g_meshHandles[TERRAIN], g_mapDataDirectry[g_mapNameIndex][2], FBX_MESH);
    LoadObjData();
    g_objIndex = 1;
  }

  //操作するオブジェクトの変更
  if(keyTrg.asterisk)
  {
    if(g_objIndex < g_objNum)
    {
      if(g_obj != NULL)
      {
        g_objIndex++;
        g_obj = g_obj->pNext_;
      }
    }
  }
  if(keyTrg.slash)
  {
    if(g_objIndex > 1)
    {
      if(g_obj != NULL)
      {
        g_objIndex--;
        g_obj = g_obj->pPrev_;
      }
    }
  }

  //設置するオブジェクトの種類変更
  if(keyTrg.up)
    g_placeObjIndex--;
  if(keyTrg.down)
    g_placeObjIndex++;
  if(g_placeObjIndex < 0)
    g_placeObjIndex = g_objTableMax - 1;
  if(g_placeObjIndex >= g_objTableMax)
    g_placeObjIndex = 0;

  //オブジェクトの設置
  if(keyTrg.z)
  {
    g_objIndex = g_objNum + 1;
    g_obj = AddObjData(g_placeObjTable[g_placeObjIndex], GetPlayerData().pos_, D3DXVECTOR3(1.0f, 1.0f, 1.0f), 0.0f);
    switch(g_placeObjTable[g_placeObjIndex])
    {
    case OBJ_KIND_SKELETON:
      g_obj->scale_ = g_meshScaleTable[SKELETON];
      ((SKELETON_OPTION*)g_obj->option_)->skeKind_ = SKELETON_KIND_NORMAL;
      ((SKELETON_OPTION*)g_obj->option_)->nowState_ = SKELETON_STATE_NO_MOVE;
      break;
    case OBJ_KIND_DROP_ITEM:
      g_obj->scale_ = g_meshScaleTable[DROP_ITEMS];
      ((ITEM_OPTION*)g_obj->option_)->itemID = 0;
      break;
    case OBJ_KIND_MIST_COMPRESSOR:
      g_obj->scale_ = g_meshScaleTable[MIST_COMPRESSOR];
      ((MIST_COMPRESSOR_OPTION*)g_obj->option_)->isSaveUp_ = false;
      break;
    case OBJ_KIND_ALTER:
      g_obj->scale_ = g_meshScaleTable[ALTER];
      ((ALTER_OPTION*)g_obj->option_)->isMove_ = false;
      break;
    case OBJ_KIND_LAUNCHER:
      g_obj->scale_ = g_meshScaleTable[LAUNCHER];
      break;
    case OBJ_KIND_SPIDERWEB:
      g_obj->scale_ = g_meshScaleTable[SPIDERWEB];
      break;
    case OBJ_KIND_AUTO_SPEAR:
      g_obj->scale_ = g_meshScaleTable[AUTO_SPEAR];
      ((AUTO_SPEAR_OPTION*)g_obj->option_)->prepareTime_ = 0;
      break;
    case OBJ_KIND_TRAP_SPEAR:
      g_obj->scale_ = g_meshScaleTable[TRAP_SPEAR];
      ((TRAP_SPEAR_OPTION*)g_obj->option_)->delayPenetrateTime_ = 0;
      break;
    case OBJ_KIND_LIGHT_SOURCE:
      ((LIGHT_SOURCE_OPTION*)g_obj->option_)->lightID = 0;
      ((LIGHT_SOURCE_OPTION*)g_obj->option_)->lightDistance = 1000;
      break;
    case OBJ_KIND_TREASURE:

      break;
    default:
      g_obj->scale_ = D3DXVECTOR3(1.0f, 1.0f, 1.0f);
      break;
    }
  }

  //オブジェクトの消去
  if(keyTrg.x)
  {
    if(g_obj != NULL)
    {
      //現在あるオブジェクトのデータを破棄する
      if((g_obj->kind_ & (OBJ_KIND_PLAYER | OBJ_KIND_MAP | OBJ_KIND_TERRAIN | OBJ_KIND_FOG)) == 0)
      {
        OBJ_DATA* prev = g_obj->pPrev_;
        DeleteListObjData(g_obj);
        g_obj = prev;
        g_objIndex--;
      }
    }
  }

  //オブジェクトの詳細指数変更
  if(keyTrg.right)
    g_objInfoIndex++;
  if(keyTrg.left)
    g_objInfoIndex--;
  //詳細指数を範囲内にする
  if(g_obj != NULL)
  {
    switch(g_obj->kind_)
    {
    case OBJ_KIND_SKELETON:
      g_objInfoIndex = CLUMP(0, g_objInfoIndex, 0);
      break;
    case OBJ_KIND_DROP_ITEM:
      g_objInfoIndex = CLUMP(0, g_objInfoIndex, 0);
      break;
    case OBJ_KIND_MIST_COMPRESSOR:
      g_objInfoIndex = CLUMP(0, g_objInfoIndex, 0);
      break;
    case OBJ_KIND_ALTER:
      g_objInfoIndex = CLUMP(0, g_objInfoIndex, 0);
      break;
    case OBJ_KIND_LAUNCHER:
      g_objInfoIndex = CLUMP(0, g_objInfoIndex, 0);
      break;
    case OBJ_KIND_SPIDERWEB:
      g_objInfoIndex = CLUMP(0, g_objInfoIndex, 0);
      break;
    case OBJ_KIND_AUTO_SPEAR:
      g_objInfoIndex = CLUMP(0, g_objInfoIndex, 0);
      break;
    case OBJ_KIND_TRAP_SPEAR:
      g_objInfoIndex = CLUMP(0, g_objInfoIndex, 0);
      break;
    case OBJ_KIND_LIGHT_SOURCE:
      g_objInfoIndex = CLUMP(0, g_objInfoIndex, 1);
      break;
    }

    key = GetKeyBoard();
    //ポジション移動
    if(key.numPad8)
      g_obj->pos_.z += 1.0f;
    if(key.numPad5)
      g_obj->pos_.z -= 1.0f;
    if(key.minus)
      g_obj->pos_.y += 1.0f;
    if(key.plus)
      g_obj->pos_.y -= 1.0f;
    if(key.numPad4)
      g_obj->pos_.x -= 1.0f;
    if(key.numPad6)
      g_obj->pos_.x += 1.0f;
    //アングル変化
    if(key.numPad7)
      g_obj->yawAngle_ -= 0.01f;
    if(key.numPad9)
      g_obj->yawAngle_ += 0.01f;

    //オブジェクト毎に詳細の値変更
    switch(g_obj->kind_)
    {
    case OBJ_KIND_SKELETON:
        if(keyTrg.numPad1)
          ((SKELETON_OPTION*)g_obj->option_)->skeKind_ = (E_SKELETON_KIND)CLUMP(0, ((SKELETON_OPTION*)g_obj->option_)->skeKind_ - 1, SKELETON_KIND_ARMOR);
        if(keyTrg.numPad2)
          ((SKELETON_OPTION*)g_obj->option_)->skeKind_ = (E_SKELETON_KIND)CLUMP(0, ((SKELETON_OPTION*)g_obj->option_)->skeKind_ + 1, SKELETON_KIND_ARMOR);
        //モデルデータの変更
        if(((SKELETON_OPTION*)g_obj->option_)->skeKind_ == SKELETON_KIND_NORMAL)
          g_obj->pMeshHandle_ = g_meshHandles[SKELETON];
        if(((SKELETON_OPTION*)g_obj->option_)->skeKind_ == SKELETON_KIND_HALF)
          g_obj->pMeshHandle_ = g_meshHandles[SKELETON_HALF];
        if(((SKELETON_OPTION*)g_obj->option_)->skeKind_ == SKELETON_KIND_ARMOR)
          g_obj->pMeshHandle_ = g_meshHandles[SKELETON_ARMOR];
      break;
    case OBJ_KIND_DROP_ITEM:
      if(keyTrg.numPad1)
        ((ITEM_OPTION*)g_obj->option_)->itemID = CLUMP(0, ((ITEM_OPTION*)g_obj->option_)->itemID - 1, 3);
      if(keyTrg.numPad2)
        ((ITEM_OPTION*)g_obj->option_)->itemID = CLUMP(0, ((ITEM_OPTION*)g_obj->option_)->itemID + 1, 3);
      break;
    case OBJ_KIND_MIST_COMPRESSOR:
      if(keyTrg.numPad1)
        ((MIST_COMPRESSOR_OPTION*)g_obj->option_)->isSaveUp_ = CLUMP(0, ((MIST_COMPRESSOR_OPTION*)g_obj->option_)->isSaveUp_ - 1, 1);
      if(keyTrg.numPad2)
        ((MIST_COMPRESSOR_OPTION*)g_obj->option_)->isSaveUp_ = CLUMP(0, ((MIST_COMPRESSOR_OPTION*)g_obj->option_)->isSaveUp_ + 1, 1);
      break;
    case OBJ_KIND_ALTER:
      break;
    case OBJ_KIND_LAUNCHER:
      break;
    case OBJ_KIND_SPIDERWEB:
      break;
    case OBJ_KIND_AUTO_SPEAR:
      if(key.numPad1)
        ((AUTO_SPEAR_OPTION*)g_obj->option_)->prepareTime_ = max(0, ((AUTO_SPEAR_OPTION*)g_obj->option_)->prepareTime_ - 10);
      if(key.numPad2)
        ((AUTO_SPEAR_OPTION*)g_obj->option_)->prepareTime_ = max(0, ((AUTO_SPEAR_OPTION*)g_obj->option_)->prepareTime_ + 10);
      break;
    case OBJ_KIND_TRAP_SPEAR:
      if(key.numPad1)
        ((TRAP_SPEAR_OPTION*)g_obj->option_)->delayPenetrateTime_ = max(0, ((TRAP_SPEAR_OPTION*)g_obj->option_)->delayPenetrateTime_ - 10);
      if(key.numPad2)
        ((TRAP_SPEAR_OPTION*)g_obj->option_)->delayPenetrateTime_ = max(0, ((TRAP_SPEAR_OPTION*)g_obj->option_)->delayPenetrateTime_ + 10);
      break;
    case OBJ_KIND_LIGHT_SOURCE:
      if(g_objInfoIndex == 0)
      {
        if(keyTrg.numPad1)
          ((LIGHT_SOURCE_OPTION*)g_obj->option_)->lightID = CLUMP(0, ((LIGHT_SOURCE_OPTION*)g_obj->option_)->lightID - 1, COLOR_MAX - 1);
        if(keyTrg.numPad2)
          ((LIGHT_SOURCE_OPTION*)g_obj->option_)->lightID = CLUMP(0, ((LIGHT_SOURCE_OPTION*)g_obj->option_)->lightID + 1, COLOR_MAX - 1);
      }
      else
      {
        if(key.numPad1)
          ((LIGHT_SOURCE_OPTION*)g_obj->option_)->lightDistance = max(0, ((LIGHT_SOURCE_OPTION*)g_obj->option_)->lightDistance - 10);
        if(key.numPad2)
          ((LIGHT_SOURCE_OPTION*)g_obj->option_)->lightDistance = max(0, ((LIGHT_SOURCE_OPTION*)g_obj->option_)->lightDistance + 10);
      }
      break;
    case OBJ_KIND_TREASURE:
      break;
    }
  }

  //オブジェデータの書き込み
  if(key.lCtrl && keyTrg.s)
  {
    SaveObjects();
  }
}

//オブジェクトデータの書き込み
void SaveObjects()
{
  //セーブするか聞く
  if(MessageBox(NULL, "セーブしますか？。", "seve?", MB_YESNO | MB_ICONQUESTION) != IDYES) { return; }

  //現在のマップ用のオブジェクトデータファイルを開く
  FILE* fp = NULL;
  fp = fopen(g_mapDataDirectry[g_mapNameIndex][0], "w");
  if(fp == NULL)
  {
    MessageBox(NULL, "ファイルが開けませんでした", "ファイルエラー", NULL);
  }

  OBJ_DATA* obj = GetHeadObjData();
  //オブジェクトをすべて参照して保存
  while(obj != NULL)
  {
    //プレイヤーと地形は保存せず次へ
    if(obj->kind_ & (OBJ_KIND_PLAYER | OBJ_KIND_MAP | OBJ_KIND_TERRAIN | OBJ_KIND_FOG))
    {
      obj = obj->pNext_;
      continue;
    }

    //構造体に情報をまとめる
    WRITE_INFO data;
    memset(&data, 0, sizeof(WRITE_INFO));
    data.objKind = (OBJ_KIND)obj->kind_;
    data.pos = obj->pos_;
    data.angle = obj->yawAngle_;
    switch(obj->kind_)
    {
    case OBJ_KIND_SKELETON:
      data.info1 = ((SKELETON_OPTION*)obj->option_)->skeKind_;
      break;
    case OBJ_KIND_DROP_ITEM:
      data.info1 = ((ITEM_OPTION*)obj->option_)->itemID;
      break;
    case OBJ_KIND_MIST_COMPRESSOR:
      data.info1 = ((MIST_COMPRESSOR_OPTION*)obj->option_)->isSaveUp_;
      break;
    case OBJ_KIND_ALTER:
      break;
    case OBJ_KIND_AUTO_SPEAR:
      data.info1 = ((AUTO_SPEAR_OPTION*)obj->option_)->prepareTime_;
      break;
    case OBJ_KIND_TRAP_SPEAR:
      data.info1 = ((TRAP_SPEAR_OPTION*)obj->option_)->delayPenetrateTime_;
      break;
    case OBJ_KIND_LIGHT_SOURCE:
      data.info1 = ((LIGHT_SOURCE_OPTION*)obj->option_)->lightID;
      data.info2 = ((LIGHT_SOURCE_OPTION*)obj->option_)->lightDistance;
      break;
    case OBJ_KIND_TREASURE:
      break;
    }

    //保存用の共用体に情報を移す
    union WRITE_UNION saveData = {0};
    saveData.info = data;

    //ファイルへの書き込み
    fprintf_s(fp, "%d,%d,%d,%d,%d,%d,%d;\n", saveData.data[0], saveData.data[1], saveData.data[2], saveData.data[3], saveData.data[4], saveData.data[5], saveData.data[6]);

    //次のオブジェクトへ
    obj = obj->pNext_;
  }
  fclose(fp);
}

/// <summary>
/// エディット中の情報を描画
/// </summary>
void RenderEdit()
{
  //操作中のオブジェクトの指数
  DrawPrintf(900, 15 * 1, 0xffff6666, "nowMap  =  %d", g_mapNameIndex);
  DrawPrintf(900, 15 * 2, 0xffff6666, "nowMove = [%3d]/[%3d]", g_objIndex, g_objNum);
  //設置するオブジェクトの種類の列挙
  DrawPrintf(10, 15 * 1, 0xffff6666, "skeleton      ");
  DrawPrintf(10, 15 * 2, 0xffff6666, "dropItem      ");
  DrawPrintf(10, 15 * 3, 0xffff6666, "mistCompressor");
  DrawPrintf(10, 15 * 4, 0xffff6666, "alter         ");
  DrawPrintf(10, 15 * 5, 0xffff6666, "launcher      ");
  DrawPrintf(10, 15 * 6, 0xffff6666, "spiderweb     ");
  DrawPrintf(10, 15 * 7, 0xffff6666, "autoSpear     ");
  DrawPrintf(10, 15 * 8, 0xffff6666, "trapSpear     ");
  DrawPrintf(10, 15 * 9, 0xffff6666, "lightSource   ");
  DrawPrintf(10, 15 * 10, 0xffff6666, "treasure      ");
  DrawPrintf(10, 15 * 11, 0xffff6666, "gObjInfoIndex = %d", g_objInfoIndex);
  //選択中の設置するオブジェクトの種類
  switch(g_placeObjTable[g_placeObjIndex])
  {
  case OBJ_KIND_SKELETON:
    DrawPrintf(10, 15 * 1, 0xffffffff, "skeleton      ");
    break;
  case OBJ_KIND_DROP_ITEM:
    DrawPrintf(10, 15 * 2, 0xffffffff, "dropItem      ");
    break;
  case OBJ_KIND_MIST_COMPRESSOR:
    DrawPrintf(10, 15 * 3, 0xffffffff, "mistCompressor");
    break;
  case OBJ_KIND_ALTER:
    DrawPrintf(10, 15 * 4, 0xffffffff, "alter         ");
    break;
  case OBJ_KIND_LAUNCHER:
    DrawPrintf(10, 15 * 5, 0xffffffff, "launcher      ");
    break;
  case OBJ_KIND_SPIDERWEB:
    DrawPrintf(10, 15 * 6, 0xffffffff, "spiderweb     ");
    break;
  case OBJ_KIND_AUTO_SPEAR:
    DrawPrintf(10, 15 * 7, 0xffffffff, "autoSpear     ");
    break;
  case OBJ_KIND_TRAP_SPEAR:
    DrawPrintf(10, 15 * 8, 0xffffffff, "trapSpear     ");
    break;
  case OBJ_KIND_LIGHT_SOURCE:
    DrawPrintf(10, 15 * 9, 0xffffffff, "lightSource   ");
    break;
  case OBJ_KIND_TREASURE:
    DrawPrintf(10, 15 * 10, 0xffffffff, "treasure      ");
    break;
  }

  //オブジェクトの詳細
  if(g_obj != NULL)
  {
    DrawPrintf(150, 15 * 2, 0xffff6666, "obj.pos.x     = %.2f", g_obj->pos_.x);
    DrawPrintf(150, 15 * 3, 0xffff6666, "obj.pos.y     = %.2f", g_obj->pos_.y);
    DrawPrintf(150, 15 * 4, 0xffff6666, "obj.pos.z     = %.2f", g_obj->pos_.z);
    DrawPrintf(150, 15 * 5, 0xffff6666, "obj.angle     = %.2f", g_obj->yawAngle_);

    switch(g_obj->kind_)
    {
    case OBJ_KIND_SKELETON:
      DrawPrintf(150, 15 * 1, 0xffff6666, "obj.kind      = skeleton");
      DrawPrintf(150, 15 * 6, 0xffff6666, "ske.skeKind   = %d", ((SKELETON_OPTION*)g_obj->option_)->skeKind_);
      if(g_objInfoIndex == 0)
        DrawPrintf(150, 15 * 6, 0xffffffff, "ske.skeKind   = %d", ((SKELETON_OPTION*)g_obj->option_)->skeKind_);
      break;
    case OBJ_KIND_DROP_ITEM:
      DrawPrintf(150, 15 * 1, 0xffff6666, "obj.kind      = drop_item");
      DrawPrintf(150, 15 * 6, 0xffff6666, "item.ID       = %d", ((ITEM_OPTION*)g_obj->option_)->itemID);
      if(g_objInfoIndex == 0)
        DrawPrintf(150, 15 * 6, 0xffffffff, "item.ID       = %d", ((ITEM_OPTION*)g_obj->option_)->itemID);
      break;
    case OBJ_KIND_MIST_COMPRESSOR:
      DrawPrintf(150, 15 * 1, 0xffff6666, "obj.kind      = mist_compressor");
      DrawPrintf(150, 15 * 6, 0xffff6666, "mist.save     = %d", ((MIST_COMPRESSOR_OPTION*)g_obj->option_)->isSaveUp_);
      if(g_objInfoIndex == 0)
        DrawPrintf(150, 15 * 6, 0xffffffff, "mist.save     = %d", ((MIST_COMPRESSOR_OPTION*)g_obj->option_)->isSaveUp_);
      break;
    case OBJ_KIND_ALTER:
      DrawPrintf(150, 15 * 1, 0xffff6666, "obj.kind      = alter");
      break;
    case OBJ_KIND_LAUNCHER:
      DrawPrintf(150, 15 * 1, 0xffff6666, "obj.kind      = launcher");
      break;
    case OBJ_KIND_SPIDERWEB:
      DrawPrintf(150, 15 * 1, 0xffff6666, "obj.kind      = spiderweb");
      break;
    case OBJ_KIND_AUTO_SPEAR:
      DrawPrintf(150, 15 * 1, 0xffff6666, "obj.kind      = autoSpear");
      DrawPrintf(150, 15 * 6, 0xffff6666, "autoSpear.prepareTime = %d", ((AUTO_SPEAR_OPTION*)g_obj->option_)->prepareTime_);
      if(g_objInfoIndex == 0)
        DrawPrintf(150, 15 * 6, 0xffffffff, "autoSpear.prepareTime = %d", ((AUTO_SPEAR_OPTION*)g_obj->option_)->prepareTime_);
      break;
    case OBJ_KIND_TRAP_SPEAR:
      DrawPrintf(150, 15 * 1, 0xffff6666, "obj.kind      = trapSpear");
      DrawPrintf(150, 15 * 6, 0xffff6666, "trapSpear.prepareTime = %d", ((TRAP_SPEAR_OPTION*)g_obj->option_)->delayPenetrateTime_);
      if(g_objInfoIndex == 0)
        DrawPrintf(150, 15 * 6, 0xffffffff, "trapSpear.prepareTime = %d", ((TRAP_SPEAR_OPTION*)g_obj->option_)->delayPenetrateTime_);
      break;
    case OBJ_KIND_LIGHT_SOURCE:
      DrawPrintf(150, 15 * 1, 0xffff6666, "obj.kind      = lightSource");
      DrawPrintf(150, 15 * 6, 0xffff6666, "light.ID        = %d", ((LIGHT_SOURCE_OPTION*)g_obj->option_)->lightID);
      DrawPrintf(150, 15 * 7, 0xffff6666, "light.distance  = %d", ((LIGHT_SOURCE_OPTION*)g_obj->option_)->lightDistance);
      if(g_objInfoIndex == 0)
        DrawPrintf(150, 15 * 6, 0xffffffff, "light.ID        = %d", ((LIGHT_SOURCE_OPTION*)g_obj->option_)->lightID);
      if(g_objInfoIndex == 1)
        DrawPrintf(150, 15 * 7, 0xffffffff, "light.distance  = %.2f", ((LIGHT_SOURCE_OPTION*)g_obj->option_)->lightDistance);
      break;
    case OBJ_KIND_TREASURE:
      DrawPrintf(150, 15 * 1, 0xffff6666, "obj.kind      = treasure ");
      break;
    }
  }
}

/// <summary>
/// マップデータとオブジェクトの再読み込み
/// </summary>
/// <param name="stageNum">変更後のステージ指定</param>
void ChangeStage(int stageNum)
{
  //ステージ数の指定
  stageNum = CLUMP(0, stageNum, g_stageMax - 1);
  //ステージデータの再読み込み
  if(g_mapNameIndex != stageNum)
  {
    g_prevMapNameIndex = g_mapNameIndex;
    g_mapNameIndex = stageNum;
    g_meshHandles[MAP] = ReloadXFile(g_meshHandles[MAP], g_mapDataDirectry[g_mapNameIndex][1], FBX_MESH);
    g_meshHandles[TERRAIN] = ReloadXFile(g_meshHandles[TERRAIN], g_mapDataDirectry[g_mapNameIndex][2], FBX_MESH);
  }
  //オブジェクト情報の読み込み
  LoadObjData();
}

/// <summary>
/// オブジェクトの設置情報の読み込み
/// </summary>
void LoadObjData()
{
  //現在あるオブジェクトのデータを破棄する
  OBJ_DATA* obj = GetHeadObjData();
  while(obj != NULL)
  {
    OBJ_DATA* next = obj->pNext_;
    //プレイヤーと地面以外を破棄
    if((obj->kind_ & (OBJ_KIND_PLAYER | OBJ_KIND_MAP | OBJ_KIND_TERRAIN | OBJ_KIND_FOG)) == 0)
      DeleteListObjData(obj);
    obj = next;
  }

  ReloadMeshHandle();

  //現在のマップ用のオブジェクトデータファイルを開く
  FILE* fp = NULL;
  fp = fopen(g_mapDataDirectry[g_mapNameIndex][0], "r");
  if(fp == NULL)
  {
    MessageBox(NULL, "ファイルが開けませんでした", "ファイルエラー", NULL);
    return;
  }

  union WRITE_UNION saveData = {0};
  obj = NULL;
  while(fscanf(fp, "%d,%d,%d,%d,%d,%d,%d;\n", &saveData.data[0], &saveData.data[1], &saveData.data[2], &saveData.data[3], &saveData.data[4], &saveData.data[5], &saveData.data[6]) != EOF)
  {
    switch(saveData.info.objKind)
    {
    case OBJ_KIND_SKELETON:
      obj = GenerateSkeleton(saveData.info.pos, saveData.info.info1);
      obj->yawAngle_ = saveData.info.angle;
      break;
    case OBJ_KIND_DROP_ITEM:
      obj = GenerateDropItems(saveData.info.pos, saveData.info.info1);
      obj->yawAngle_ = saveData.info.angle;
      break;
    case OBJ_KIND_MIST_COMPRESSOR:
      obj = GenerateMistCompressor(saveData.info.pos, saveData.info.info1);
      obj->yawAngle_ = saveData.info.angle;
      break;
    case OBJ_KIND_ALTER:
      obj = GenerateAlter(saveData.info.pos, saveData.info.angle);
      break;
    case OBJ_KIND_LAUNCHER:
      obj = GenerateLauncher(saveData.info.pos, saveData.info.angle);
      obj->yawAngle_ = saveData.info.angle;
      break;
    case OBJ_KIND_SPIDERWEB:
      obj = GenerateSpiderweb(saveData.info.pos);
      break;
    case OBJ_KIND_AUTO_SPEAR:
      obj = GenerateAutoSpear(saveData.info.pos);
      ((AUTO_SPEAR_OPTION*)obj->option_)->prepareTime_ = saveData.info.info1;
      break;
    case OBJ_KIND_TRAP_SPEAR:
      obj = GenerateTrapSpear(saveData.info.pos);
      ((TRAP_SPEAR_OPTION*)obj->option_)->delayPenetrateTime_ = saveData.info.info1;
      break;
    case OBJ_KIND_LIGHT_SOURCE:
      obj = GenerateLightSource(saveData.info.pos, 0);
      ((LIGHT_SOURCE_OPTION*)obj->option_)->lightID = saveData.info.info1;
      ((LIGHT_SOURCE_OPTION*)obj->option_)->lightDistance = saveData.info.info2;
      break;
    case OBJ_KIND_TREASURE:
      obj = GenerateTreasure(saveData.info.pos);
      obj->yawAngle_ = saveData.info.angle;
    }
  }

  fclose(fp);
}

/*

F1~F5 ゲームモード切替
  F1 = 通常
  F2 = HP無限
  F3 = 飛べる
  F4 = 飛べる&あたり判定見える
  F5 = オブジェクト設置

Shift.Space 上下移動
  Shift 降下
  Space 上昇

Z オブジェクトの設置
↑↓ 設置するオブジェクトの変更

*/
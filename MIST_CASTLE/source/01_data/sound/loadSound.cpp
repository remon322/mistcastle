#include "dslib.h"
#include "loadSound.h"
#include "../../00_application/app/app.h"

//テクスチャハンドル情報
int g_soundHandles[SOUND_MAX] = { 0 };

/// <summary>
/// サウンドハンドル情報の初期化
/// </summary>
void InitSoundHandles()
{
  g_soundHandles[TITLE_BGM]          = DSoundLoadFile("res/sound/bgm/title.wav", -1,DSLF_FLG_STATIC);
  g_soundHandles[OPENING_BGM]        = DSoundLoadFile("res/sound/bgm/opening.wav", -1,DSLF_FLG_STATIC);
  g_soundHandles[ENDING_BGM]         = DSoundLoadFile("res/sound/bgm/ending.wav", -1,DSLF_FLG_STATIC);
  g_soundHandles[INGAME_BGM]         = DSoundLoadFile("res/sound/bgm/gameBGM.wav", -1,DSLF_FLG_STATIC);
  g_soundHandles[MAP2_BGM]           = DSoundLoadFile("res/sound/bgm/map2.wav", -1,DSLF_FLG_STATIC);
  g_soundHandles[GAMEOVER_BGM]       = DSoundLoadFile("res/sound/bgm/gameover.wav", -1,DSLF_FLG_STATIC);
  g_soundHandles[SKELETON_ATTACK_SE] = DSoundLoadFile("res/sound/skeleton/skeletonAttack.wav", -1,DSLF_FLG_STATIC);
  g_soundHandles[SKELETON_ATTACK2_SE]= DSoundLoadFile("res/sound/skeleton/skeletonAttack2.wav", -1,DSLF_FLG_STATIC);
  g_soundHandles[SKELETON_WALK_SE]   = DSoundLoadFile("res/sound/skeleton/skeletonWalk.wav", -1,DSLF_FLG_STATIC);
  g_soundHandles[PLAYER_WALK_SE]     = DSoundLoadFile("res/sound/player/walk.wav", -1,DSLF_FLG_STATIC);
  g_soundHandles[PLAYER_RUN_SE]      = DSoundLoadFile("res/sound/player/run.wav", -1,DSLF_FLG_STATIC);
  g_soundHandles[PLAYER_DEATH_SE]    = DSoundLoadFile("res/sound/player/death.wav", -1,DSLF_FLG_STATIC);
  g_soundHandles[PLAYER_DAMAGE_SE]   = DSoundLoadFile("res/sound/player/damage.wav", -1,DSLF_FLG_STATIC);

  g_soundHandles[MENU_CHOOSE_SE]     = DSoundLoadFile("res/sound/menu/choose.wav", -1,DSLF_FLG_STATIC);
  g_soundHandles[MENU_DECIDE_SE]     = DSoundLoadFile("res/sound/menu/decide.wav", -1,DSLF_FLG_STATIC);
  g_soundHandles[MENU_MESSAGE_SE]    = DSoundLoadFile("res/sound/menu/message.wav", -1,DSLF_FLG_STATIC);
  g_soundHandles[MENU_POSE_SE]       = DSoundLoadFile("res/sound/menu/pose.wav", -1,DSLF_FLG_STATIC);

  g_soundHandles[ITEM_GET_SE]       = DSoundLoadFile("res/sound/itemGet.wav", -1,DSLF_FLG_STATIC);
  g_soundHandles[TREASURE_GET_SE]   = DSoundLoadFile("res/sound/treasure.wav", -1,DSLF_FLG_STATIC);

  g_soundHandles[FOG_UP_SE]         = DSoundLoadFile("res/sound/fog/up.wav", -1,DSLF_FLG_STATIC);
  g_soundHandles[FOG_DOWN_SE]       = DSoundLoadFile("res/sound/fog/down.wav", -1,DSLF_FLG_STATIC);
}

/// <summary>
/// BGMの再生を停止する
/// </summary>
void StopBGM()
{
  DSoundStop(g_soundHandles[TITLE_BGM]);
  DSoundStop(g_soundHandles[OPENING_BGM]);
  DSoundStop(g_soundHandles[ENDING_BGM]);
  DSoundStop(g_soundHandles[INGAME_BGM]);
  DSoundStop(g_soundHandles[MAP2_BGM]);
  DSoundStop(g_soundHandles[GAMEOVER_BGM]);
}
#pragma once

#include <d3dx9.h>
#include "../../00_application/lib/myDx9Lib.h"

//使用音声識別列挙
enum SOUND_NAME
{
  TITLE_BGM,
  OPENING_BGM,
  ENDING_BGM,
  INGAME_BGM,
  MAP2_BGM,
  GAMEOVER_BGM,

  SKELETON_ATTACK_SE,
  SKELETON_ATTACK2_SE,
  SKELETON_WALK_SE,

  PLAYER_WALK_SE,
  PLAYER_RUN_SE,
  PLAYER_DEATH_SE,
  PLAYER_DAMAGE_SE,

  MENU_CHOOSE_SE,
  MENU_DECIDE_SE,
  MENU_MESSAGE_SE,
  MENU_POSE_SE,

  ITEM_GET_SE,
  TREASURE_GET_SE,

  FOG_UP_SE,
  FOG_DOWN_SE,


  SOUND_MAX,
};

//テクスチャハンドル情報
extern int g_soundHandles[];

//テクスチャハンドル情報の初期化
void InitSoundHandles();

//BGMの再生を停止する
void StopBGM();
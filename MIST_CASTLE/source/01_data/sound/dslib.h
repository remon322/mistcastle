#pragma once
#include <windows.h>

// 初期化
bool DSoundInit(HWND hWnd, LPGUID lpGuid);

// 解放
bool DSoundRelease();

// wavファイルの読み込み
enum
{
  DSLF_FLG_DEFAULT = 0,
  DSLF_FLG_STATIC,
  DSLF_FLG_STREAM,
};
int DSoundLoadFile(
  LPSTR szFileName,		// ファイル名 or リソース
  int nChannel,			// 読み込みチャンネル番号(-1で空いてるチャンネルに追加)
  DWORD dwFlg				// フラグ DSLF_FLG_xxxx
);

// wavファイルの解放
bool DSoundReleaseChannel(int nChannel);

// 再生
bool DSoundPlay(int nChannel, bool bLoop);

// 停止
void DSoundStop(int nChannel);

// ボリュームセット
bool DSoundSetVolume(int nChannel, int nVolume);

// パンセット
bool DSoundSetPan(int nChannel, int nCmd);

// 再生中か？
bool DSoundIsStop(int nChannel);
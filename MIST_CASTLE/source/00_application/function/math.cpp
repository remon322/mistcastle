
#include <math.h>
#include "math.h"

/// <summary>
/// 極座標からx_成分の抜出
/// </summary>
/// <param name="r">絶対値</param>
/// <param name="theta_">xz平面での角度</param>
/// <param name="phi_">yz平面での角度</param>
/// <returns>3次元ベクトルのx_成分</returns>
float GetVectorComponentX(float r, float theta_, float phi_)
{
  return r * sinf(theta_) * cosf(phi_);
}

/// <summary>
/// 極座標からy_成分の抜出
/// </summary>
/// <param name="r">絶対値</param>
/// <param name="theta_">xz平面での角度</param>
/// <param name="phi_">yz平面での角度</param>
/// <returns>3次元ベクトルのy_成分</returns>
float GetVectorComponentY(float r, float theta_, float phi_)
{
  return r * cosf(theta_);
}

/// <summary>
/// 極座標からz_成分の抜出
/// </summary>
/// <param name="r">絶対値</param>
/// <param name="theta_">xz平面での角度</param>
/// <param name="phi_">yz平面での角度</param>
/// <returns>3次元ベクトルのz_成分</returns>
float GetVectorComponentZ(float r, float theta_, float phi_)
{
  return r * sinf(theta_) * sinf(phi_);
}

/// <summary>
/// 極座標から3次元ベクトルの抜出
/// </summary>
/// <param name="r">絶対値</param>
/// <param name="theta_">xz平面での角度</param>
/// <param name="phi_">yz平面での角度</param>
/// <returns>3次元ベクトル</returns>
D3DXVECTOR3 GetVectorComponentXYZ(float r, float theta_, float phi_)
{
  D3DXVECTOR3 result;

  result.x = r * sinf(theta_) * cosf(phi_);
  result.y = r * cosf(theta_);
  result.z = r * sinf(theta_) * sinf(phi_);

  return result;
}

/// <summary>
/// 3次元ベクトルから極座標の抽出
/// </summary>
/// <param name="vector">3次元ベクトル</param>
/// <param name="r">(out)絶対値</param>
/// <param name="theta_">(out)xz平面での角度</param>
/// <param name="phi_">(out)yz平面での角度</param>
void GetVectorPolar(D3DXVECTOR3 vector, float* r, float* theta_, float* phi_)
{
  float resultTheta;
  float resultPhi;
  float resultR;

  resultPhi = atan2f(vector.z , vector.x);
  resultTheta = cosf(resultPhi) ? acosf((vector.x - vector.y) / cosf(resultPhi)) : 0.0f;//割る数に0が来たら角度を0にする
  resultR = cosf(resultTheta) ? vector.y / cosf(resultTheta) : 0.0f;//割る数に0が来たら角度を0にする

  //NULLポインタには入れない
  if(theta_) { *theta_ = resultTheta; }
  if(phi_) { *phi_ = resultPhi; }
  if(r) { *r = resultR; }
}

/// <summary>
/// 2Dベクトルの絶対値(スカラー)を取り出す
/// </summary>
/// <param name="x_">2Dベクトルのx_成分</param>
/// <param name="y_">2Dベクトルのy_成分</param>
/// <returns>絶対値(スカラー)</returns>
float GetScalar2D(float x,float y)
{
  return sqrtf(x * x + y * y);
}

/// <summary>
/// 3Dベクトルの2点間の距離を求める
/// </summary>
/// <param name="a">座標A</param>
/// <param name="b">座標B</param>
/// <returns>2点間の距離</returns>
float GetDistance(D3DXVECTOR3 a, D3DXVECTOR3 b)
{
  D3DXVECTOR3 sub = a - b;
  float result = sqrt(sub.x * sub.x + sub.y * sub.y + sub.z * sub.z);
  return result;
}

/// <summary>
/// 2Dベクトルの内積を求める
/// </summary>
/// <param name="x1">座標1</param>
/// <param name="y1">座標1</param>
/// <param name="x2">座標2</param>
/// <param name="y2">座標2</param>
/// <returns>内積の値</returns>
float DotProduct(float x1, float y1, float x2, float y2)
{
  return x1 * x2 + y1 * y2;
}

/// <summary>
/// 2Dベクトルの外積を求める
/// </summary>
/// <param name="x1">座標1</param>
/// <param name="y1">座標1</param>
/// <param name="x2">座標2</param>
/// <param name="y2">座標2</param>
/// <returns>外積の値</returns>
float CrossProduct(float x1, float y1, float x2, float y2)
{
  return x1 * y2 + x1 * y2;
}

/// <summary>
/// 3Dベクトルの内積を求める
/// </summary>
/// <param name="a">座標A</param>
/// <param name="b">座標B</param>
/// <returns>内積の値</returns>
float DotProduct3D(D3DXVECTOR3 a, D3DXVECTOR3 b)
{
  return a.x*b.x + a.y*b.y + a.z*b.z;
}

/// <summary>
/// 3Dベクトルの外積を求める(順番によって結果が変化)
/// </summary>
/// <param name="a">座標A</param>
/// <param name="b">座標B</param>
/// <returns>外積の値</returns>
D3DXVECTOR3 CrossProduct3D(D3DXVECTOR3 a, D3DXVECTOR3 b)
{
  D3DXVECTOR3 result = { 0.0f,0.0f,0.0f };
  result.x = a.y * b.z - a.z * b.y;
  result.y = a.z * b.x - a.x * b.z;
  result.z = a.x * b.y - a.y * b.x;
  return result;
}

/// <summary>
/// 3Dベクトルとスカラーの乗算
/// </summary>
/// <param name="a">座標A</param>
/// <param name="b">座標B</param>
/// <returns>乗算後の3Dベクトル</returns>
D3DXVECTOR3 MulVectorScalar(D3DXVECTOR3 a,float b)
{
  D3DXVECTOR3 result;
  result.x = a.x * b;
  result.y = a.y * b;
  result.z = a.z * b;
  return result;
}

/// <summary>
/// 3Dベクトルとスカラーの除算
/// </summary>
/// <param name="a">座標A</param>
/// <param name="b">座標B</param>
/// <returns>除算後の3Dベクトル</returns>
D3DXVECTOR3 DivVectorScalar(D3DXVECTOR3 a, float b)
{
  D3DXVECTOR3 result;
  result.x = a.x / b;
  result.y = a.y / b;
  result.z = a.z / b;
  return result;
}

/// <summary>
/// ベクトルを単位ベクトルにする
/// </summary>
/// <param name="vector">単位にしたいベクトル</param>
/// <returns>単位ベクトル</returns>
D3DXVECTOR3 UnitVector(D3DXVECTOR3 vector)
{
  D3DXVECTOR3 result = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
  float scalar = sqrt(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z);

  result = DivVectorScalar(vector, scalar);

  return result;
}

/// <summary>
/// ベクトルのスカラーを取得
/// </summary>
/// <param name="vector">3Dベクトル</param>
/// <returns>スカラー</returns>
float GetScalar(D3DXVECTOR3 vector)
{
  float result = 0.0f;
  float scalar = sqrt(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z);

  result = scalar;

  return result;
}

/// <summary>
/// 角度を0~2*PIにする
/// </summary>
/// <param name="angle_">正規化したい角度</param>
/// <returns>正規化後の角度</returns>
float NormalizeAngle(float angle)
{
  float result = angle;
  
  while (result < 2 * PI)
  {
    result += 2 * PI;
  }
  while (result > 2 * PI)
  {
    result -= 2 * PI;
  }

  return result;
}
#pragma once
#define STRSAFE_NO_DEPRECATE
#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif
#include <windows.h>
#include <mmsystem.h>
#include <d3dx9.h>
#include <d3dx9anim.h>
#include <string.h>
#include <stdio.h>
#include <strsafe.h>
#include <time.h>
#include "myDx9Lib.h"
#include "FbxMeshAmg.h"
#include "../app/app.h"

#pragma comment(lib,"winmm.lib")
#pragma comment(lib,"d3d9.lib")
#pragma comment(lib,"d3dx9.lib")
#pragma comment(lib,"dxguid.lib")
#pragma comment(lib,"dinput8.lib")

 //DirectX用旧関数エイリアス定義
int (WINAPIV * __vsnprintf)(char *, size_t, const char*, va_list) = _vsnprintf;
int (WINAPIV * _sscanf)(const char*, const char*, ...) = sscanf;
int (WINAPIV * __snprintf)(char *, size_t, const char*, ...) = _snprintf;
int (WINAPIV * _sprintf)(char*, const char*, ...) = sprintf;

#define SAFE_FREE(p)         { if(p) { (p)->Release(); (p)=NULL; } }
#define SAFE_RELEASE(p)      { if(p) { (p)->Release(); (p)=NULL; } }
#define SAFE_DELETE(p)       { if(p) { delete (p);     (p)=NULL; } }
#define SAFE_DELETE_ARRAY(p) { if(p) { delete[] (p);   (p)=NULL; } }
#define CONVERT_VECTOR(v)    {D3DXVECTOR3(v.x, v.y, v.z)} 

static LPDIRECT3D9                  g_pD3D;                      //3Dデバイス
static LPDIRECT3DDEVICE9            g_pd3dDevice;                //レンダリング用デバイス
static LPD3DXFONT                   g_pFont = NULL;              //文字列管理用ハンドル
static int                          g_fontSize = (16);           //文字列描画のフォントサイズ(初期設定16)
static LPDIRECT3DVERTEXDECLARATION9 g_pVertexDeclaration = NULL; //頂点シェーダーフォーマット(g_pXshaderEffect)
static LPDIRECT3DVERTEXDECLARATION9 g_pVertexDeclaration2 = NULL; //頂点シェーダーフォーマット(g_p2dShaderEffect)
static LPDIRECT3DVERTEXDECLARATION9 g_pVertexDeclaration3 = NULL; //頂点シェーダーフォーマット(g_pSpeedershaderEffect)
//エフェクトインターフェース(HLSLを使うためのパイプライン情報の集まり)
static ID3DXEffect*                 g_pXshaderEffect = NULL;     //非アニメーションXfileメッシュ用
static ID3DXEffect*                 g_p2dShaderEffect = NULL;    //2Dテクスチャ用
static ID3DXEffect*                 g_pAnimXshaderEffect = NULL; //アニメーションXfileメッシュ用
static ID3DXEffect*                 g_pSpeedershaderEffect = NULL; //アニメーションXfileメッシュ用

//階層関係
/*■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■*/
//派生メッシュコンテナー構造体
//コンテナーがテクスチャを複数持てるようにポインターのポインターを追加する）
struct MYMESHCONTAINER : public D3DXMESHCONTAINER
{
  LPDIRECT3DTEXTURE9*  ppTextures;
  DWORD                dwWeight;//重みの個数（重みとは頂点への影響。）
  DWORD                dwBoneNum;//ボーンの数
  LPD3DXBUFFER         pBoneBuffer;//ボーン・テーブル
  D3DXMATRIX**         ppBoneMatrix;//全てのボーンのワールド行列の先頭ポインター
  D3DXMATRIX*          pBoneOffsetMatrices;//フレームとしてのボーンのワールド行列のポインター
};

//派生フレーム構造体 (それぞれのメッシュ用の最終ワールド行列を追加する）
struct MYFRAME : public D3DXFRAME
{
  D3DXMATRIX CombinedTransformationMatrix;
};

//ID3DXAllocateHierarchyは派生すること想定して設計されている。
class MY_HIERARCHY : public ID3DXAllocateHierarchy
{
public:
  MY_HIERARCHY() {}
  STDMETHOD(CreateFrame)(THIS_ LPCSTR, LPD3DXFRAME *);
  STDMETHOD(CreateMeshContainer)(THIS_ LPCTSTR, CONST D3DXMESHDATA*, CONST D3DXMATERIAL*,
                                 CONST D3DXEFFECTINSTANCE*, DWORD, CONST DWORD *, LPD3DXSKININFO, LPD3DXMESHCONTAINER *);
  STDMETHOD(DestroyFrame)(THIS_ LPD3DXFRAME);
  STDMETHOD(DestroyMeshContainer)(THIS_ LPD3DXMESHCONTAINER);
};

//HRESULT MY_HIERARCHY::CreateFrame(LPCTSTR Name, LPD3DXFRAME *ppNewFrame)
//フレームを作成する
HRESULT MY_HIERARCHY::CreateFrame(LPCTSTR Name, LPD3DXFRAME *ppNewFrame)
{
  HRESULT hr = S_OK;
  MYFRAME *pFrame;

  *ppNewFrame = NULL;

  pFrame = new MYFRAME;
  if(pFrame == NULL)
  {
    return E_OUTOFMEMORY;
  }
  pFrame->Name = new TCHAR[lstrlen(Name) + 1];
  if(!pFrame->Name)
  {
    return E_FAIL;
  }
  strcpy_s(pFrame->Name, sizeof(TCHAR)*lstrlen(Name) + 1, Name);

  D3DXMatrixIdentity(&pFrame->TransformationMatrix);
  D3DXMatrixIdentity(&pFrame->CombinedTransformationMatrix);
  pFrame->pMeshContainer = NULL;
  pFrame->pFrameSibling = NULL;
  pFrame->pFrameFirstChild = NULL;
  *ppNewFrame = pFrame;

  return S_OK;
}

//メッシュコンテナーを作成する
HRESULT MY_HIERARCHY::CreateMeshContainer(LPCSTR Name, CONST D3DXMESHDATA* pMeshData,
                                          CONST D3DXMATERIAL* pMaterials, CONST D3DXEFFECTINSTANCE* pEffectInstances,
                                          DWORD NumMaterials, CONST DWORD *pAdjacency, LPD3DXSKININFO pSkinInfo,
                                          LPD3DXMESHCONTAINER *ppMeshContainer)
{

  HRESULT hr;
  MYMESHCONTAINER *pMeshContainer = NULL;
  int iFacesAmount;
  int iMaterial;
  LPDIRECT3DDEVICE9 pDevice = NULL;
  LPD3DXMESH pMesh = NULL;
  *ppMeshContainer = NULL;
  DWORD dwBoneNum = 0;

  pMesh = pMeshData->pMesh;
  pMeshContainer = new MYMESHCONTAINER;
  if(pMeshContainer == NULL)
  {
    return E_OUTOFMEMORY;
  }
  ZeroMemory(pMeshContainer, sizeof(MYMESHCONTAINER));

  pMeshContainer->Name = new TCHAR[lstrlen(Name) + 1];
  if(!pMeshContainer->Name)
  {
    return E_FAIL;
  }
  strcpy(pMeshContainer->Name, Name);
  pMesh->GetDevice(&pDevice);
  iFacesAmount = pMesh->GetNumFaces();

  pMeshContainer->MeshData.pMesh = pMesh;
  pMeshContainer->MeshData.Type = D3DXMESHTYPE_MESH;

  if(pSkinInfo == NULL)
  {
    pMesh->AddRef();//通常メッシュの場合はこれが必要。スキンの場合、これをするとなぜかメモリリークになる。
  }
  //メッシュのマテリアル設定
  pMeshContainer->NumMaterials = max(1, NumMaterials);
  pMeshContainer->pMaterials = new D3DXMATERIAL[pMeshContainer->NumMaterials];
  pMeshContainer->ppTextures = new LPDIRECT3DTEXTURE9[pMeshContainer->NumMaterials];
  pMeshContainer->pAdjacency = new DWORD[iFacesAmount * 3];
  if((pMeshContainer->pAdjacency == NULL) || (pMeshContainer->pMaterials == NULL))
  {
    return E_FAIL;
  }
  memcpy(pMeshContainer->pAdjacency, pAdjacency, sizeof(DWORD) * iFacesAmount * 3);
  memset(pMeshContainer->ppTextures, 0, sizeof(LPDIRECT3DTEXTURE9) * pMeshContainer->NumMaterials);

  if(NumMaterials > 0)
  {
    memcpy(pMeshContainer->pMaterials, pMaterials, sizeof(D3DXMATERIAL) * NumMaterials);

    for(iMaterial = 0; iMaterial < NumMaterials; iMaterial++)
    {
      if(pMeshContainer->pMaterials[iMaterial].pTextureFilename != NULL)
      {
        TCHAR strTexturePath[MAX_PATH + 1];
        strcpy_s(strTexturePath, sizeof(TCHAR) * (MAX_PATH + 1), pMeshContainer->pMaterials[iMaterial].pTextureFilename);
        if(FAILED(D3DXCreateTextureFromFile(pDevice, strTexturePath,
           &pMeshContainer->ppTextures[iMaterial])))
          pMeshContainer->ppTextures[iMaterial] = NULL;
        pMeshContainer->pMaterials[iMaterial].pTextureFilename = NULL;
      }
    }
  }
  else
  {
    pMeshContainer->pMaterials[0].pTextureFilename = NULL;
    memset(&pMeshContainer->pMaterials[0].MatD3D, 0, sizeof(D3DMATERIAL9));
    pMeshContainer->pMaterials[0].MatD3D.Diffuse.r = 0.5f;
    pMeshContainer->pMaterials[0].MatD3D.Diffuse.g = 0.5f;
    pMeshContainer->pMaterials[0].MatD3D.Diffuse.b = 0.5f;
    pMeshContainer->pMaterials[0].MatD3D.Specular = pMeshContainer->pMaterials[0].MatD3D.Diffuse;
  }
  //当該メッシュがスキン情報を持っている場合（スキンメッシュ固有の処理）
  if(pSkinInfo != NULL)
  {
    pMeshContainer->pSkinInfo = pSkinInfo;
    pSkinInfo->AddRef();
    dwBoneNum = pSkinInfo->GetNumBones();
    pMeshContainer->pBoneOffsetMatrices = new D3DXMATRIX[dwBoneNum];

    for(DWORD i = 0; i < dwBoneNum; i++)
    {
      memcpy(&pMeshContainer->pBoneOffsetMatrices[i],
             pMeshContainer->pSkinInfo->GetBoneOffsetMatrix(i), sizeof(D3DMATRIX));
    }
    if(FAILED(pMeshContainer->pSkinInfo->ConvertToBlendedMesh(
      pMesh,
      NULL, pMeshContainer->pAdjacency, NULL, NULL, NULL,
      &pMeshContainer->dwWeight,
      &pMeshContainer->dwBoneNum,
      &pMeshContainer->pBoneBuffer,
      &pMeshContainer->MeshData.pMesh)
      ))
    {
      return E_FAIL;
    }
  }
  //ローカルに生成したメッシュコンテナーを呼び出し側にコピーする （コピーじゃないけど・・・）
  *ppMeshContainer = pMeshContainer;
  //参照カウンタを増やしたので減らす
  SAFE_RELEASE(pDevice);

  return S_OK;
}

//フレームを破棄する
HRESULT MY_HIERARCHY::DestroyFrame(LPD3DXFRAME pFrameToFree)
{
  if(pFrameToFree == NULL)
  {
    return S_FALSE;
  }

  SAFE_DELETE_ARRAY(pFrameToFree->Name);

  if(pFrameToFree->pFrameFirstChild)
  {
    DestroyFrame(pFrameToFree->pFrameFirstChild);
  }
  if(pFrameToFree->pFrameSibling)
  {
    DestroyFrame(pFrameToFree->pFrameSibling);
  }

  SAFE_DELETE(pFrameToFree);

  return S_OK;
}

//HRESULT MY_HIERARCHY::DestroyMeshContainer(LPD3DXMESHCONTAINER pMeshContainerBase)
//メッシュコンテナーを破棄する
HRESULT MY_HIERARCHY::DestroyMeshContainer(LPD3DXMESHCONTAINER pMeshContainerBase)
{
  DWORD iMaterial;
  MYMESHCONTAINER *pMeshContainer = (MYMESHCONTAINER*)pMeshContainerBase;

  SAFE_DELETE_ARRAY(pMeshContainer->Name);
  SAFE_RELEASE(pMeshContainer->pSkinInfo);
  SAFE_DELETE_ARRAY(pMeshContainer->pAdjacency);
  SAFE_DELETE_ARRAY(pMeshContainer->pMaterials);

  SAFE_DELETE_ARRAY(pMeshContainer->ppBoneMatrix);

  if(pMeshContainer->ppTextures != NULL)
  {
    for(iMaterial = 0; iMaterial < pMeshContainer->NumMaterials; iMaterial++)
    {
      SAFE_RELEASE(pMeshContainer->ppTextures[iMaterial]);
    }
  }
  SAFE_DELETE_ARRAY(pMeshContainer->ppTextures);

  SAFE_RELEASE(pMeshContainer->MeshData.pMesh);

  if(pMeshContainer->pBoneBuffer != NULL)
  {
    SAFE_RELEASE(pMeshContainer->pBoneBuffer);
    SAFE_DELETE_ARRAY(pMeshContainer->pBoneOffsetMatrices);
  }

  SAFE_DELETE(pMeshContainer);

  return S_OK;
}

//階層
MY_HIERARCHY cHierarchy;

//メッシュデータ関係
/*■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■*/

//読み込んだモデルのデータを格納する構造体
struct MESHDATA
{
  MESH_KIND                 meshKind_;           //メッシュの種類
  //アニメーションがないモデルのメッシュデータ
  DWORD                     dwNumMaterials_;     //マテリアル数
  LPD3DXMESH                pMesh_;              //メッシュデータのポインタ
  D3DMATERIAL9*             pMeshMaterials_;     //メッシュマテリアルへのポインタ
  LPDIRECT3DTEXTURE9*       pMeshTextures_;      //メッシュのテクスチャ格納用ポインタ

  //アニメーションがあるモデルのメッシュデータ
  LPD3DXFRAME               pFrameRoot_;         //ルートフレーム
  LPD3DXANIMATIONCONTROLLER pAnimController_;    //アニメーションコントローラオブジェクト
  LPD3DXANIMATIONSET*       ppAnimSet_;          //アニメーションセット
  int                       animNumMax_;         //アニメーション数

  //FBXのメッシュデータ
  FBXMESHAMG*               fbxMeshData_;

  //バンディングボックスデータ
  bool                      isInitBBox_;         //BBox_の生成確認フラグ 生成時:true
  BBOX                      BBox_;               //バウンディングボックス

  //リスト化用のデータ
  MESHDATA*                 prev_;                //リストの前情報へのリンク
  MESHDATA*                 next_;                //リストの次情報へのリンク
};

MESHDATA* g_headMeshPointer;                       //メッシュデータ双方向リストの先頭ポインタ
MESHDATA* g_endMeshPointer;                        //メッシュデータ双方向リストの最後尾ポインタ

//階層内、全てのフレームをリリースする
void FreeAnim(LPD3DXFRAME pFrame)
{
  if(pFrame == NULL)
  {
    return;
  }

  if(pFrame->pMeshContainer != NULL)
  {
    cHierarchy.DestroyMeshContainer(pFrame->pMeshContainer);
  }

  if(pFrame->pFrameSibling != NULL)
  {
    FreeAnim(pFrame->pFrameSibling);
  }

  if(pFrame->pFrameFirstChild != NULL)
  {
    FreeAnim(pFrame->pFrameFirstChild);
  }
}

//引数のアドレスの次方向にメッシュデータを追加する
//return:確保したメモリ
MESHDATA* AddMeshData(MESHDATA* addPlacePointer)
{
  if(addPlacePointer == NULL)
  {
    MESHDATA *newPointer = (MESHDATA*)malloc(sizeof(MESHDATA));
    memset(newPointer, 0, sizeof(MESHDATA));
    newPointer->next_ = NULL;
    newPointer->prev_ = NULL;
    return newPointer;
  }

  //メモリの確保と確保メモリの前後へのリンク
  MESHDATA *newPointer = (MESHDATA*)malloc(sizeof(MESHDATA));
  memset(newPointer, 0, sizeof(MESHDATA));
  newPointer->next_ = addPlacePointer->next_;
  newPointer->prev_ = addPlacePointer;

  //前後のメモリのリンク
  addPlacePointer->next_ = newPointer;
  if(newPointer->next_)
  {
    newPointer->next_->prev_ = newPointer;
  }
  return newPointer;
}

//引数のアドレスのメッシュデータのメモリを解放し、前後のリストをつなげる
void DeleteMeshData(MESHDATA* deletePointer)
{
  //マテリアルの解放
  if(deletePointer->pMeshMaterials_ != NULL)
    delete[] deletePointer->pMeshMaterials_;

  //テクスチャの解放
  if(deletePointer->pMeshTextures_)
  {
    for(DWORD j = 0; j < deletePointer->dwNumMaterials_; j++)
    {
      if(deletePointer->pMeshTextures_[j])
        deletePointer->pMeshTextures_[j]->Release();
    }
    delete[] deletePointer->pMeshTextures_;
  }
  if(deletePointer->pMesh_ != NULL)
    deletePointer->pMesh_->Release();

  //階層の解放
  FreeAnim(deletePointer->pFrameRoot_);
  cHierarchy.DestroyFrame(deletePointer->pFrameRoot_);
  //free(deletePointer->pAnimController_);
  deletePointer->pAnimController_ = NULL;

  MESHDATA* prevPointer = deletePointer->prev_;
  MESHDATA* nextPointer = deletePointer->next_;
  free(deletePointer);
  deletePointer = NULL;

  if(prevPointer)
  {
    prevPointer->next_ = nextPointer;
  }
  if(nextPointer)
  {
    nextPointer->prev_ = prevPointer;
  }
}

//再起処理を使った全メッシュデータの解放
//1番最初に呼び出すときにメッシュデータのリストの先頭を指定する
void AllDeleteMeshData(MESHDATA* deletePointer)
{
  if(!g_endMeshPointer)
  {
    return;
  }

  if(deletePointer == g_endMeshPointer)
  {
    g_endMeshPointer = deletePointer->prev_;
    DeleteMeshData(deletePointer);
    return;
  }
  AllDeleteMeshData(deletePointer->next_);
  g_endMeshPointer = deletePointer->prev_;
  DeleteMeshData(deletePointer);
}

//ボーンのワールド行列をコンテナーに格納する(AllocateAllBoneMatricesの内部呼び出し関数)
HRESULT AllocateBoneMatrix(MESHDATA* pMesh, LPD3DXMESHCONTAINER pMeshContainerBase)
{
  MYFRAME *pFrame = NULL;
  DWORD dwBoneNum = 0;

  MYMESHCONTAINER *pMeshContainer = (MYMESHCONTAINER*)pMeshContainerBase;
  if(pMeshContainer->pSkinInfo == NULL)
  {
    return S_OK;
  }
  dwBoneNum = pMeshContainer->pSkinInfo->GetNumBones();
  pMeshContainer->ppBoneMatrix = new D3DXMATRIX*[dwBoneNum];

  for(DWORD i = 0; i < dwBoneNum; i++)
  {
    pFrame = (MYFRAME*)D3DXFrameFind(pMesh->pFrameRoot_, pMeshContainer->pSkinInfo->GetBoneName(i));
    if(pFrame == NULL)
    {
      return E_FAIL;
    }
    pMeshContainer->ppBoneMatrix[i] = &pFrame->CombinedTransformationMatrix;
  }
  return S_OK;
}

//すべてのボーンのワールド行列をコンテナーに格納する
HRESULT AllocateAllBoneMatrices(MESHDATA* pMesh, LPD3DXFRAME pFrame)
{
  if(pFrame->pMeshContainer != NULL)
  {
    if(FAILED(AllocateBoneMatrix(pMesh, pFrame->pMeshContainer)))
    {
      return E_FAIL;
    }
  }
  if(pFrame->pFrameSibling != NULL)
  {
    if(FAILED(AllocateAllBoneMatrices(pMesh, pFrame->pFrameSibling)))
    {
      return E_FAIL;
    }
  }
  if(pFrame->pFrameFirstChild != NULL)
  {
    if(FAILED(AllocateAllBoneMatrices(pMesh, pFrame->pFrameFirstChild)))
    {
      return E_FAIL;
    }
  }
  return S_OK;
}

//指定したモデルデータを読み込む
//fileDirectory :モデルデータのディレクトリ
//meshKind      :メッシュの種類(MESH_KINDを参照)
//animationData_ :アニメーション情報
//戻り値:3Dデータのハンドル
void* LoadModel(LPCSTR fileDirectory, int meshKind)
{
  g_endMeshPointer = AddMeshData(g_endMeshPointer);
  if(g_headMeshPointer == NULL)
  {
    g_headMeshPointer = g_endMeshPointer;
  }
  g_endMeshPointer->meshKind_ = (MESH_KIND)meshKind;

  //アニメーションメッシュの読み込み処理
  if(meshKind == ANIMATION_MESH){
    // Xファイルからアニメーションメッシュを読み込み作成する
    if(FAILED(
      D3DXLoadMeshHierarchyFromX(fileDirectory, D3DXMESH_MANAGED, g_pd3dDevice, &cHierarchy,
      NULL, &g_endMeshPointer->pFrameRoot_, &g_endMeshPointer->pAnimController_)))
    {
      WriteOutLog("loadXFile ファイル名 : %s 読み込み失敗", fileDirectory);
      MessageBox(NULL, "Xファイルの読み込みに失敗しました", fileDirectory, MB_OK);
      DeleteMeshData(g_endMeshPointer);
      return NULL;
    }
    AllocateAllBoneMatrices(g_endMeshPointer, g_endMeshPointer->pFrameRoot_);
    //アニメーションのトラック数の取得とアニメーションセットのメモリ確保
    g_endMeshPointer->animNumMax_ = g_endMeshPointer->pAnimController_->GetNumAnimationSets();
    g_endMeshPointer->ppAnimSet_ = (LPD3DXANIMATIONSET*)malloc(sizeof(LPD3DXANIMATIONSET) * g_endMeshPointer->animNumMax_);
    if(g_endMeshPointer->ppAnimSet_ == NULL)
    {
      WriteOutLog("loadXFile ファイル名 : %s アニメーショントラック作成失敗", fileDirectory);
      DeleteMeshData(g_endMeshPointer);
      return NULL;
    }
    //アニメーショントラックを得る
    for(DWORD i = 0; i<g_endMeshPointer->animNumMax_; i++)
    {
      g_endMeshPointer->pAnimController_->GetAnimationSet(i, &g_endMeshPointer->ppAnimSet_[i]);
    }
    WriteOutLog("loadXFile 成功 ファイル名 : %s", fileDirectory);
    return (void*)g_endMeshPointer;
  }

  //アニメーションがないメッシュの読み込み処理
  if(meshKind == MESH){
    //マテリアル情報の一時保存変数
    LPD3DXBUFFER pD3DXMtrlBuffer;

    // バッファに指定ファイル名からメッシュを読み込む
    if(FAILED(D3DXLoadMeshFromX(fileDirectory, D3DXMESH_SYSTEMMEM,
       g_pd3dDevice, NULL,
       &pD3DXMtrlBuffer, NULL, &g_endMeshPointer->dwNumMaterials_,
       &g_endMeshPointer->pMesh_)))
    {
      //読み込み失敗時、上の階層から読み込みを試みる
      char string[50];
      sprintf_s(string, sizeof(char) * 50, "..\\%s", fileDirectory);
      if(FAILED(D3DXLoadMeshFromX(string, D3DXMESH_SYSTEMMEM,
         g_pd3dDevice, NULL,
         &pD3DXMtrlBuffer, NULL, &g_endMeshPointer->dwNumMaterials_,
         &g_endMeshPointer->pMesh_)))
      {
        //読み込めなかった際のエラー処理
        sprintf_s(string, sizeof(char) * 50, "Could not find %s", fileDirectory);
        MessageBox(NULL, string, "Meshes", MB_OK);
        WriteOutLog("loadXFile ファイル名 : %s 読み込み失敗", fileDirectory);
        return NULL;
      }
    }

    //バッファからマテリアル情報とテクスチャ名を読み込む
    g_endMeshPointer->pMeshMaterials_ = (D3DMATERIAL9*)malloc(sizeof(D3DMATERIAL9) * g_endMeshPointer->dwNumMaterials_);
    g_endMeshPointer->pMeshTextures_ = (LPDIRECT3DTEXTURE9*)malloc(sizeof(LPDIRECT3DTEXTURE9) * g_endMeshPointer->dwNumMaterials_);

    //データのマテリアル数繰り返す
    for(DWORD i = 0; i < g_endMeshPointer->dwNumMaterials_; i++)
    {
      D3DXMATERIAL* d3dxMaterials = (D3DXMATERIAL*)pD3DXMtrlBuffer->GetBufferPointer();

      //マテリアルをコピー
      g_endMeshPointer->pMeshMaterials_[i] = d3dxMaterials[i].MatD3D;
      //マテリアルのアンビエント色を設営(D3DXの場合必要なし)
      g_endMeshPointer->pMeshMaterials_[i].Ambient = g_endMeshPointer->pMeshMaterials_[i].Diffuse;

      g_endMeshPointer->pMeshTextures_[i] = NULL;
      if(d3dxMaterials[i].pTextureFilename != NULL &&
         lstrlen(d3dxMaterials[i].pTextureFilename) > 0)
      {
        // テクスチャの形成
        if(FAILED(D3DXCreateTextureFromFile(g_pd3dDevice,
           d3dxMaterials[i].pTextureFilename,
           &g_endMeshPointer->pMeshTextures_[i])))
        {
          //テクスチャの読み込み失敗時、上の階層から再度読み込みを試みる
          const TCHAR* strPrefix = TEXT("..\\");
          TCHAR strTexture[MAX_PATH];
          strcpy_s(strTexture, MAX_PATH, strPrefix);
          StringCchCat(strTexture, MAX_PATH, d3dxMaterials[i].pTextureFilename);
          if(FAILED(D3DXCreateTextureFromFile(g_pd3dDevice,
             strTexture,
             &g_endMeshPointer->pMeshTextures_[i])))
          {
            //テクスチャが見つからなかった場合のエラー処理
            MessageBox(NULL, "Could not find texture map", "Meshes.exe", MB_OK);
            WriteOutLog("loadXFile ファイル名 : %s テクスチャ:%sの読み込み失敗", fileDirectory, d3dxMaterials[i].pTextureFilename);
          }
        }
      }
    }
    pD3DXMtrlBuffer->Release();

    WriteOutLog("loadXFile 成功 ファイル名 : %s", fileDirectory);
    return (void*)g_endMeshPointer;
  }

  //fbxファイルの読み込み
  if(meshKind == FBX_MESH)
  {
    g_endMeshPointer->fbxMeshData_ = new FBXMESHAMG();
    g_endMeshPointer->fbxMeshData_->Create(g_pd3dDevice, fileDirectory);
    return (void*)g_endMeshPointer;
  }
  return NULL;
}

//現在のhandleを破棄して指定したモデルデータを読み込む
//handle        :上書きしたいメモリに入っているhandle
//fileDirectory :モデルデータのディレクトリ
//meshKind      :メッシュの種類(MESH_KINDを参照)
//animationData_ :アニメーション情報
//戻り値:3Dデータのハンドル
void* ReloadXFile(void* handle, LPCSTR fileDirectory, int meshKind_)
{
  if(handle != NULL)
    DeleteMeshData((MESHDATA*)handle);
  return LoadModel(fileDirectory, meshKind_);
}

//FBXファイルのアニメーションをハンドルに読み込ませる
// controlName      : 制御用アニメーション名
// animationFileName: アニメーションデータ名(ディレクトリ)
// meshHandle       : 3Dデータのハンドル
void AddFbxAnimation(char* controlName,char* animationFileName,void* meshHandle)
{
  MESHDATA* meshData = (MESHDATA*)meshHandle;
  if(meshData == NULL) { return; }
  if(meshData->meshKind_ != FBX_MESH) { return; }

  meshData->fbxMeshData_->AddMotion(controlName, animationFileName);
}

//指定したハンドルのXファイルのメモリを解放する
void ReleaseXFile(void* handle)
{
  DeleteMeshData((MESHDATA*)handle);
}

//2Dテクスチャデータ関係
/*■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■*/

// 2D画像情報構造体
struct TEXTURE2D_DATA
{
  int                 numX_;            // スプライトシートの場合の枚数
  int                 numY_;            // スプライトシートの場合の枚数
  int                 nWidth_;          // 幅
  int                 nHeight_;         // 高さ
  char*               pictureFileName_; // ファイル名
  LPDIRECT3DTEXTURE9  pictureInfo_;     // 読み込んだ画像データ

  //リスト化用のデータ
  TEXTURE2D_DATA*     prev_;            // リストの前情報へのリンク
  TEXTURE2D_DATA*     next_;            // リストの次情報へのリンク
};

TEXTURE2D_DATA* gHeadTexturePointer; //テクスチャデータ双方向リストの先頭ポインタ
TEXTURE2D_DATA* gEndTexturePointer; //テクスチャデータ双方向リストの最後尾ポインタ

//引数のアドレスの次方向にテクスチャデータを追加する
//return:確保したメモリ
TEXTURE2D_DATA* AddTextureData(TEXTURE2D_DATA* addPlacePointer)
{
  //前後がなければ確保して、前後をNULLに
  if(addPlacePointer == NULL)
  {
    TEXTURE2D_DATA *newPointer = (TEXTURE2D_DATA*)malloc(sizeof(TEXTURE2D_DATA));
    memset(newPointer, 0, sizeof(TEXTURE2D_DATA));
    newPointer->next_ = NULL;
    newPointer->prev_ = NULL;
    return newPointer;
  }

  //メモリの確保と確保メモリの前後へのリンク
  TEXTURE2D_DATA *newPointer = (TEXTURE2D_DATA*)malloc(sizeof(TEXTURE2D_DATA));
  memset(newPointer, 0, sizeof(TEXTURE2D_DATA));
  newPointer->next_ = addPlacePointer->next_;
  newPointer->prev_ = addPlacePointer;

  //前後のメモリのリンク
  addPlacePointer->next_ = newPointer;
  if(newPointer->next_)
  {
    newPointer->next_->prev_ = newPointer;
  }
  return newPointer;
}

//引数のアドレスのテクスチャデータのメモリを解放し、前後のリストをつなげる
void DeleteTextureData(TEXTURE2D_DATA* deletePointer)
{
  TEXTURE2D_DATA* prevPointer = deletePointer->prev_;
  TEXTURE2D_DATA* nextPointer = deletePointer->next_;
  free(deletePointer);
  deletePointer = NULL;

  if(prevPointer)
  {
    prevPointer->next_ = nextPointer;
  }
  if(nextPointer)
  {
    nextPointer->prev_ = prevPointer;
  }
}

//再起処理を使った全テクスチャデータの解放
//1番最初に呼び出すときにテクスチャデータのリストの先頭を指定する
void AllDeleteTextureData(TEXTURE2D_DATA* deletePointer)
{
  if(!g_endMeshPointer)
  {
    return;
  }

  if(deletePointer == gEndTexturePointer)
  {
    gEndTexturePointer = deletePointer->prev_;
    DeleteTextureData(deletePointer);
    return;
  }
  AllDeleteTextureData(deletePointer->next_);
  gEndTexturePointer = deletePointer->prev_;
  DeleteTextureData(deletePointer);
}

//グラフィックバッファの作成
LPDIRECT3DTEXTURE9 CreateTextureBuffer(char *fileDirectory)
{
  LPDIRECT3DTEXTURE9  p2dGraph = NULL;  // ロードテクスチャ
  D3DFORMAT           dfTexFormat;      // テクスチャフォーマット
  D3DXIMAGE_INFO      image;            // 画像情報

  // 画像の大きさなどを取得
  D3DXGetImageInfoFromFile((LPCSTR)fileDirectory, &image);


  // ピクセルフォーマット
  dfTexFormat = D3DFMT_A8R8G8B8;

    //  デバイスにデータを登録
    D3DXCreateTextureFromFileEx(
    g_pd3dDevice,                   // Direct3Dデバイス
    (LPCSTR)fileDirectory,          // ファイル名
    image.Width,                    // ファイルの横幅サイズ(D3DX_DEFAULT ならDirectXが自動設定)
    image.Height,                   // ファイルの縦幅サイズ(D3DX_DEFAULT ならDirectXが自動設定)
    1,                              // ミップマップのレベル
    D3DUSAGE_RENDERTARGET,          // テクスチャの使い方
    image.Format,                   // テクスチャのカラーフォーマットの設定
    D3DPOOL_DEFAULT,                // テクスチャメモリの管理方法
    D3DX_FILTER_POINT,              // フィルタリング方法
    D3DX_DEFAULT,                   // ミップマップのフィルタリング方法
    NULL,                           // 抜き色の指定。指定された色は透明色として扱われる
    NULL,                           // 元の画像の情報が欲しい場合に使用する
    NULL,                           // 256色のカラーの場合のみカラーパレットが格納される
    &p2dGraph);                     // テクスチャポインタ(データ格納用)

  return p2dGraph;
}

//2D用のテクスチャの読み込み
void* LoadTexture(char *fileDirectory)
{
  D3DXIMAGE_INFO picture_;				// 画像の大きさなどのデータ
  if(E_FAIL == D3DXGetImageInfoFromFile((LPCSTR)fileDirectory, &picture_))  // 画像の大きさなどを取得
  {
    WriteOutLog("loadTexture ファイル名 : %s 読み込み失敗", fileDirectory);
    return NULL;
  }

  //画像情報を取得
  LPDIRECT3DTEXTURE9 texture_info = CreateTextureBuffer(fileDirectory);
  if(!texture_info)
  {
    WriteOutLog("loadTexture ファイル名 : %s 読み込み失敗", fileDirectory);
    return NULL;
  }//エラー

  //テクスチャ用メモリの確保
  TEXTURE2D_DATA* newTextureData = AddTextureData(gEndTexturePointer);
  if(!newTextureData)
  {
    WriteOutLog("loadTexture ファイル名 : %s メモリ確保失敗", fileDirectory);
    return NULL;
  }//エラー

  //データをメモリに格納
  newTextureData->numX_ = 1;                           // 画像の枚数
  newTextureData->numY_ = 1;                           // 画像の枚数
  newTextureData->pictureFileName_ = fileDirectory;  // ファイル名の登録
  newTextureData->nHeight_ = picture_.Height;           // 画像サイズの登録
  newTextureData->nWidth_ = picture_.Width;             // 画像サイズの登録
  newTextureData->pictureInfo_ = texture_info;        // 画像情報を入れる

  void* texture_handle = (void*)newTextureData;              // ハンドルを登録する
  WriteOutLog("loadTexture ファイル名 : %s 成功", fileDirectory);
  return texture_handle;
}

//2D用の連番テクスチャの読み込み
void* LoadBlockTexture(char *fileDirectory, int nSplitX, int nSplitY)
{
  D3DXIMAGE_INFO picture_; // 画像の大きさなどのデータ
  if(E_FAIL == D3DXGetImageInfoFromFile((LPCSTR)fileDirectory, &picture_))  // 画像の大きさなどを取得
  {
    WriteOutLog("loadTexture ファイル名 : %s 読み込み失敗", fileDirectory);
    return NULL;
  }

  //画像情報を取得
  LPDIRECT3DTEXTURE9 textureInfo = CreateTextureBuffer(fileDirectory);
  if(!textureInfo)
  {
    WriteOutLog("loadTexture ファイル名 : %s 読み込み失敗", fileDirectory);
    return NULL;
  }//エラー

  //テクスチャ用メモリの確保
  TEXTURE2D_DATA* newTextureData = AddTextureData(gEndTexturePointer);
  if(!newTextureData)
  {
    WriteOutLog("loadTexture ファイル名 : %s メモリ確保失敗", fileDirectory);
    return NULL;
  }//エラー

   //データをメモリに格納
  newTextureData->numX_ = nSplitX;                     // 画像の枚数
  newTextureData->numY_ = nSplitY;                     // 画像の枚数
  newTextureData->pictureFileName_ = fileDirectory;  // ファイル名の登録
  newTextureData->nHeight_ = picture_.Height / nSplitY; // 画像サイズの登録
  newTextureData->nWidth_ = picture_.Width / nSplitX;   // 画像サイズの登録
  newTextureData->pictureInfo_ = textureInfo;        // 画像情報を入れる

  void* texture_handle = (void*)newTextureData;              // ハンドルを登録する
  WriteOutLog("loadTexture ファイル名 : %s 成功", fileDirectory);
  return texture_handle;
}

//指定したハンドルのテクスチャデータのメモリを解放する
void ReleaseTexture(void* handle)
{
  DeleteTextureData((TEXTURE2D_DATA*)handle);
}

//ウィンドウとDirectXの初期化
/*■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■*/

//以前に初期化されたすべてのオブジェクトを解放する
void Cleanup()
{
  //メッシュを破棄
  AllDeleteMeshData(g_headMeshPointer);
  //テクスチャを破棄
  AllDeleteTextureData(gHeadTexturePointer);
  g_headMeshPointer = NULL;
  gHeadTexturePointer = NULL;

  if(g_pFont != NULL)
    g_pFont->Release();

  if(g_pd3dDevice != NULL)
    g_pd3dDevice->Release();

  if(g_pD3D != NULL)
    g_pD3D->Release();

  if(g_pXshaderEffect != NULL)
    g_pXshaderEffect->Release();

  if(g_p2dShaderEffect != NULL)
    g_p2dShaderEffect->Release();

  if(g_pAnimXshaderEffect != NULL)
    g_pAnimXshaderEffect->Release();

  if(g_pSpeedershaderEffect != NULL)
    g_pSpeedershaderEffect->Release();

  if(g_pVertexDeclaration != NULL)
    g_pVertexDeclaration->Release();
}

// 標準 Windows メッセージのハンドラー
LRESULT WINAPI MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  switch(msg)
  {
  case WM_DESTROY:
    PostQuitMessage(0);
    return 0;
  }

  return DefWindowProc(hWnd, msg, wParam, lParam);
}

//フルスクリーンフラグ
static bool gIsFullScreen = false;
static int gDispWidth = 640;
static int gDispHeight = 360;
static char gApplicationName[256] = "TKYNG Library";

//ウィンドウの大きさとウィンドウ名の決定
void SetWindowOption(int width, int height, char* name, bool isFullScreen)
{
  gDispWidth = width;
  gDispHeight = height;
  strcpy_s(gApplicationName, sizeof(char) * 256, name);
  gIsFullScreen = isFullScreen;
}

// Direct3Dの初期化
bool InitD3D(HWND hWnd)
{
  // Create the D3D object.
  if(NULL == (g_pD3D = Direct3DCreate9(D3D_SDK_VERSION)))
    return false;

  //デバイスの設定
  D3DPRESENT_PARAMETERS d3dpp;
  ZeroMemory(&d3dpp, sizeof(d3dpp));
  d3dpp.BackBufferWidth = gDispWidth;
  d3dpp.BackBufferHeight = gDispHeight;
  d3dpp.BackBufferFormat = D3DFMT_R5G6B5;
  d3dpp.BackBufferCount = 1;
  d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
  d3dpp.Windowed = TRUE;
  d3dpp.EnableAutoDepthStencil = TRUE;
  d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
  d3dpp.Flags = D3DPRESENTFLAG_LOCKABLE_BACKBUFFER;
  d3dpp.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT; // リフレッシュレート
  d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_DEFAULT; // 表示周期

  //デバイスを作成
  if(FAILED(g_pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
    D3DCREATE_HARDWARE_VERTEXPROCESSING | D3DCREATE_MULTITHREADED,
     &d3dpp, &g_pd3dDevice)))
  {
    if(FAILED(g_pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
       D3DCREATE_HARDWARE_VERTEXPROCESSING | D3DCREATE_MULTITHREADED,
       &d3dpp, &g_pd3dDevice)))
    {
      if(FAILED(g_pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_REF, hWnd,
         D3DCREATE_HARDWARE_VERTEXPROCESSING | D3DCREATE_MULTITHREADED,
         &d3dpp, &g_pd3dDevice)))
      {
        if(FAILED(g_pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_REF, hWnd,
           D3DCREATE_HARDWARE_VERTEXPROCESSING | D3DCREATE_MULTITHREADED,
           &d3dpp, &g_pd3dDevice)))
        {
          return false;
        }
      }
    }
  }

  //文字列レンダリングの初期化
  if(FAILED(D3DXCreateFont(g_pd3dDevice, g_fontSize, g_fontSize / 2, FW_REGULAR, NULL, false, SHIFTJIS_CHARSET,
     OUT_DEFAULT_PRECIS, PROOF_QUALITY, FIXED_PITCH | FF_MODERN, "ＭＳ ゴシック", &g_pFont)))
  {
    return false;
  }
  
  // Turn off culling, so we see the front and back of the triangle
  //g_pd3dDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);

  // Turn off D3D lighting, since we are providing our own vertex colors
  g_pd3dDevice->SetRenderState(D3DRS_LIGHTING, TRUE);

  //Zバッファの有効
  g_pd3dDevice->SetRenderState(D3DRS_ZENABLE, TRUE);

  //アルファブレンディングを設定する
  g_pd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);// アルファ・ブレンディングを行う
  g_pd3dDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);// 半透明処理を行う
  g_pd3dDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);// 透過処理を行う

  //アルファブレンディング テクスチャの設定
  g_pd3dDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
  g_pd3dDevice->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE);
  g_pd3dDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_MODULATE);
  g_pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
  g_pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);
  g_pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);

  return true;
}

//ウィンドウの作成とDirectXの初期化
HWND InitWindow(HINSTANCE hInstance)
{
  HWND hWnd = NULL;
  WNDCLASSEX wndclass;
  RECT rc;

  //画面中央にウィンドウが行くように計算
  GetWindowRect(GetDesktopWindow(), &rc);
  int createCoordsX;
  int createCoordsY;
  createCoordsX = (rc.right + rc.left - gDispWidth) / 2;
  createCoordsY = (rc.bottom + rc.top - gDispHeight) / 2;

  //ウィンドウ設定
  wndclass.cbSize = sizeof(WNDCLASSEX);
  wndclass.style = CS_HREDRAW | CS_VREDRAW;
  wndclass.lpfnWndProc = MsgProc;
  wndclass.cbClsExtra = 0;
  wndclass.cbWndExtra = 0;
  wndclass.hInstance = hInstance;
  wndclass.hIcon = (HICON)LoadImage(NULL, "icon.ico", IMAGE_ICON, 0, 0, LR_SHARED | LR_LOADFROMFILE);   //アイコン変更
  wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
  wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
  wndclass.lpszMenuName = NULL;
  wndclass.lpszClassName = gApplicationName;
  wndclass.hIconSm = LoadIcon(NULL, IDI_ASTERISK);
  RegisterClassEx(&wndclass);

  if(gIsFullScreen)
  {
    //フルスクリーン時クライアント領域が画面左上に行くように
    long long int windowType = (WS_OVERLAPPED | WS_DLGFRAME | WS_SYSMENU | WS_VISIBLE | WS_POPUP);
    hWnd = CreateWindow(gApplicationName, gApplicationName,
                          windowType, -3, -3, gDispWidth, gDispHeight,
                          GetDesktopWindow(), NULL, wndclass.hInstance, NULL);
  }
  else
  {
    //ウィンドウ時クライアント領域中央が画面中央に合わさるように
    //long long int windowType = (WS_OVERLAPPED | WS_BORDER | WS_DLGFRAME | WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX);
    long long int windowType = (WS_OVERLAPPED | WS_DLGFRAME | WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX);
    hWnd = CreateWindow(gApplicationName, gApplicationName,
                          windowType, createCoordsX, createCoordsY, gDispWidth, gDispHeight,
                          GetDesktopWindow(), NULL, wndclass.hInstance, NULL);
  }

  if(gIsFullScreen)
  {
    //画面設定をフルスクリーンにする
    DEVMODE devMode;
    devMode.dmSize = sizeof(DEVMODE);
    devMode.dmFields = DM_PELSWIDTH | DM_PELSHEIGHT;
    devMode.dmPelsWidth = gDispWidth;
    devMode.dmPelsHeight = gDispHeight;

    ChangeDisplaySettings(&devMode, CDS_FULLSCREEN);
  }

  //ウィンドウの境界線を計算
  RECT rc1;
  RECT rc2;
  float borderSizeX;
  float borderSizeY;
  GetWindowRect(hWnd, &rc1);
  GetClientRect(hWnd, &rc2);
  borderSizeX = ((rc1.right - rc1.left) - (rc2.right - rc2.left));
  borderSizeY = ((rc1.bottom - rc1.top) - (rc2.bottom - rc2.top));
  SetWindowPos(hWnd, NULL,
               createCoordsX - borderSizeX / 2.0f,
               createCoordsY - borderSizeY / 2.0f,
               gDispWidth + borderSizeX,
               gDispHeight + borderSizeY,
               (SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE));

  ShowWindow(hWnd, SW_SHOWDEFAULT);
  UpdateWindow(hWnd);

  return hWnd;
}

//生成したウィンドウの削除とメモリの解放
void DeleteWindow(HINSTANCE hInstance)
{
  UnregisterClass(gApplicationName, hInstance);
}

//shaderの使用頂点情報の宣言
D3DVERTEXELEMENT9 deDecl[] =
{
  {0,   0, D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION   , 0},// 頂点座標
  {0,  16, D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDWEIGHT, 0},// 頂点ブレンディングの重み
  {0,  32, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD   , 0},// uv座標
  {0,  40, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL     , 0},// 法線ベクトル
  {0,  52, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TANGENT    , 1},// 接線データ
  {0,  64, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BINORMAL   , 1},// 従法線データ
  D3DDECL_END()
};

/*
float x_, y_, z_, w;    // 頂点座標
float rhw_;        // 除算数
D3DCOLOR color_;   // 色情報(アルファ値もあり)
float tu_, tv_;     // テクスチャ座標
*/

//shaderの使用頂点情報の宣言
D3DVERTEXELEMENT9 deDecl2[] =
{
  {0,   0, D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION   , 0},// 頂点座標
  {0,  20, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD   , 0},// uv座標
  D3DDECL_END()
};

//shaderの使用頂点情報の宣言
D3DVERTEXELEMENT9 deDecl3[] =
{
  { 0,   0, D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION    , 0 },// 頂点座標
  { 0,  16, D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDWEIGHT , 0 },// 頂点ブレンディングの重み
  { 0,  32, D3DDECLTYPE_FLOAT2, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD    , 0 },// uv座標
  { 0,  40, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL      , 0 },// 法線ベクトル
  { 0,  52, D3DDECLTYPE_FLOAT3, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TANGENT     , 1 },// 接線データ
  { 0,  64, D3DDECLTYPE_FLOAT4, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BINORMAL    , 1 },// 従法線データ + index
  D3DDECL_END()
};

// シェーダーのエフェクトインターフェースを生成する
bool MakeShaderEffects()
{
  HRESULT         hr, hr2, hr3, hr4;
  LPD3DXBUFFER    pCompileErrors = NULL;
  DWORD           dwShaderFlags = 0;
  FILE            *rfp;
  char            *pcCode;
  int             nCodeSize;

  // エフェクト作成
  // 非アニメーションXfileメッシュ用エフェクト
  if((rfp = fopen("shader/xShader.fx", "rb")) != NULL)
  {
    fseek(rfp, 0, SEEK_END);
    nCodeSize = ftell(rfp);
    fseek(rfp, 0, SEEK_SET);
    pcCode = (char *)malloc(nCodeSize);
    fread(pcCode, nCodeSize, 1, rfp);
    hr = g_pd3dDevice->CreateVertexDeclaration(deDecl, &g_pVertexDeclaration);  // 頂点データフォーマット作成
    if(FAILED(hr)) return hr;
    hr = D3DXCreateEffect(g_pd3dDevice, pcCode, nCodeSize, NULL, NULL, dwShaderFlags, NULL, &g_pXshaderEffect, &pCompileErrors);
    free(pcCode);
    fclose(rfp);
  }
  else
  {
    hr = -1;
  }
  // 非アニメーションXfileメッシュ用エフェクト
  if((rfp = fopen("shader/texture2DShader.fx", "rb")) != NULL)
  {
    fseek(rfp, 0, SEEK_END);
    nCodeSize = ftell(rfp);
    fseek(rfp, 0, SEEK_SET);
    pcCode = (char *)malloc(nCodeSize);
    fread(pcCode, nCodeSize, 1, rfp);
    hr3 = g_pd3dDevice->CreateVertexDeclaration(deDecl2, &g_pVertexDeclaration2);  // 頂点データフォーマット作成
    if(FAILED(hr3)) return hr3;
    hr3 = D3DXCreateEffect(g_pd3dDevice, pcCode, nCodeSize, NULL, NULL, dwShaderFlags, NULL, &g_p2dShaderEffect, &pCompileErrors);
    free(pcCode);
    fclose(rfp);
  }
  else
  {
    hr3 = -1;
  }
  //アニメーションXfileメッシュ用エフェクト
  if((rfp = fopen("shader/AnimXShader.fx", "rb")) != NULL)
  {
    fseek(rfp, 0, SEEK_END);
    nCodeSize = ftell(rfp);
    fseek(rfp, 0, SEEK_SET);
    pcCode = (char *)malloc(nCodeSize);
    fread(pcCode, nCodeSize, 1, rfp);
    hr2 = g_pd3dDevice->CreateVertexDeclaration(deDecl, &g_pVertexDeclaration);  // 頂点データフォーマット作成
    if(FAILED(hr2)) return hr2;
    hr2 = D3DXCreateEffect(g_pd3dDevice, pcCode, nCodeSize, NULL, NULL, dwShaderFlags, NULL, &g_pAnimXshaderEffect, &pCompileErrors);
    free(pcCode);
    fclose(rfp);
  }
  else
  {
    hr2 = -1;
  }

  //高速用シェーダーエフェクト
  if ((rfp = fopen("shader/speederShader.fx", "rb")) != NULL)
  {
    fseek(rfp, 0, SEEK_END);
    nCodeSize = ftell(rfp);
    fseek(rfp, 0, SEEK_SET);
    pcCode = (char *)malloc(nCodeSize);
    fread(pcCode, nCodeSize, 1, rfp);
    hr4 = g_pd3dDevice->CreateVertexDeclaration(deDecl3, &g_pVertexDeclaration3);  // 頂点データフォーマット作成
    //if (FAILED(hr4)) return hr4;
    hr4 = D3DXCreateEffect(g_pd3dDevice, pcCode, nCodeSize, NULL, NULL, dwShaderFlags, NULL, &g_pSpeedershaderEffect, &pCompileErrors);
    free(pcCode);
    fclose(rfp);
  }
  else
  {
    hr4 = -1;
  }

  if(FAILED(hr) || FAILED(hr2) || FAILED(hr3) || FAILED(hr4))
  {
    char *szErrors;
    FILE *fp = fopen("EffectErrors.txt", "w");
    if(pCompileErrors)
    { // コンパイルエラーならエラーメッセージ出力
      szErrors = (char *)(pCompileErrors->GetBufferPointer());
      fputs(szErrors, fp);
    }
    else
    { // ファイルエラーならメッセージ出力
      fprintf(fp, "Can't open fx file\n");
    }
    fclose(fp);
    MessageBox(NULL, "Effect creation error( For detail, see EffectErrors.txt )", "Error", MB_OK);
    return hr;
  }

  return true;
}

//3Dのデバイスの取得
LPDIRECT3DDEVICE9 GetDevice()
{
  return g_pd3dDevice;
}

//shaderの使用頂点情報の取得
LPD3DVERTEXELEMENT9 GetVertexElement()
{
  return deDecl;
}

//スキンアニメーション関係
/*■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■*/

//メッシュデータからアニメーションデータをコピーする
//meshHandle : 任意のモデルデータのハンドル
//返り値      : コピーしたアニメーション情報
ANIMATION_DATA GetAnimationData(void* meshHandle)
{
  MESHDATA* pData = (MESHDATA*)meshHandle;
  ANIMATION_DATA result;
  memset(&result, 0, sizeof(ANIMATION_DATA));

  //fbx用アニメーション進行度データ
  result.frame = 0;
  result.motionName = "default";

  if(pData == NULL)
  {
    return result;
  }
  //メッシュデータにx_ファイル用アニメーションデータがなければスルー
  if(pData->pAnimController_ != NULL)
  {
    result.pAnimController_ = pData->pAnimController_;
    result.animNumMax_ = pData->animNumMax_;
    result.ppAnimSet_ = pData->ppAnimSet_;
  }

  return result;
}

//任意の番号のアニメーショントラックを有効にする
//animationData_ : アニメーション情報
//animID        : 切り替え後のアニメーション番号
//shiftFrame    : 切り替えにかける時間
//loopRate      : アニメーション速度倍率
void ChangeAnimation(ANIMATION_DATA* animationData, int animID, int shiftFrame, float loopRate)
{
  if(animationData == NULL)
  {
    return;
  }
  if(animID >= animationData->animNumMax_ || animationData->pAnimController_ == NULL || animationData->ppAnimSet_ == NULL)
  {
    return;
  }

  // 現在のアニメーションをトラック１に移行
  animationData->pAnimController_->SetTrackAnimationSet(1, animationData->ppAnimSet_[animationData->animID]);
  // 新しいアニメーションのセット
  animationData->pAnimController_->SetTrackAnimationSet(0, animationData->ppAnimSet_[animID]);
  // トラックの合成
  animationData->pAnimController_->SetTrackEnable(0, true);
  animationData->pAnimController_->SetTrackEnable(1, true);
  animationData->pAnimController_->SetTrackWeight(0, 0);
  animationData->pAnimController_->SetTrackWeight(1, 1);
  animationData->pAnimController_->SetTrackPosition(0, 0);
  animationData->pAnimController_->SetTrackPosition(1, animationData->nowTime);
  // アニメーション番号の切り替え
  animationData->prevAnimID = animationData->animID;
  animationData->animID = animID;
  // 時間の初期化
  animationData->prevTime = animationData->nowTime;
  animationData->nowTime = 0;
  // 切り替え時間の設定
  animationData->shiftFrame = shiftFrame;
  // アニメーション終了時間の設定
  animationData->loopEndTime = animationData->ppAnimSet_[animID]->GetPeriod() * loopRate;
  // アニメーション速度倍率の設定
  animationData->prevLoopRate = animationData->loopRate;
  animationData->loopRate = loopRate ? loopRate : 1.0f;
  //グローバル時間を更新
  animationData->pAnimController_->AdvanceTime(0, NULL); 

  //アニメーションIDが同じだった場合時間の調整
  if (animationData->prevAnimID == animationData->animID)
  {
    animationData->nowTime = animationData->prevTime * animationData->loopRate / animationData->prevLoopRate;
  }
}

//FBXデータのアニメーション切り替え
//animationData_ : アニメーション管理データ
//controlName : アニメーション管理名
//meshHandle  : メッシュデータハンドル
void ChangeFbxAnimation(ANIMATION_DATA* animationData, char* controlName,void* meshHandle)
{
  MESHDATA* meshData = (MESHDATA*)meshHandle;
  if(meshData == NULL) { return; }
  if(meshData->meshKind_ != FBX_MESH) { return; }

  meshData->fbxMeshData_->Play(animationData, controlName);
  animationData->motionName = controlName;
}

//アニメーションの終了時間補正値
#define ANIM_ADJUST_TIME (0.0001f)

//アニメーションを更新する
int UpdateAnimation(ANIMATION_DATA* animationData)
{
  if(animationData == NULL)
  {
    return false;
  }
  if(animationData->pAnimController_ == NULL || animationData->ppAnimSet_ == NULL)
  {
    return false;
  }
  // アニメーションが合成中かを判定
  float shiftEndTime = animationData->shiftFrame * ANIMATION_ADVANCE_TIME;
  if(animationData->nowTime < shiftEndTime)
  {
    // 合成中
    float weight = animationData->nowTime / shiftEndTime;
    animationData->pAnimController_->SetTrackWeight(0, weight);
    animationData->pAnimController_->SetTrackWeight(1, 1 - weight);
  }
  else
  {
    // 合成終了
    animationData->pAnimController_->SetTrackWeight(0, true);
    animationData->pAnimController_->SetTrackEnable(1, false);
  }
  // アニメーション更新
  animationData->nowTime += ANIMATION_ADVANCE_TIME;
  animationData->pAnimController_->SetTrackPosition(0, animationData->nowTime / animationData->loopRate);
  animationData->pAnimController_->SetTrackPosition(1, animationData->prevTime / animationData->prevLoopRate);
  animationData->pAnimController_->AdvanceTime(0, NULL);
  //アニメーションループ
  if(animationData->nowTime >= animationData->loopEndTime)
  {
    //アニメーションの1ループを超えていたら終わりに戻す
    //animationData_->nowTime = animationData_->endTime - 0.01;
    return animationData->nowTime / animationData->loopEndTime;
  }
  return 0;
}

//FBXデータのアニメーション更新
//animationData_ : アニメーション管理データ
//meshHandle  : メッシュデータハンドル
//rate        : 再生速度倍率
void UpdateFbxAnimation(ANIMATION_DATA* animationData, void* meshHandle, float rate)
{
  MESHDATA* meshData = (MESHDATA*)meshHandle;
  if(meshData == NULL) { return; }
  if(meshData->meshKind_ != FBX_MESH) { return; }

  meshData->fbxMeshData_->Animate(animationData, 0.016667f * GetSpeedRate() * rate);
}

//描画関係
/*■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■*/

static float g_fogStartDistance = 50.0f;
static float g_fogEndDistance = 70.0f;

//フォグの開始距離と完全濃くなる距離
void SetFogDistance(float start, float end)
{
  g_fogStartDistance = start;
  g_fogEndDistance = end;
}

static bool g_canBeginRender = false;

//メッシュコンテナに基づいてレンダリングする
void RenderMeshContainer(LPDIRECT3DDEVICE9 pDevice, MYMESHCONTAINER* pMeshContainer, MYFRAME* pFrame, D3DXMATRIX worldMat)
{
  DWORD i, k, m;
  DWORD dwBoneIndex, dwBlendMatrixNum;
  DWORD dwPrevBoneID;
  LPD3DXBONECOMBINATION pBoneCombination;
  UINT iMatrixIndex;
  D3DXMATRIX mStack;
  //アクティブなテクニックを設定する。
  g_pAnimXshaderEffect->SetTechnique("RenderSceneWithTexture");

  //とりあえず1回だけ読み込み
  LPDIRECT3DTEXTURE9 normalMap = NULL;
  int static flag = 0;
  if(flag == 0)
  {
    (D3DXCreateTextureFromFile(g_pd3dDevice,
     "res/texture/3D/default_normal.png",
     &normalMap));
    flag++;
  }

  if(normalMap)
    g_pAnimXshaderEffect->SetTexture("g_MeshNormalMap", normalMap);

  //スキンメッシュの場合
  if(pMeshContainer->pSkinInfo != NULL)
  {
    pBoneCombination = (LPD3DXBONECOMBINATION)pMeshContainer->pBoneBuffer->GetBufferPointer();

    dwPrevBoneID = UINT_MAX;
    for(i = 0; i < pMeshContainer->dwBoneNum; i++)
    {
      dwBlendMatrixNum = 0;
      for(k = 0; k< pMeshContainer->dwWeight; k++)
      {
        if(pBoneCombination[i].BoneId[k] != UINT_MAX)
        {
          dwBlendMatrixNum = k;
        }
      }
      pDevice->SetRenderState(D3DRS_VERTEXBLEND, dwBlendMatrixNum);
      D3DXMATRIX matWorlds[4];

      memset(&mStack, 0.0f, sizeof(D3DXMATRIX));
      for(k = 0; k < pMeshContainer->dwWeight; k++)
      {
        iMatrixIndex = pBoneCombination[i].BoneId[k];
        if(iMatrixIndex != UINT_MAX)
        {
          mStack = pMeshContainer->pBoneOffsetMatrices[iMatrixIndex] * (*pMeshContainer->ppBoneMatrix[iMatrixIndex]);
          g_pAnimXshaderEffect->SetMatrix("g_mWorld", &mStack);
          matWorlds[k] = mStack;
          //pDevice->SetTransform(D3DTS_WORLDMATRIX(k), &mStack);
        }
      }

      g_pAnimXshaderEffect->SetMatrixArray("g_matWorlds", matWorlds, 4);

      g_pAnimXshaderEffect->SetTexture("g_MeshTexture", pMeshContainer->ppTextures[pBoneCombination[i].AttribId]);
      dwPrevBoneID = pBoneCombination[i].AttribId;

      UINT nPasses;
      UINT iPass;
      //描画
      g_pAnimXshaderEffect->Begin(&nPasses, 0);
      for(iPass = 0; iPass < 1; iPass++)
      {
        //アクティブなテクニックの中でパスを開始
        g_pAnimXshaderEffect->BeginPass(iPass);

        //メッシュをレンダリングする
        pMeshContainer->MeshData.pMesh->DrawSubset(i);

        //アクティブなテクニックの中でパスを終了
        g_pAnimXshaderEffect->EndPass();
      }
      g_pAnimXshaderEffect->End();
    }
  }
  //通常メッシュの場合
  else
  {
    pDevice->SetRenderState(D3DRS_VERTEXBLEND, 0);
    g_pXshaderEffect->SetMatrix("g_mWorld", &pFrame->CombinedTransformationMatrix);
    for(i = 0; i < pMeshContainer->NumMaterials; i++)
    {
      pDevice->SetMaterial(&pMeshContainer->pMaterials[i].MatD3D);
      g_pXshaderEffect->SetTexture("g_MeshTexture", pMeshContainer->ppTextures[i]);

      UINT nPasses;
      UINT iPass;
      //描画
      g_pXshaderEffect->Begin(&nPasses, 0);
      for(iPass = 0; iPass < 1; iPass++)
      {
        //アクティブなテクニックの中でパスを開始
        g_pXshaderEffect->BeginPass(iPass);

        //メッシュをレンダリングする
        pMeshContainer->MeshData.pMesh->DrawSubset(i);

        //アクティブなテクニックの中でパスを終了
        g_pXshaderEffect->EndPass();
      }
      g_pXshaderEffect->End();
    }
  }
}

//フレームをレンダリングする。
void DrawFrame(LPDIRECT3DDEVICE9 pDevice, LPD3DXFRAME pFrameBase, D3DXMATRIX worldMat)
{
  MYFRAME* pFrame = (MYFRAME*)pFrameBase;
  MYMESHCONTAINER* pMeshContainer = (MYMESHCONTAINER*)pFrame->pMeshContainer;

  while(pMeshContainer != NULL)
  {
    RenderMeshContainer(pDevice, pMeshContainer, pFrame, worldMat);

    pMeshContainer = (MYMESHCONTAINER*)pMeshContainer->pNextMeshContainer;
  }
  if(pFrame->pFrameSibling != NULL)
  {
    DrawFrame(pDevice, pFrame->pFrameSibling, worldMat);
  }
  if(pFrame->pFrameFirstChild != NULL)
  {
    DrawFrame(pDevice, pFrame->pFrameFirstChild, worldMat);
  }
}

//フレーム行列の更新
void UpdateFrameMatrices(LPD3DXFRAME pFrameBase, LPD3DXMATRIX pParentMatrix)
{
  MYFRAME *pFrame = (MYFRAME*)pFrameBase;

  if(pParentMatrix != NULL)
  {
    D3DXMatrixMultiply(&pFrame->CombinedTransformationMatrix, &pFrame->TransformationMatrix, pParentMatrix);
  }
  else
  {
    pFrame->CombinedTransformationMatrix = pFrame->TransformationMatrix;
  }
  if(pFrame->pFrameSibling != NULL)
  {
    UpdateFrameMatrices(pFrame->pFrameSibling, pParentMatrix);
  }
  if(pFrame->pFrameFirstChild != NULL)
  {
    UpdateFrameMatrices(pFrame->pFrameFirstChild, &pFrame->CombinedTransformationMatrix);
  }
}

//描画の開始と画面のリセット
bool BeginRender()
{
  //画面を指定色でクリア
  g_pd3dDevice->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(204, 209, 218), 1.0f, 0);

  if(!SUCCEEDED(g_pd3dDevice->BeginScene()))
  {
    g_canBeginRender = false;
    return false;//描画開始失敗
  }
  g_canBeginRender = true;
  return true;
}

//描画の終了
void EndRender()
{
  //終了前に残っているスタックを描画して破棄する
  FBXMESHAMG::RenderStack(true,g_pd3dDevice, g_pSpeedershaderEffect, g_pVertexDeclaration3);
  FBXMESHAMG::RenderStack(false,g_pd3dDevice, g_pSpeedershaderEffect, g_pVertexDeclaration3);

  if(g_canBeginRender)
  {
    g_pd3dDevice->EndScene();
  }
  g_pd3dDevice->Present(NULL, NULL, NULL, NULL);
}

struct LIGHT
{
  D3DXVECTOR4 pos;            //光源座標
  D3DXVECTOR4 color;          //光の色
  float       range;          //光の届く距離
  float       distanceCamera; //カメラからの距離
};

static const int   g_lightStackMax = 5;                    //スタックの最大数
static D3DXVECTOR4 g_lightsPos[g_lightStackMax];            //光源座標
static D3DXVECTOR4 g_lightsColor[g_lightStackMax];          //光の色
static float       g_lightsRange[g_lightStackMax];          //光の届く距離
static float       g_lightsDistanceCamera[g_lightStackMax]; //カメラからの距離
static int         g_stackLightNum = 0;                     //スタックされている数

//光源をセットする colorはrgbaの順
void PushLightSource(D3DXVECTOR3 lightPos, D3DXVECTOR4 lightColor, float lightRange, float distanceCamera)
{
  //スタック枠が空いていればそのまま代入
  if(g_stackLightNum < g_lightStackMax)
  {
    g_lightsPos[g_stackLightNum]            = D3DXVECTOR4(lightPos.x, lightPos.y, lightPos.z, 1);
    g_lightsColor[g_stackLightNum]          = lightColor;
    g_lightsRange[g_stackLightNum]          = lightRange;
    g_lightsDistanceCamera[g_stackLightNum] = distanceCamera;
    g_stackLightNum++;
  }
  //枠がなければカメラとの距離が近い方を入れる
  else
  {
    for(int i = 0; i < g_lightStackMax; i++)
    {
      if(distanceCamera < g_lightsDistanceCamera[i])
      {
        g_lightsPos[i] = D3DXVECTOR4(lightPos.x, lightPos.y, lightPos.z, 1);
        g_lightsColor[i] = lightColor;
        g_lightsRange[i] = lightRange;
        g_lightsDistanceCamera[i] = distanceCamera;
      }
    }
  }//else
}

//光源設定
void SetupLights()
{
  ////アンビエント光の設定
  //D3DMATERIAL9 mtrl;
  //ZeroMemory(&mtrl, sizeof(D3DMATERIAL9));
  //mtrl.Diffuse.r = mtrl.Ambient.r = 1.0f;
  //mtrl.Diffuse.g = mtrl.Ambient.g = 1.0f;
  //mtrl.Diffuse.b = mtrl.Ambient.b = 1.0f;
  //mtrl.Diffuse.a = mtrl.Ambient.a = 1.0f;
  //g_pd3dDevice->SetMaterial(&mtrl);

  //光源の設定
  D3DXVECTOR3 lightSorce = D3DXVECTOR3(0.0f, 1000.0f, 0.0f);//光源の座標
  D3DXVECTOR4 ambientColor = D3DXVECTOR4(0.3f, 0.3f, 0.3f, 1.0f);//アンビエント光の色　左からrgba

  //PushLightSource(lightSorce, D3DXVECTOR4(0.3f, 0.3f, 0.3f, 1.0f), 50000, 0);
  PushLightSource(lightSorce, D3DXVECTOR4(0.0f, 0.0f, 0.0f, 1.0f), 50000, 0);

  g_pXshaderEffect->SetValue("g_ambientLight", &ambientColor, sizeof(D3DXVECTOR4));
  g_pXshaderEffect->SetValue("g_lightSource", &lightSorce, sizeof(D3DXVECTOR3));
  g_pXshaderEffect->SetValue("g_fogStartRange", &g_fogStartDistance, sizeof(float));
  g_pXshaderEffect->SetValue("g_fogEndRange", &g_fogEndDistance, sizeof(float));
  g_pSpeedershaderEffect->SetValue("g_ambientLight", &ambientColor, sizeof(D3DXVECTOR4));
  g_pSpeedershaderEffect->SetVectorArray("g_lightSource", g_lightsPos, g_stackLightNum);
  g_pSpeedershaderEffect->SetVectorArray("g_lightColor", g_lightsColor, g_stackLightNum);
  g_pSpeedershaderEffect->SetFloatArray("g_lightRange", g_lightsRange, g_stackLightNum);
  g_pSpeedershaderEffect->SetValue("g_fogStartRange", &g_fogStartDistance, sizeof(float));
  g_pSpeedershaderEffect->SetValue("g_fogEndRange", &g_fogEndDistance, sizeof(float));
  g_p2dShaderEffect->SetValue("g_ambientLight", &ambientColor, sizeof(D3DXVECTOR4));
  g_pAnimXshaderEffect->SetValue("g_ambientLight", &ambientColor, sizeof(D3DXVECTOR4));
  g_pAnimXshaderEffect->SetValue("g_lightSource", &lightSorce, sizeof(D3DXVECTOR3));

  //光源情報を破棄
  g_stackLightNum = 0;
  for(int i = 0; i < g_lightStackMax; i++)
  {
    g_lightsPos[i] = D3DXVECTOR4(0.0f, 0.0f, 0.0f, 0.0f);
    g_lightsColor[i] = D3DXVECTOR4(0.0f, 0.0f, 0.0f, 0.0f);
    g_lightsRange[i] = 0.0f;
    g_lightsDistanceCamera[i] = 0.0f;
  }
}

static D3DXVECTOR3 g_LookAt;

//プロジェクション行列の設定
void SetupMatrices(D3DXVECTOR3 eye,
                   D3DXVECTOR3 lookAt,
                   D3DXVECTOR3 upVec)
{
  D3DXVECTOR3 vEyePt_ = CONVERT_VECTOR(eye);
  D3DXVECTOR3 vLookatPt_ = CONVERT_VECTOR(lookAt);
  D3DXVECTOR3 vUpVec_ = CONVERT_VECTOR(upVec);

  // ビュートランスフォーム
  D3DXMATRIXA16 matView;
  D3DXMatrixLookAtLH(&matView, &vEyePt_, &vLookatPt_, &vUpVec_);
  g_pd3dDevice->SetTransform(D3DTS_VIEW, &matView);

  //プロジェクショントランスフォーム
  D3DXMATRIXA16 matProj;
  D3DXMatrixPerspectiveFovLH(&matProj, D3DX_PI / 4, ((float)gDispWidth / (float)gDispHeight), 50.0f, 25000.0f);
  g_pd3dDevice->SetTransform(D3DTS_PROJECTION, &matProj);

  g_LookAt = vLookatPt_;
  //行列情報をHLSL側に渡す
  g_pXshaderEffect->SetMatrix("g_matView", &matView);
  g_pXshaderEffect->SetMatrix("g_matProj", &matProj);
  g_pXshaderEffect->SetValue("g_cameraPos", &vEyePt_, sizeof(float) * 3);
  g_pSpeedershaderEffect->SetMatrix("g_matView", &matView);
  g_pSpeedershaderEffect->SetMatrix("g_matProj", &matProj);
  g_pSpeedershaderEffect->SetValue("g_cameraPos", &vEyePt_, sizeof(float) * 3);
  g_p2dShaderEffect->SetMatrix("g_matView", &matView);
  g_p2dShaderEffect->SetMatrix("g_matProj", &matProj);
  g_pAnimXshaderEffect->SetMatrix("g_matView", &matView);
  g_pAnimXshaderEffect->SetMatrix("g_matProj", &matProj);
}

//3Dモデルの描画
//引数 x_,y_,z_      : ワールド座標のx_座標,y_座標,z_座標
//     scaleX,Y,Z : オブジェクトの拡縮倍率
//     yawAngle_   : y_軸を回転中心軸とした回転角度
//     meshHandle : 任意のメッシュデータのハンドル
//     animationData_ : アニメーション情報
void Render3D(float x, float y, float z, float scaleX, float scaleY, float scaleZ, float yawAngle, void* meshHandle, ANIMATION_DATA* animationData, bool isShade)
{
  //meshHandleが不正であれば何もしない
  if(meshHandle <= 0)
  {
    return;
  }
  MESHDATA* meshData = (MESHDATA*)meshHandle;

  //ワールドトランスフォームは”個々の物体ごとに”必要
  D3DXMATRIX mWorld;       //最終的なワールド行列
  D3DXMATRIX mScale;       //スケーリング行列
  D3DXMATRIX mRotation;    //回転行列
  D3DXMATRIX mTranslation; //平行移動行列

  //行列の初期化
  D3DXMatrixIdentity(&mWorld);
  D3DXMatrixIdentity(&mScale);
  D3DXMatrixIdentity(&mRotation);
  D3DXMatrixIdentity(&mTranslation);

  D3DXMatrixTranslation(&mTranslation, x, y, z);      //移動
  D3DXMatrixScaling(&mScale, scaleX, scaleY, scaleZ); //スケーリング
  D3DXMatrixRotationY(&mRotation, -yawAngle);         //回転
  mWorld = mScale * mRotation * mTranslation;         //ワールド行列＝スケーリング×回転×移動

  if(meshData->meshKind_ == ANIMATION_MESH){
    if(animationData == NULL || animationData->pAnimController_ == NULL)
    {
      return;
    }
    //レンダリング
    UpdateFrameMatrices(meshData->pFrameRoot_, &mWorld);
    DrawFrame(g_pd3dDevice, meshData->pFrameRoot_, mWorld);
  }

  if(meshData->meshKind_ == MESH){
    UINT nPasses;
    UINT iPass;

    //ワールド行列をHLSLに送る
    g_pXshaderEffect->SetMatrix("g_mWorld", &mWorld);
    g_pXshaderEffect->SetMatrix("g_mRotation", &mRotation);

    //d3d_device->SetVertexDeclaration(VertexDeclaration);

    //マテリアルの種類の数だけ繰り返す
    for(DWORD j = 0; j < meshData->dwNumMaterials_; j++)
    {
      HRESULT hr;


      //TODO:とりあえず1回だけ読み込み
      static LPDIRECT3DTEXTURE9 normalMap = NULL;
      int static flag = 0;
      if(flag == 0)
      {
        (D3DXCreateTextureFromFile(g_pd3dDevice,
         "res/texture/3D/catNomal_normal_map.png",
         &normalMap));
        flag++;
      }

      //アクティブなテクニックを設定する。
      hr = g_pXshaderEffect->SetTechnique("RenderSceneWithTexture");
      hr = g_pXshaderEffect->SetTexture("g_MeshTexture", meshData->pMeshTextures_[j]);

      //TODO:とりま法線マップを送る
      if(normalMap)
      hr = g_pXshaderEffect->SetTexture("g_MeshNormalMap", normalMap);

      //描画
      hr = g_pXshaderEffect->Begin( &nPasses, 0 );
      for( iPass = 0; iPass < 1; iPass ++ )
      {
        //アクティブなテクニックの中でパスを開始
        hr = g_pXshaderEffect->BeginPass(iPass);

        //メッシュをレンダリングする
        hr = meshData->pMesh_->DrawSubset(j);

        //アクティブなテクニックの中でパスを終了
        hr = g_pXshaderEffect->EndPass();
      }
      hr = g_pXshaderEffect->End();
    }
  }

  //fbxデータのレンダリング
  if(meshData->meshKind_ == FBX_MESH)
  {
    g_pXshaderEffect->SetMatrix("g_mWorld", &mWorld);
    g_pXshaderEffect->SetMatrix("g_mRotation", &mRotation);
    meshData->fbxMeshData_->Render(animationData, g_pd3dDevice, g_pSpeedershaderEffect, g_pVertexDeclaration3, mWorld, mRotation, isShade);
  }
}

// 描画文字列のフォントサイズの設定
void SetFontSize(int size)
{
  g_fontSize = size;
}

// 文字列の描画
// x_        : 描画する左上座標X
// y_        : 描画する左上座標Y
// szString : 描画する文字列
// color_    : 文字色(16進2桁ずつでr,g,b,aの順)
void DrawString(int x, int y, char *szString, int color)
{
  int  stringLength;  // 文字列の長さ
  RECT drawRect;

  stringLength = (int)strlen(szString);
  SetRect(&drawRect, x, y, x + stringLength * g_fontSize, y + g_fontSize);
  g_pFont->DrawTextA(NULL, (LPCSTR)szString, stringLength, &drawRect, DT_LEFT | DT_TOP, (D3DCOLOR)color);
}

// printf()的に DrawString()を使う関数
void DrawPrintf(int x, int y, int nColor, const char *fmt, ...)
{
  char sz[0xffff];
  va_list list;
  va_start(list, fmt);
  vsprintf(sz, fmt, list);
  va_end(list);

  DrawString(x, y, sz, nColor);
}

static LPDIRECT3DTEXTURE9 g_boxTexture = NULL;
int static g_textureFlag = 0;

//ボックスを描画する(ほぼデバッグ用)
//pos_を中心にxyzSizeのボックスを生成する
void RenderBox(D3DXVECTOR3 pos, float xSize, float ySize, float zSize, float yawAngle, int nColor)
{
  //ワールドトランスフォームは”個々の物体ごとに”必要
  D3DXMATRIX mWorld;       //最終的なワールド行列
  D3DXMATRIX mScale;       //スケーリング行列
  D3DXMATRIX mRotation;    //回転行列
  D3DXMATRIX mTranslation; //平行移動行列
  //行列の初期化
  D3DXMatrixIdentity(&mWorld);
  D3DXMatrixIdentity(&mScale);
  D3DXMatrixIdentity(&mRotation);
  D3DXMatrixIdentity(&mTranslation);
  //ビュー
  D3DXMatrixTranslation(&mTranslation, pos.x, pos.y, pos.z);  //移動
  D3DXMatrixScaling(&mScale, 1.0f, 1.0f, 1.0f);               //スケーリング
  D3DXMatrixRotationY(&mRotation, -yawAngle);                 //回転
  mWorld = mScale * mRotation * mTranslation;                 //ワールド行列＝スケーリング×回転×移動
  //ワールド行列をHLSLに送る

  g_pXshaderEffect->SetTechnique("RenderSceneWithMaterial");
  g_pXshaderEffect->SetMatrix("g_mWorld", &mWorld);
  g_pXshaderEffect->SetMatrix("g_mRotation", &mRotation);

  float materialColor[4];
  materialColor[0] = ((nColor >> 16) & 0x000000ff) / 255.0f;
  materialColor[1] = ((nColor >> 8)  & 0x000000ff) / 255.0f;
  materialColor[2] = ((nColor)       & 0x000000ff) / 255.0f;
  materialColor[3] = ((nColor >> 24) & 0x000000ff) / 255.0f;
  g_pXshaderEffect->SetValue("g_materialColor", &materialColor, sizeof(float) * 4);

  //TODO:とりあえず1回だけ読み込み
  if(g_textureFlag == 0)
  {
    (D3DXCreateTextureFromFile(g_pd3dDevice,
     "res/texture/3D/test.png",
     &g_boxTexture));
    g_textureFlag++;
  }
  g_pXshaderEffect->SetTexture("g_MeshTexture", g_boxTexture);

  LPD3DXMESH pMesh = NULL;
  D3DXCreateBox(g_pd3dDevice, xSize, ySize, zSize, &pMesh, NULL);
  if(pMesh)
  {
    //描画
    g_pXshaderEffect->Begin(NULL, 0);
    //アクティブなテクニックの中でパスを開始
    g_pXshaderEffect->BeginPass(0);
    //メッシュをレンダリングする
    pMesh->DrawSubset(0);
    //アクティブなテクニックの中でパスを終了
    g_pXshaderEffect->EndPass();
    g_pXshaderEffect->End();
    //メッシュデータの解放
    pMesh->Release();
  }
}

//球体を描画する(ほぼデバッグ用)
void RenderSphere(D3DXVECTOR3 pos, float r, int nColor)
{
  //ワールドトランスフォームは”個々の物体ごとに”必要
  D3DXMATRIX mWorld;       //最終的なワールド行列
  D3DXMATRIX mScale;       //スケーリング行列
  D3DXMATRIX mRotation;    //回転行列
  D3DXMATRIX mTranslation; //平行移動行列
                           //行列の初期化
  D3DXMatrixIdentity(&mWorld);
  D3DXMatrixIdentity(&mScale);
  D3DXMatrixIdentity(&mRotation);
  D3DXMatrixIdentity(&mTranslation);
  //ビュー
  D3DXMatrixTranslation(&mTranslation, pos.x, pos.y, pos.z);  //移動
  D3DXMatrixScaling(&mScale, 1.0f, 1.0f, 1.0f);               //スケーリング
  D3DXMatrixRotationY(&mRotation, 0.0f);                      //回転
  mWorld = mScale * mRotation * mTranslation;                 //ワールド行列＝スケーリング×回転×移動
                                                              //ワールド行列をHLSLに送る
  g_pXshaderEffect->SetTechnique("RenderSceneWithMaterial");
  g_pXshaderEffect->SetMatrix("g_mWorld", &mWorld);
  g_pXshaderEffect->SetMatrix("g_mRotate", &mWorld);

  float materialColor[4];
  materialColor[0] = ((nColor >> 16) & 0x000000ff) / 255.0f;
  materialColor[1] = ((nColor >> 8)  & 0x000000ff) / 255.0f;
  materialColor[2] = ((nColor)       & 0x000000ff) / 255.0f;
  materialColor[3] = ((nColor >> 24) & 0x000000ff) / 255.0f;
  g_pXshaderEffect->SetValue("g_materialColor", &materialColor, sizeof(float) * 4);

  //TODO:とりあえず1回だけ読み込み
  if(g_textureFlag == 0)
  {
    (D3DXCreateTextureFromFile(g_pd3dDevice,
     "res/texture/3D/default_texture.png",
     &g_boxTexture));
    g_textureFlag++;
  }
  g_pXshaderEffect->SetTexture("g_MeshTexture", g_boxTexture);
  g_pd3dDevice->SetVertexDeclaration(g_pVertexDeclaration);

  LPD3DXMESH pMesh = NULL;
  D3DXCreateSphere(g_pd3dDevice, r, 16, 16, &pMesh, NULL);
  if(pMesh)
  {
    //描画
    g_pXshaderEffect->Begin(NULL, 0);
    //アクティブなテクニックの中でパスを開始
    g_pXshaderEffect->BeginPass(0);
    //メッシュをレンダリングする
    pMesh->DrawSubset(0);
    //アクティブなテクニックの中でパスを終了
    g_pXshaderEffect->EndPass();
    g_pXshaderEffect->End();
    //メッシュデータの解放
    pMesh->Release();
  }
}

//円柱を描画する(ほぼデバッグ用)
void RenderCylinder(D3DXVECTOR3 pos, float r, float length, int nColor)
{
  //ワールドトランスフォームは”個々の物体ごとに”必要
  D3DXMATRIX mWorld;       //最終的なワールド行列
  D3DXMATRIX mScale;       //スケーリング行列
  D3DXMATRIX mRotation;    //回転行列
  D3DXMATRIX mTranslation; //平行移動行列
                           //行列の初期化
  D3DXMatrixIdentity(&mWorld);
  D3DXMatrixIdentity(&mScale);
  D3DXMatrixIdentity(&mRotation);
  D3DXMatrixIdentity(&mTranslation);
  //ビュー
  D3DXMatrixTranslation(&mTranslation, pos.x, pos.y, pos.z);  //移動
  D3DXMatrixScaling(&mScale, 1.0f, 1.0f, 1.0f);               //スケーリング
  D3DXMatrixRotationX(&mRotation, PI / 2.0f);                 //回転
  mWorld = mScale * mRotation * mTranslation;                 //ワールド行列＝スケーリング×回転×移動
                                                              //ワールド行列をHLSLに送る
  g_pXshaderEffect->SetTechnique("RenderSceneWithMaterial");
  g_pXshaderEffect->SetMatrix("g_mWorld", &mWorld);
  g_pXshaderEffect->SetMatrix("g_mRotate", &mWorld);

  float materialColor[4];
  materialColor[0] = ((nColor >> 16) & 0x000000ff) / 255.0f;
  materialColor[1] = ((nColor >> 8)  & 0x000000ff) / 255.0f;
  materialColor[2] = ((nColor)       & 0x000000ff) / 255.0f;
  materialColor[3] = ((nColor >> 24) & 0x000000ff) / 255.0f;
  g_pXshaderEffect->SetValue("g_materialColor", &materialColor, sizeof(float) * 4);
  //TODO:とりあえず1回だけ読み込み
  if(g_textureFlag == 0)
  {
    (D3DXCreateTextureFromFile(g_pd3dDevice,
     "res/texture/3D/default_texture.png",
     &g_boxTexture));
    g_textureFlag++;
  }
  g_pXshaderEffect->SetTexture("g_MeshTexture", g_boxTexture);

  LPD3DXMESH pMesh = NULL;
  D3DXCreateCylinder(g_pd3dDevice, r, r, length, 16, 16, &pMesh, NULL);
  if(pMesh)
  {
    //描画
    g_pXshaderEffect->Begin(NULL, 0);
    //アクティブなテクニックの中でパスを開始
    g_pXshaderEffect->BeginPass(0);
    //メッシュをレンダリングする
    pMesh->DrawSubset(0);
    //アクティブなテクニックの中でパスを終了
    g_pXshaderEffect->EndPass();
    g_pXshaderEffect->End();
    //メッシュデータの解放
    pMesh->Release();
  }
}

//ベクトルのxyz成分の抜出
D3DXVECTOR3 GetVectorComponent(float r, float theta_, float phi_)
{
  D3DXVECTOR3 result;

  result.x = r * sinf(theta_) * cosf(phi_);
  result.y = r * cosf(theta_);
  result.z = r * sinf(theta_) * sinf(phi_);

  return result;
}

//ベクトルのxyz成分の抜出
D3DXVECTOR4 GetVectorComponentV4(float r, float theta_, float phi_)
{
  D3DXVECTOR4 result;

  result.x = r * sinf(theta_) * cosf(phi_);
  result.y = r * cosf(theta_);
  result.z = r * sinf(theta_) * sinf(phi_);
  result.w = 1.0f;

  return result;
}

//扇形柱を描画する(ほぼデバッグ用)
void RenderFanPillar(D3DXVECTOR3 pos, float r, float length, float fanAngle, D3DXVECTOR3 dir, int nColor)
{
  //ワールドトランスフォームは”個々の物体ごとに”必要
  D3DXMATRIX mWorld;       //最終的なワールド行列
  D3DXMATRIX mScale;       //スケーリング行列
  D3DXMATRIX mRotation;    //回転行列
  D3DXMATRIX mTranslation; //平行移動行列
                           //行列の初期化
  D3DXMatrixIdentity(&mWorld);
  D3DXMatrixIdentity(&mScale);
  D3DXMatrixIdentity(&mRotation);
  D3DXMatrixIdentity(&mTranslation);
  //ビュー
  D3DXMatrixTranslation(&mTranslation, pos.x, pos.y, pos.z);  //移動
  D3DXMatrixScaling(&mScale, 1.0f, 1.0f, 1.0f);               //スケーリング
  D3DXMatrixRotationY(&mRotation, -atan2f(dir.z, dir.x) + PI / 2.0f);     //回転
  //D3DXMatrixRotationY(&mRotation, 0);     //回転
  mWorld = mScale * mRotation * mTranslation;                 //ワールド行列＝スケーリング×回転×移動
                                                              //ワールド行列をHLSLに送る
  g_pXshaderEffect->SetTechnique("RenderSceneWithMaterial");
  g_pXshaderEffect->SetMatrix("g_mWorld", &mWorld);
  g_pXshaderEffect->SetMatrix("g_mRotate", &mWorld);

  float materialColor[4];
  materialColor[0] = ((nColor >> 16) & 0x000000ff) / 255.0f;
  materialColor[1] = ((nColor >> 8)  & 0x000000ff) / 255.0f;
  materialColor[2] = ((nColor)       & 0x000000ff) / 255.0f;
  materialColor[3] = ((nColor >> 24) & 0x000000ff) / 255.0f;
  g_pXshaderEffect->SetValue("g_materialColor", &materialColor, sizeof(float) * 4);

  struct VERTEX
  {
    D3DXVECTOR4 coord;
    D3DXVECTOR4 weight;
    D3DXVECTOR2 uv;
    D3DXVECTOR3 normal;
    D3DXVECTOR3 tan;
    D3DXVECTOR3 bin;
  };
  const int splitNum = 16;

  VERTEX vertex[splitNum * 12 + 12] = {};

  fanAngle /= 2.0f;
  vertex[0].coord = D3DXVECTOR4(0.0f, length / 2.0, 0.0f,1.0f);
  vertex[1].coord = D3DXVECTOR4(r * sinf(fanAngle), length / 2.0, r * cosf(fanAngle),1.0f);
  vertex[2].coord = D3DXVECTOR4(r * sinf(fanAngle), -length / 2.0, r * cosf(fanAngle),1.0f);

  vertex[3].coord = vertex[0].coord;
  vertex[4].coord = vertex[2].coord;
  vertex[5].coord = D3DXVECTOR4(0.0f, -length / 2.0, 0.0f,0.0f);
  vertex[6].coord = vertex[0].coord;
  vertex[7].coord = vertex[5].coord;
  vertex[8].coord = D3DXVECTOR4(r * sinf(-fanAngle), -length / 2.0, r * cosf(-fanAngle), 1.0f);
  vertex[9].coord = vertex[0].coord;
  vertex[10].coord = vertex[8].coord;
  vertex[11].coord = D3DXVECTOR4(r * sinf(-fanAngle), length / 2.0, r * cosf(-fanAngle), 1.0f);

  for(int i = 0; i < splitNum; i++)
  {
    //弧の側面の三角ポリゴン
    vertex[i * 12 + 12].coord = GetVectorComponentV4(r, PI / 2.0f, fanAngle * 2 / splitNum * i + PI / 2.0f - fanAngle);
    vertex[i * 12 + 12].coord.y = length / 2.0;
    vertex[i * 12 + 13].coord = GetVectorComponentV4(r, PI / 2.0f, fanAngle * 2 / splitNum * (i + 1) + PI / 2.0f - fanAngle);
    vertex[i * 12 + 13].coord.y = length / 2.0;
    vertex[i * 12 + 14].coord = GetVectorComponentV4(r, PI / 2.0f, fanAngle * 2 / splitNum * (i + 1) + PI / 2.0f - fanAngle);
    vertex[i * 12 + 14].coord.y = -length / 2.0;
    //上面の三角ポリゴン
    vertex[i * 12 + 15].coord = D3DXVECTOR4(0.0f, length / 2.0, 0.0f, 1.0f);
    vertex[i * 12 + 16].coord = vertex[i * 12 + 12].coord;
    vertex[i * 12 + 17].coord = vertex[i * 12 + 13].coord;
    //弧の側面の三角ポリゴン
    vertex[i * 12 + 18].coord = GetVectorComponentV4(r, PI / 2.0f, fanAngle * 2 / splitNum * i + PI / 2.0f - fanAngle);
    vertex[i * 12 + 18].coord.y = length / 2.0;
    vertex[i * 12 + 19].coord = GetVectorComponentV4(r, PI / 2.0f, fanAngle * 2 / splitNum * (i + 1) + PI / 2.0f - fanAngle);
    vertex[i * 12 + 19].coord.y = -length / 2.0;
    vertex[i * 12 + 20].coord = GetVectorComponentV4(r, PI / 2.0f, fanAngle * 2 / splitNum * i + PI / 2.0f - fanAngle);
    vertex[i * 12 + 20].coord.y = -length / 2.0;
    //底面の三角ポリゴン
    vertex[i * 12 + 21].coord = D3DXVECTOR4(0.0f, -length / 2.0, 0.0f, 1.0f);
    vertex[i * 12 + 22].coord = vertex[i * 12 + 19].coord;
    vertex[i * 12 + 23].coord = vertex[i * 12 + 20].coord;
  }

  for(int i = 0; i < splitNum * 12 + 12; i++)
  {
    vertex[i].weight = D3DXVECTOR4(0.0f, 0.0f, 0.0f, 0.0f);
    vertex[i].uv.x = 0.0f;
    vertex[i].uv.y = 0.0f;
    vertex[i].normal = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
    vertex[i].tan =    D3DXVECTOR3(0.0f, 1.0f, 0.0f);
    vertex[i].bin =    D3DXVECTOR3(0.0f, 1.0f, 0.0f);
  }

  //1回だけ読み込み
  if(g_textureFlag == 0)
  {
    (D3DXCreateTextureFromFile(g_pd3dDevice,
     "res/texture/3D/default_texture.png",
     &g_boxTexture));
    g_textureFlag++;
  }
  g_pXshaderEffect->SetTexture("g_MeshTexture", g_boxTexture);

  g_pd3dDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);

  //描画
  g_pXshaderEffect->Begin(NULL, 0);
  //アクティブなテクニックの中でパスを開始
  g_pXshaderEffect->BeginPass(0);
  //メッシュをレンダリングする
  g_pd3dDevice->DrawPrimitiveUP(D3DPT_TRIANGLELIST, splitNum * 4 + 4, vertex, sizeof(VERTEX));
  //アクティブなテクニックの中でパスを終了
  g_pXshaderEffect->EndPass();
  g_pXshaderEffect->End();
}

//レイの描画
void RenderRay(D3DXVECTOR3 startPos,D3DXVECTOR3 endPos,int color)
{
  //レイの始終点
  D3DXVECTOR3 vPnt[2];
  vPnt[0] = startPos;
  vPnt[1] = endPos;

  //色変換
  D3DMATERIAL9 mtrl;
  ZeroMemory(&mtrl, sizeof(mtrl));
  mtrl.Diffuse.a = (color >> 24) & 0xff;
  mtrl.Diffuse.r = (color >> 16) & 0xff;
  mtrl.Diffuse.g = (color >> 8 ) & 0xff;
  mtrl.Diffuse.b = color & 0xff;
  mtrl.Ambient = mtrl.Diffuse;
  g_pd3dDevice->SetMaterial(&mtrl);

  //ワールドトランスフォームは”個々の物体ごとに”必要
  D3DXMATRIX mWorld;       //最終的なワールド行列
  D3DXMATRIX mScale;       //スケーリング行列
  D3DXMATRIX mRotation;    //回転行列
  D3DXMATRIX mTranslation; //平行移動行列
  //行列の初期化
  D3DXMatrixIdentity(&mWorld);
  D3DXMatrixIdentity(&mScale);
  D3DXMatrixIdentity(&mRotation);
  D3DXMatrixIdentity(&mTranslation);
  //ビュー
  D3DXMatrixTranslation(&mTranslation, 0.0f, 0.0f, 0.0f); //移動
  D3DXMatrixScaling(&mScale, 1.0f, 1.0f, 1.0f);           //スケーリング
  D3DXMatrixRotationY(&mRotation, -0.0f);                  //回転
  mWorld = mScale * mRotation * mTranslation;             //ワールド行列＝スケーリング×回転×移動
  g_pd3dDevice->SetTransform(D3DTS_WORLD, &mWorld);

  g_pd3dDevice->DrawPrimitiveUP(D3DPT_LINELIST, 1, vPnt, sizeof(D3DXVECTOR3));
}

// 2Dテクスチャ頂点フォーマット
#define D3DFVF_2DTEX ( D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_TEX1 )

// 2D描画板ポリゴンに使用する
struct CUSTOM_VERTEX
{
  float x_, y_, z_;    // 頂点座標
  float rhw_;        // 除算数
  D3DCOLOR color_;   // 色情報(アルファ値もあり)
  float tu_, tv_;     // テクスチャ座標
};

// 2D画像の描画(画像の大きさそのまま)
// x_             : 描画する左上座標X
// y_             : 描画する左上座標Y
// textureHandle : 描画する画像のハンドル
// alpha_         : 透明度
void RenderTexture(int x, int y, void* textureHandle, int alpha)
{
  //DrawPrintf(0, 340, 0xffffffff, "アルファ=%d", alpha_);
  if(!textureHandle) { return; }//エラー

  TEXTURE2D_DATA* textureData = (TEXTURE2D_DATA*)textureHandle;

  D3DCOLOR color;  // 頂点カラー
  color = D3DCOLOR_RGBA(255, 255, 255, alpha);

  // トライアングルファンによる設定
  CUSTOM_VERTEX TriangleFan[] =									// ここにいれると拡大縮小
  {
    // 左上頂点
    {x,                       y,                        0.0f, 1.0f, color, 0.0f, 0.0f},
    // 右上頂点
    {x + textureData->nWidth_, y,                        0.0f, 1.0f, color, 1.0f, 0.0f},
    // 右下頂点
    {x + textureData->nWidth_, y + textureData->nHeight_, 0.0f, 1.0f, color, 1.0f, 1.0f},
    // 左下頂点
    {x,                       y + textureData->nHeight_, 0.0f, 1.0f, color, 0.0f, 1.0f},
  };

  // アルファブレンド設定を行う
  //setAlphaMode(DRAW_ALPHA_BLEND);

  // 描画設定
  g_pd3dDevice->SetTexture(0, textureData->pictureInfo_); // 使用するテクスチャの指定(毎回する必要がある)
  g_pd3dDevice->SetFVF(D3DFVF_2DTEX);                     // CustomVertexの指定
  // 描画を行う
  g_pd3dDevice->DrawPrimitiveUP(D3DPT_TRIANGLEFAN,        // プリミティブの種類(トライアングルファンにしている)
                                2,                        // プリミティブの数(描画するポリゴン数)
                                TriangleFan,              // 描画するカスタムバーテックスのポインタ
                                sizeof(CUSTOM_VERTEX));   // カスタムバーテックスのサイズ
}

// 2D画像の描画(画像の大きさそのまま)
// x_             : 描画する左上座標X
// y_             : 描画する左上座標Y
// textureHandle : 描画する画像のハンドル
// alpha_         : 透明度
// shavePixX     : 削るピクセル数
// shavePixY     : 削るピクセル数
void RenderTextureShave(int x, int y, void* textureHandle, int alpha, float shaveRateX, float shaveRateY)
{
  if (!textureHandle) { return; }//エラー

  TEXTURE2D_DATA* textureData = (TEXTURE2D_DATA*)textureHandle;

  D3DCOLOR color;  // 頂点カラー
  color = D3DCOLOR_RGBA(255, 255, 255, alpha);

  // トライアングルファンによる設定
  CUSTOM_VERTEX TriangleFan[] =									// ここにいれると拡大縮小
  {
  // 左上頂点
  { x,
    y,
    0.0f, 1.0f, color,
    0.0f,
    0.0f },
  // 右上頂点
  { x + textureData->nWidth_ - shaveRateX,
    y,
    0.0f, 1.0f, color,
    1.0f - (float)(shaveRateX / textureData->nWidth_),
    0.0f },
  // 右下頂点
  { x + textureData->nWidth_ - shaveRateX,
    y + textureData->nHeight_ - shaveRateY,
    0.0f, 1.0f, color,
    1.0f - (float)(shaveRateX / textureData->nWidth_),
    1.0f - (float)(shaveRateY / textureData->nHeight_)},
  // 左下頂点
  { x,
    y + textureData->nHeight_ - shaveRateY,
    0.0f, 1.0f, color,
    0.0f,
    1.0f - (float)(shaveRateY / textureData->nHeight_)},
  };

  // 描画設定
  g_pd3dDevice->SetTexture(0, textureData->pictureInfo_); // 使用するテクスチャの指定(毎回する必要がある)
  g_pd3dDevice->SetFVF(D3DFVF_2DTEX);                     // CustomVertexの指定
  // 描画を行う
  g_pd3dDevice->DrawPrimitiveUP(D3DPT_TRIANGLEFAN,        // プリミティブの種類(トライアングルファンにしている)
                                2,                        // プリミティブの数(描画するポリゴン数)
                                TriangleFan,              // 描画するカスタムバーテックスのポインタ
                                sizeof(CUSTOM_VERTEX));   // カスタムバーテックスのサイズ
}

// 2D連番画像の描画(画像の大きさそのまま)
// x_             : 描画する左上座標X
// y_             : 描画する左上座標Y
// textureHandle : 描画する画像のハンドル
// alpha_         : 透明度
// num           : 連番画像の描画番号(左上からZ字の順 0~(n-1))
void RenderBlockTexture(int x, int y, void* textureHandle, int alpha, int num)
{
  if(!textureHandle) { return; }//エラー

  TEXTURE2D_DATA* textureData = (TEXTURE2D_DATA*)textureHandle;

  D3DCOLOR color;  // 頂点カラー
  color = D3DCOLOR_RGBA(255, 255, 255, alpha);

  int numX = num % textureData->numX_;
  int numY = num / textureData->numX_;

  // トライアングルファンによる設定
  CUSTOM_VERTEX TriangleFan[] =
  {
    // 左上頂点
    {x,                        y,                         0.0f, 1.0f, color, 1.0f / textureData->numX_ * numX,       1.0f / textureData->numY_ * numY},
    // 右上頂点
    {x + textureData->nWidth_, y,                         0.0f, 1.0f, color, 1.0f / textureData->numX_ * (numX + 1), 1.0f / textureData->numY_ * numY},
    // 右下頂点
    {x + textureData->nWidth_, y + textureData->nHeight_, 0.0f, 1.0f, color, 1.0f / textureData->numX_ * (numX + 1), 1.0f / textureData->numY_ * (numY + 1)},
    // 左下頂点
    {x,                        y + textureData->nHeight_, 0.0f, 1.0f, color, 1.0f / textureData->numX_ * numX,       1.0f / textureData->numY_ * (numY + 1)},
  };

  // 描画設定
  g_pd3dDevice->SetTexture(0, textureData->pictureInfo_); // 使用するテクスチャの指定(毎回する必要がある)
  g_pd3dDevice->SetFVF(D3DFVF_2DTEX);                     // CustomVertexの指定
  // 描画を行う
  g_pd3dDevice->DrawPrimitiveUP(D3DPT_TRIANGLEFAN,        // プリミティブの種類(トライアングルファンにしている)
                                2,                        // プリミティブの数(描画するポリゴン数)
                                TriangleFan,              // 描画するカスタムバーテックスのポインタ
                                sizeof(CUSTOM_VERTEX));   // カスタムバーテックスのサイズ
}

// 2Dのボックスの描画
// x_             : 描画する左上座標X
// y_             : 描画する左上座標Y
// sizeX         : 描画する左上座標X
// sizeY         : 描画する左上座標Y
// color         : 文字色(16進2桁ずつでr,g,b,aの順)
void Render2DBox(int x, int y, int sizeX, int sizeY, int color)
{ 
  D3DCOLOR _color = (D3DCOLOR)color;  // 頂点カラー

  // トライアングルファンによる設定
  CUSTOM_VERTEX TriangleFan[] =									// ここにいれると拡大縮小
  {
  // 左上頂点
  {x,         y,         0.0f, 1.0f, _color, 0.0f, 0.0f},
  // 右上頂点             
  {x + sizeX, y,         0.0f, 1.0f, _color, 1.0f, 0.0f},
  // 右下頂点
  {x + sizeX, y + sizeY, 0.0f, 1.0f, _color, 1.0f, 1.0f},
  // 左下頂点
  {x,         y + sizeY, 0.0f, 1.0f, _color, 0.0f, 1.0f},
  };

  // アルファブレンド設定を行う
  //setAlphaMode(DRAW_ALPHA_BLEND);

  // 描画設定
  g_pd3dDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_SELECTARG2);	// ディフューズ色のみ
  g_pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG2);	// ディフューズアルファのみ

  //g_pd3dDevice->SetTexture(0, NULL); // 使用するテクスチャの指定(毎回する必要がある)
  g_pd3dDevice->SetFVF(D3DFVF_2DTEX);                     // CustomVertexの指定
                                                          // 描画を行う
  //レンダリングする
  g_pd3dDevice->DrawPrimitiveUP(D3DPT_TRIANGLEFAN,        // プリミティブの種類(トライアングルファンにしている)
                                2,                        // プリミティブの数(描画するポリゴン数)
                                TriangleFan,              // 描画するカスタムバーテックスのポインタ
                                sizeof(CUSTOM_VERTEX));   // カスタムバーテックスのサイズ
  g_pd3dDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_MODULATE);	// テクスチャ色も復帰
  g_pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);	// テクスチャアルファも復帰
}

// 2D画像のビルボード描画(画像の大きさそのまま)
// pos_           : 描画中心座標
// targetPos     : 画像が向く地点の座標
// textureHandle : テクスチャハンドル
// alpha_         : 不透明度
void RenderBillboard(D3DXVECTOR3 pos, D3DXVECTOR3 targetPos, void* textureHandle, int alpha)
{
  //ワールドトランスフォームは”個々の物体ごとに”必要
  D3DXMATRIX mWorld;       //最終的なワールド行列
  D3DXMATRIX mScale;       //スケーリング行列
  D3DXMATRIX mRotation;    //回転行列
  D3DXMATRIX mTranslation; //平行移動行列
  D3DXMATRIX mInv;         //回転逆行列
  //行列の初期化
  D3DXMatrixIdentity(&mWorld);
  D3DXMatrixIdentity(&mScale);
  D3DXMatrixIdentity(&mRotation);
  D3DXMatrixIdentity(&mTranslation);

  //移動
  D3DXMatrixTranslation(&mTranslation, pos.x, pos.y, pos.z);  

  //カメラの回転の逆行列
  D3DXMatrixIdentity(&mInv);
  D3DXMatrixLookAtLH(&mInv, &targetPos, &pos, &D3DXVECTOR3(0, 1, 0));
  D3DXMatrixInverse(&mInv, NULL, &mInv);
  mInv._41 = 0.0f;   // オフセットを切る（回転行列だけにしてしまう）
  mInv._42 = 0.0f;
  mInv._43 = 0.0f;

  mWorld = mScale * mInv * mTranslation;                 //ワールド行列＝スケーリング×回転×移動


  ////ワールド行列をHLSLに送る
  //g_pXshaderEffect->SetTechnique("RenderSceneWithMaterial");
  //g_pXshaderEffect->SetMatrix("g_mWorld", &mWorld);
  //g_pXshaderEffect->SetMatrix("g_mRotation", &mRotation);

  if(!textureHandle) { return; }//エラー

  TEXTURE2D_DATA* textureData = (TEXTURE2D_DATA*)textureHandle;

  D3DCOLOR color;  // 頂点カラー
  color = D3DCOLOR_RGBA(255, 255, 255, alpha);

  // トライアングルファンによる設定
  CUSTOM_VERTEX TriangleFan[] =
  {
    // 左上頂点
    {-textureData->nWidth_ / 2.0f,  textureData->nHeight_ / 2.0f, 0.0f, 1.0f, color, 0.0f, 0.0f},
    // 右上頂点
    { textureData->nWidth_ / 2.0f,  textureData->nHeight_ / 2.0f, 0.0f, 1.0f, color, 1.0f, 0.0f},
    // 右下頂点
    { textureData->nWidth_ / 2.0f, -textureData->nHeight_ / 2.0f, 0.0f, 1.0f, color, 1.0f, 1.0f},
    // 左下頂点
    {-textureData->nWidth_ / 2.0f, -textureData->nHeight_ / 2.0f, 0.0f, 1.0f, color, 0.0f, 1.0f},
  };

  g_pd3dDevice->SetVertexDeclaration(g_pVertexDeclaration2);

  //描画
  HRESULT hr = 0;
  //アクティブなテクニックを設定する。
  hr = g_p2dShaderEffect->SetTechnique("RenderSceneWithTexture");
  hr = g_p2dShaderEffect->SetTexture("g_MeshTexture", textureData->pictureInfo_);
  hr = g_p2dShaderEffect->SetMatrix("g_mWorld", &mWorld);
  //アクティブなテクニックの中でパスを開始
  hr = g_p2dShaderEffect->Begin(NULL, 0);
  hr = g_p2dShaderEffect->BeginPass(0);
  //レンダリングする
  g_pd3dDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);//Zバッファを無効
  g_pd3dDevice->DrawPrimitiveUP(D3DPT_TRIANGLEFAN,        // プリミティブの種類(トライアングルファンにしている)
                                2,                        // プリミティブの数(描画するポリゴン数)
                                TriangleFan,              // 描画するカスタムバーテックスのポインタ
                                sizeof(CUSTOM_VERTEX));   // カスタムバーテックスのサイズ
  g_pd3dDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);//Zバッファを有効に戻す
  //アクティブなテクニックの中でパスを終了
  hr = g_p2dShaderEffect->EndPass();
  hr = g_p2dShaderEffect->End();
}

// 2D画像のビルボード回転描画(画像の大きさそのまま)
// pos_           : 描画中心座標
// targetPos     : 画像が向く地点の座標
// rotAngle      : 回転角度
// textureHandle : テクスチャハンドル
// alpha_         : 不透明度
void RenderBillboardRot(D3DXVECTOR3 pos, D3DXVECTOR3 targetPos, float rotAngle, void* textureHandle, int alpha)
{
  //ワールドトランスフォームは”個々の物体ごとに”必要
  D3DXMATRIX mWorld;       //最終的なワールド行列
  D3DXMATRIX mScale;       //スケーリング行列
  D3DXMATRIX mRotation;    //回転行列
  D3DXMATRIX mTranslation; //平行移動行列
  D3DXMATRIX mInv;         //回転逆行列
                           //行列の初期化
  D3DXMatrixIdentity(&mWorld);
  D3DXMatrixIdentity(&mScale);
  D3DXMatrixIdentity(&mRotation);
  D3DXMatrixIdentity(&mTranslation);

  //移動
  D3DXMatrixTranslation(&mTranslation, pos.x, pos.y, pos.z);

  //カメラの回転の逆行列
  D3DXMatrixIdentity(&mInv);
  D3DXMatrixLookAtLH(&mInv, &targetPos, &pos, &D3DXVECTOR3(0, 1, 0));
  D3DXMatrixInverse(&mInv, NULL, &mInv);
  mInv._41 = 0.0f;   // オフセットを切る（回転行列だけにしてしまう）
  mInv._42 = 0.0f;
  mInv._43 = 0.0f;

  mWorld = mScale * mInv * mTranslation;                 //ワールド行列＝スケーリング×回転×移動

  if(!textureHandle) { return; }//エラー

  TEXTURE2D_DATA* textureData = (TEXTURE2D_DATA*)textureHandle;

  D3DCOLOR color;  // 頂点カラー
  color = D3DCOLOR_RGBA(255, 255, 255, alpha);

  // トライアングルファンによる設定
  CUSTOM_VERTEX TriangleFan[] =
  {
  // 左上頂点
  {-textureData->nWidth_ / 2.0f,  textureData->nHeight_ / 2.0f, 0.0f, 1.0f, color, 0.0f, 0.0f},
  // 右上頂点
  {textureData->nWidth_ / 2.0f,  textureData->nHeight_ / 2.0f, 0.0f, 1.0f, color, 1.0f, 0.0f},
  // 右下頂点
  {textureData->nWidth_ / 2.0f, -textureData->nHeight_ / 2.0f, 0.0f, 1.0f, color, 1.0f, 1.0f},
  // 左下頂点
  {-textureData->nWidth_ / 2.0f, -textureData->nHeight_ / 2.0f, 0.0f, 1.0f, color, 0.0f, 1.0f},
  };

  float sinTheta = sinf(rotAngle);
  float cosTheta = cosf(rotAngle);
  for(int i = 0; i < 4; i++)
  {
    TriangleFan[i].x_ = TriangleFan[i].x_ * cosTheta - TriangleFan[i].y_ * sinTheta; //x_' = xcosθ - ysinθ
    TriangleFan[i].y_ = TriangleFan[i].x_ * sinTheta + TriangleFan[i].y_ * cosTheta; //y_' = xsinθ + ycosθ
  }

  g_pd3dDevice->SetVertexDeclaration(g_pVertexDeclaration2);

  //描画
  HRESULT hr = 0;
  //アクティブなテクニックを設定する。
  hr = g_p2dShaderEffect->SetTechnique("RenderSceneWithTexture");
  hr = g_p2dShaderEffect->SetTexture("g_MeshTexture", textureData->pictureInfo_);
  hr = g_p2dShaderEffect->SetMatrix("g_mWorld", &mWorld);
  //アクティブなテクニックの中でパスを開始
  hr = g_p2dShaderEffect->Begin(NULL, 0);
  hr = g_p2dShaderEffect->BeginPass(0);
  //レンダリングする
  g_pd3dDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);//Zバッファを無効
  g_pd3dDevice->DrawPrimitiveUP(D3DPT_TRIANGLEFAN,        // プリミティブの種類(トライアングルファンにしている)
                                2,                        // プリミティブの数(描画するポリゴン数)
                                TriangleFan,              // 描画するカスタムバーテックスのポインタ
                                sizeof(CUSTOM_VERTEX));   // カスタムバーテックスのサイズ
  g_pd3dDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);//Zバッファを有効に戻す
  //アクティブなテクニックの中でパスを終了
  hr = g_p2dShaderEffect->EndPass();
  hr = g_p2dShaderEffect->End();
}

// 2D画像のビルボード連番回転描画(画像の大きさそのまま)
// pos_           : 描画中心座標
// targetPos     : 画像が向く地点の座標
// rotAngle      : 回転角度
// textureHandle : テクスチャハンドル
// alpha_         : 不透明度
// num           : 連番画像の描画番号(左上からZ字の順 0~(n-1))
void RenderBlockBillboardRot(D3DXVECTOR3 pos, D3DXVECTOR3 targetPos, float rotAngle, void* textureHandle, int alpha, int num)
{
  //ワールドトランスフォームは”個々の物体ごとに”必要
  D3DXMATRIX mWorld;       //最終的なワールド行列
  D3DXMATRIX mScale;       //スケーリング行列
  D3DXMATRIX mRotation;    //回転行列
  D3DXMATRIX mTranslation; //平行移動行列
  D3DXMATRIX mInv;         //回転逆行列
                           //行列の初期化
  D3DXMatrixIdentity(&mWorld);
  D3DXMatrixIdentity(&mScale);
  D3DXMatrixIdentity(&mRotation);
  D3DXMatrixIdentity(&mTranslation);

  //移動
  D3DXMatrixTranslation(&mTranslation, pos.x, pos.y, pos.z);

  //カメラの回転の逆行列
  D3DXMatrixIdentity(&mInv);
  D3DXMatrixLookAtLH(&mInv, &targetPos, &pos, &D3DXVECTOR3(0, 1, 0));
  D3DXMatrixInverse(&mInv, NULL, &mInv);
  mInv._41 = 0.0f;   // オフセットを切る（回転行列だけにしてしまう）
  mInv._42 = 0.0f;
  mInv._43 = 0.0f;

  mWorld = mScale * mInv * mTranslation;                 //ワールド行列＝スケーリング×回転×移動

  if(!textureHandle) { return; }//エラー

  TEXTURE2D_DATA* textureData = (TEXTURE2D_DATA*)textureHandle;

  D3DCOLOR color;  // 頂点カラー
  color = D3DCOLOR_RGBA(255, 255, 255, alpha);

  int numX = num % textureData->numX_;
  int numY = num / textureData->numX_;

  // トライアングルファンによる設定
  CUSTOM_VERTEX TriangleFan[] =
  {
  // 左上頂点
  {-textureData->nWidth_ / 2.0f, textureData->nHeight_ / 2.0f,  0.0f, 1.0f, color, 1.0f / textureData->numX_ * numX,       1.0f / textureData->numY_ * numY},
  // 右上頂点
  { textureData->nWidth_ / 2.0f, textureData->nHeight_ / 2.0f,  0.0f, 1.0f, color, 1.0f / textureData->numX_ * (numX + 1), 1.0f / textureData->numY_ * numY},
  // 右下頂点
  { textureData->nWidth_ / 2.0f, -textureData->nHeight_ / 2.0f, 0.0f, 1.0f, color, 1.0f / textureData->numX_ * (numX + 1), 1.0f / textureData->numY_ * (numY + 1)},
  // 左下頂点
  {-textureData->nWidth_ / 2.0f, -textureData->nHeight_ / 2.0f, 0.0f, 1.0f, color, 1.0f / textureData->numX_ * numX,       1.0f / textureData->numY_ * (numY + 1)},
  };

  float sinTheta = sinf(rotAngle);
  float cosTheta = cosf(rotAngle);
  for(int i = 0; i < 4; i++)
  {
    TriangleFan[i].x_ = TriangleFan[i].x_ * cosTheta - TriangleFan[i].y_ * sinTheta; //x_' = xcosθ - ysinθ
    TriangleFan[i].y_ = TriangleFan[i].x_ * sinTheta + TriangleFan[i].y_ * cosTheta; //y_' = xsinθ + ycosθ
  }

  g_pd3dDevice->SetVertexDeclaration(g_pVertexDeclaration2);

  //描画
  HRESULT hr = 0;
  //アクティブなテクニックを設定する。
  hr = g_p2dShaderEffect->SetTechnique("RenderSceneWithTexture");
  hr = g_p2dShaderEffect->SetTexture("g_MeshTexture", textureData->pictureInfo_);
  hr = g_p2dShaderEffect->SetMatrix("g_mWorld", &mWorld);
  //アクティブなテクニックの中でパスを開始
  hr = g_p2dShaderEffect->Begin(NULL, 0);
  hr = g_p2dShaderEffect->BeginPass(0);
  //レンダリングする
  g_pd3dDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);//Zバッファを無効
  g_pd3dDevice->DrawPrimitiveUP(D3DPT_TRIANGLEFAN,        // プリミティブの種類(トライアングルファンにしている)
                                2,                        // プリミティブの数(描画するポリゴン数)
                                TriangleFan,              // 描画するカスタムバーテックスのポインタ
                                sizeof(CUSTOM_VERTEX));   // カスタムバーテックスのサイズ
  g_pd3dDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);//Zバッファを有効に戻す
  //アクティブなテクニックの中でパスを終了
  hr = g_p2dShaderEffect->EndPass();
  hr = g_p2dShaderEffect->End();
}

// 2D画像のビルボード座標ずらし連番回転描画(画像の大きさそのまま)
// centerPos     : 描画中心座標
// shiftPos      : 描画ずらし座標(ターゲットから中心座標のベクトルをZ軸とする)
// targetPos     : 画像が向く地点の座標
// rotAngle      : 回転角度
// textureHandle : テクスチャハンドル
// alpha         : 不透明度
// num           : 連番画像の描画番号(左上からZ字の順 0~(n-1))
void RenderBlockBillboardShiftRot(D3DXVECTOR3 centerPos, D3DXVECTOR3 shiftPos, D3DXVECTOR3 targetPos, float rotAngle, void* textureHandle, int alpha, int num)
{
  //ワールドトランスフォームは”個々の物体ごとに”必要
  D3DXMATRIX mWorld;       //最終的なワールド行列
  D3DXMATRIX mScale;       //スケーリング行列
  D3DXMATRIX mRotation;    //回転行列
  D3DXMATRIX mTranslation; //平行移動行列
  D3DXMATRIX mInv;              //回転逆行列
  //行列の初期化
  D3DXMatrixIdentity(&mWorld);
  D3DXMatrixIdentity(&mScale);
  D3DXMatrixIdentity(&mRotation);
  D3DXMatrixIdentity(&mTranslation);

  //カメラから見た(xz平面における)z方向の取得
  D3DXVECTOR3 vShiftZ = D3DXVECTOR3(centerPos.x, 0, centerPos.z) - D3DXVECTOR3(targetPos.x, 0, targetPos.z);
  D3DXVec3Normalize(&vShiftZ, &vShiftZ);
  vShiftZ.x *= shiftPos.z;
  vShiftZ.z *= shiftPos.z;
  //移動
  D3DXMatrixTranslation(&mTranslation, centerPos.x + vShiftZ.x, centerPos.y, centerPos.z + vShiftZ.z);

  //カメラの回転の逆行列
  D3DXMatrixIdentity(&mInv);
  D3DXMatrixLookAtLH(&mInv, &targetPos, &centerPos, &D3DXVECTOR3(0, 1, 0));//仮回転行列の作成
  mInv._21 = 0.0f;   // y座標の回転を消す
  mInv._22 = 1.0f;
  mInv._23 = 0.0f;
  D3DXMatrixInverse(&mInv, NULL, &mInv);
  mInv._41 = 0.0f;   // オフセットを切る（回転行列だけにしてしまう）
  mInv._42 = 0.0f;
  mInv._43 = 0.0f;

  //移動
  //mWorld = mScale * mShiftTranslation * mInv * mTranslation;                 //ワールド行列＝スケーリング×回転×移動
  mWorld = mScale * mInv * mTranslation;                 //ワールド行列＝スケーリング×回転×移動

  if(!textureHandle) { return; }//エラー

  TEXTURE2D_DATA* textureData = (TEXTURE2D_DATA*)textureHandle;

  D3DCOLOR color;  // 頂点カラー
  color = D3DCOLOR_RGBA(255, 255, 255, alpha);

  int numX = num % textureData->numX_;
  int numY = num / textureData->numX_;

  // トライアングルファンによる設定
  CUSTOM_VERTEX TriangleFan[] =
  {
  // 左上頂点
  {-textureData->nWidth_ / 2.0f, textureData->nHeight_ / 2.0f,  0.0f, 1.0f, color, 1.0f / textureData->numX_ * numX,       1.0f / textureData->numY_ * numY},
  // 右上頂点
  {textureData->nWidth_ / 2.0f, textureData->nHeight_ / 2.0f,  0.0f, 1.0f, color, 1.0f / textureData->numX_ * (numX + 1), 1.0f / textureData->numY_ * numY},
  // 右下頂点
  {textureData->nWidth_ / 2.0f, -textureData->nHeight_ / 2.0f, 0.0f, 1.0f, color, 1.0f / textureData->numX_ * (numX + 1), 1.0f / textureData->numY_ * (numY + 1)},
  // 左下頂点
  {-textureData->nWidth_ / 2.0f, -textureData->nHeight_ / 2.0f, 0.0f, 1.0f, color, 1.0f / textureData->numX_ * numX,       1.0f / textureData->numY_ * (numY + 1)},
  };

  float sinTheta = sinf(rotAngle);
  float cosTheta = cosf(rotAngle);
  for(int i = 0; i < 4; i++)
  {
    TriangleFan[i].x_ = TriangleFan[i].x_ * cosTheta - TriangleFan[i].y_ * sinTheta; //x_' = xcosθ - ysinθ
    TriangleFan[i].y_ = TriangleFan[i].x_ * sinTheta + TriangleFan[i].y_ * cosTheta; //y_' = xsinθ + ycosθ
  }

  //xyの座標をずらす
  for(int i = 0; i < 4; i++)
  {
    TriangleFan[i].x_ += shiftPos.x;
    TriangleFan[i].y_ += shiftPos.y;
  }

  g_pd3dDevice->SetVertexDeclaration(g_pVertexDeclaration2);

  //描画
  HRESULT hr = 0;
  //アクティブなテクニックを設定する。
  hr = g_p2dShaderEffect->SetTechnique("RenderSceneWithTexture");
  hr = g_p2dShaderEffect->SetTexture("g_MeshTexture", textureData->pictureInfo_);
  hr = g_p2dShaderEffect->SetMatrix("g_mWorld", &mWorld);
  //アクティブなテクニックの中でパスを開始
  hr = g_p2dShaderEffect->Begin(NULL, 0);
  hr = g_p2dShaderEffect->BeginPass(0);
  //レンダリングする
  g_pd3dDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_USEW);//Zバッファを無効
  g_pd3dDevice->DrawPrimitiveUP(D3DPT_TRIANGLEFAN,        // プリミティブの種類(トライアングルファンにしている)
                                2,                        // プリミティブの数(描画するポリゴン数)
                                TriangleFan,              // 描画するカスタムバーテックスのポインタ
                                sizeof(CUSTOM_VERTEX));   // カスタムバーテックスのサイズ
  g_pd3dDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);//Zバッファを有効に戻す
  //アクティブなテクニックの中でパスを終了
  hr = g_p2dShaderEffect->EndPass();
  hr = g_p2dShaderEffect->End();
}

//入力関係
/*■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■*/

#include <dinput.h>
#pragma comment(lib,"dinput8.lib")

#define MOVE_SPEED (1000)

LPDIRECTINPUT8 g_pDinput = NULL;
LPDIRECTINPUTDEVICE8 g_pKeyDevice = NULL;
LPDIRECTINPUTDEVICE8 g_pMouseDevice = NULL;
LPDIRECTINPUTDEVICE8 g_pJoyDevice = NULL;

//アプリケーションで使用するコントローラーのプロパティを列挙して設定する
BOOL CALLBACK EnumObjectsCallback(const DIDEVICEOBJECTINSTANCE* pdidoi, VOID* pContext)
{
  if(pdidoi->dwType & DIDFT_AXIS)
  {
    DIPROPRANGE diprg;
    diprg.diph.dwSize = sizeof(DIPROPRANGE);
    diprg.diph.dwHeaderSize = sizeof(DIPROPHEADER);
    diprg.diph.dwHow = DIPH_BYID;
    diprg.diph.dwObj = pdidoi->dwType;
    diprg.lMin = -MOVE_SPEED;
    diprg.lMax = MOVE_SPEED;

    if(FAILED(g_pJoyDevice->SetProperty(DIPROP_RANGE, &diprg.diph)))
    {
      return DIENUM_STOP;
    }
  }
  return DIENUM_CONTINUE;
}

//利用可能なジョイスティックを列挙するコールバック関数
BOOL CALLBACK EnumJoysticksCallback(const DIDEVICEINSTANCE* pdidInstance, VOID* pContext)
{
  //複数列挙される場合、ユーザーに選択・確認させる
  TCHAR szConfirm[MAX_PATH + 1];
  sprintf_s(szConfirm,sizeof(TCHAR) * (MAX_PATH + 1), "この物理デバイスでデバイスオブジェクトを作成しますか？\n%s\n%s",
          pdidInstance->tszProductName, pdidInstance->tszInstanceName);
  //if(MessageBox(0, szConfirm, "確認", MB_YESNO) == IDNO)
  if(false)
  {
    return DIENUM_CONTINUE;
  }
  // 「DirectInputデバイス」オブジェクトの作成
  if(FAILED(g_pDinput->CreateDevice(pdidInstance->guidInstance,
     &g_pJoyDevice, NULL)))
  {
    return DIENUM_CONTINUE;
  }
  return DIENUM_STOP;
}

//キーボード使用の初期化
int InitKeyBoard(HWND hWnd)
{
  // 「DirectInputデバイス」オブジェクトの作成
  if(FAILED(g_pDinput->CreateDevice(GUID_SysKeyboard, &g_pKeyDevice, NULL)))
  {
    return -1;
  }
  // デバイスをキーボードに設定
  if(FAILED(g_pKeyDevice->SetDataFormat(&c_dfDIKeyboard)))
  {
    return -2;
  }
  // 協調レベルの設定
  if(FAILED(g_pKeyDevice->SetCooperativeLevel(
    hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND)))
  {
    return -3;
  }
  // デバイスを「取得」する
  if(FAILED(g_pKeyDevice->Acquire()))
  {
    return -4;
  }
  return 0;
}

//マウス使用の初期化
int InitMouse(HWND hWnd)
{
  // 「DirectInputデバイス」オブジェクトの作成
  if(FAILED(g_pDinput->CreateDevice(GUID_SysMouse, &g_pMouseDevice, NULL)))
  {
    return -1;
  }
  // デバイスをマウスに設定
  if(FAILED(g_pMouseDevice->SetDataFormat(&c_dfDIMouse2)))
  {
    return -2;
  }
  // 協調レベルの設定
  if(FAILED(g_pMouseDevice->SetCooperativeLevel(
    hWnd, DISCL_NONEXCLUSIVE | DISCL_FOREGROUND)))
  {
    return -3;
  }
  // デバイスを「取得」する
  if(FAILED(g_pMouseDevice->Acquire()))
  {
    return -4;
  }
  return 1;
}

//ゲームコントローラー使用の初期化
int InitJoyStick(HWND hWnd)
{
  //利用可能なゲームコントローラーの列挙関数を実行
  if(FAILED(g_pDinput->EnumDevices(DI8DEVCLASS_GAMECTRL, EnumJoysticksCallback,
     NULL, DIEDFL_ATTACHEDONLY)))
  {
    return -1;
  }
  if(!g_pJoyDevice)
  {
    return -2;
  }
  // デバイスをジョイスティックに設定
  if(FAILED(g_pJoyDevice->SetDataFormat(&c_dfDIJoystick2)))
  {
    return -3;
  }
  // 協調レベルの設定
  if(FAILED(g_pJoyDevice->SetCooperativeLevel(
    hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND)))
  {
    return -4;
  }
  //アプリケーションで使用するコントローラーのプロパティを列挙して設定する
  if(FAILED(g_pJoyDevice->EnumObjects(EnumObjectsCallback,
     NULL, DIDFT_ALL)))
  {
    return -5;
  }
  // デバイスを「取得」する
  if(FAILED(g_pJoyDevice->Acquire()))
  {
    return -6;
  }
  return 1;
}

//ダイレクトインプットの初期化関数
bool InitDinput(HWND hWnd)
{
  int setupKeyBoardResult = 0;
  int setupMouseResult = 0;
  int setupJoyStickResult = 0;
  // 「DirectInput」オブジェクトの作成
  if(FAILED(DirectInput8Create(GetModuleHandle(NULL),
     DIRECTINPUT_VERSION, IID_IDirectInput8, (VOID**)&g_pDinput, NULL)))
  {
    return false;
  }

  setupKeyBoardResult = InitKeyBoard(hWnd);
  setupMouseResult = InitMouse(hWnd);

  //ゲームコントローラー
  //if(MessageBox(hWnd, "ゲームコントローラーを使用しますか？", "", MB_YESNO | MB_APPLMODAL) == IDYES)
  {
    setupJoyStickResult = InitJoyStick(hWnd);
  }

  //エラー表示
  switch(setupKeyBoardResult)
  {
  case -1:
    //MessageBox(NULL, "DirectInputデバイスオブジェクトの作成失敗", "キーボード", NULL);
    break;
  case -2:
    //MessageBox(NULL, "デバイスの設定失敗", "キーボード", NULL);
    break;
  case -3:
    //MessageBox(NULL, "協調レベルの設定失敗", "キーボード", NULL);
    break;
  case -4:
    //MessageBox(NULL, "デバイスの取得失敗", "キーボード", NULL);
    break;
  }

  switch(setupMouseResult)
  {
  case -1:
    //MessageBox(NULL, "DirectInputデバイスオブジェクトの作成失敗", "マウス", NULL);
    break;
  case -2:
    //MessageBox(NULL, "デバイスの設定失敗", "マウス", NULL);
    break;
  case -3:
    //MessageBox(NULL, "協調レベルの設定失敗", "マウス", NULL);
    break;
  case -4:
    //MessageBox(NULL, "デバイスの取得失敗", "マウス", NULL);
    break;
  }

  switch(setupJoyStickResult)
  {
  case -1:
    //MessageBox(NULL, "コントローラーが見つかりません", "コントローラー", NULL);
    break;
  case -2:
    //MessageBox(NULL, "コントローラーが確認できません", "コントローラー", NULL);
    break;
  case -3:
    //MessageBox(NULL, "デバイスの設定失敗", "コントローラー", NULL);
    break;
  case -4:
    //MessageBox(NULL, "協調レベルの設定失敗", "コントローラー", NULL);
    break;
  case -5:
    //MessageBox(NULL, "コントローラーのプロパティ設定失敗", "コントローラー", NULL);
    break;
  case -6:
    //MessageBox(NULL, "デバイスの取得失敗", "コントローラー", NULL);
    break;
  }

  return true;
}

//キーデバイスの再取得とキー情報の更新
KEY_BOARD GetKeyBoardState()
{
  struct KEY_BOARD key = {0};
  if(g_pKeyDevice == NULL)
  {
    return key;
  }
  HRESULT hr = g_pKeyDevice->Acquire();
  if((hr == (DI_OK)) || (hr == S_FALSE))
  {
    BYTE diks[256];
    g_pKeyDevice->GetDeviceState(sizeof(diks), &diks);
    key.esc = diks[DIK_ESCAPE] & 0x80;
    key.num0 = diks[DIK_0] & 0x80;
    key.num1 = diks[DIK_1] & 0x80;
    key.num2 = diks[DIK_2] & 0x80;
    key.num3 = diks[DIK_3] & 0x80;
    key.num4 = diks[DIK_4] & 0x80;
    key.num5 = diks[DIK_5] & 0x80;
    key.num6 = diks[DIK_6] & 0x80;
    key.num7 = diks[DIK_7] & 0x80;
    key.num8 = diks[DIK_8] & 0x80;
    key.num9 = diks[DIK_9] & 0x80;
    key.numPad0 = diks[DIK_NUMPAD0] & 0x80;
    key.numPad1 = diks[DIK_NUMPAD1] & 0x80;
    key.numPad2 = diks[DIK_NUMPAD2] & 0x80;
    key.numPad3 = diks[DIK_NUMPAD3] & 0x80;
    key.numPad4 = diks[DIK_NUMPAD4] & 0x80;
    key.numPad5 = diks[DIK_NUMPAD5] & 0x80;
    key.numPad6 = diks[DIK_NUMPAD6] & 0x80;
    key.numPad7 = diks[DIK_NUMPAD7] & 0x80;
    key.numPad8 = diks[DIK_NUMPAD8] & 0x80;
    key.numPad9 = diks[DIK_NUMPAD9] & 0x80;
    key.plus    = diks[DIK_ADD] & 0x80;
    key.minus   = diks[DIK_SUBTRACT] & 0x80;
    key.slash   = diks[DIK_NUMPADSLASH] & 0x80;
    key.asterisk= diks[DIK_NUMPADSTAR] & 0x80;
    key.tab = diks[DIK_TAB] & 0x80;
    key.a = diks[DIK_A] & 0x80;
    key.b = diks[DIK_B] & 0x80;
    key.c = diks[DIK_C] & 0x80;
    key.d = diks[DIK_D] & 0x80;
    key.e = diks[DIK_E] & 0x80;
    key.f = diks[DIK_F] & 0x80;
    key.g = diks[DIK_G] & 0x80;
    key.h = diks[DIK_H] & 0x80;
    key.i = diks[DIK_I] & 0x80;
    key.j = diks[DIK_J] & 0x80;
    key.k = diks[DIK_K] & 0x80;
    key.l = diks[DIK_L] & 0x80;
    key.n = diks[DIK_N] & 0x80;
    key.m = diks[DIK_M] & 0x80;
    key.o = diks[DIK_O] & 0x80;
    key.p = diks[DIK_P] & 0x80;
    key.q = diks[DIK_Q] & 0x80;
    key.r = diks[DIK_R] & 0x80;
    key.s = diks[DIK_S] & 0x80;
    key.t = diks[DIK_T] & 0x80;
    key.u = diks[DIK_U] & 0x80;
    key.v = diks[DIK_V] & 0x80;
    key.w = diks[DIK_W] & 0x80;
    key.x = diks[DIK_X] & 0x80;
    key.y = diks[DIK_Y] & 0x80;
    key.z = diks[DIK_Z] & 0x80;
    key.rEnter = diks[DIK_RETURN] & 0x80;
    key.lCtrl = diks[DIK_LCONTROL] & 0x80;
    key.lShift = diks[DIK_LSHIFT] & 0x80;
    key.space = diks[DIK_SPACE] & 0x80;
    key.alt = diks[DIK_LMENU] & 0x80;
    key.f1 = diks[DIK_F1] & 0x80;
    key.f2 = diks[DIK_F2] & 0x80;
    key.f3 = diks[DIK_F3] & 0x80;
    key.f4 = diks[DIK_F4] & 0x80;
    key.f5 = diks[DIK_F5] & 0x80;
    key.f6 = diks[DIK_F6] & 0x80;
    key.f7 = diks[DIK_F7] & 0x80;
    key.f8 = diks[DIK_F8] & 0x80;
    key.f9 = diks[DIK_F9] & 0x80;
    key.f10 = diks[DIK_F10] & 0x80;
    key.f11 = diks[DIK_F11] & 0x80;
    key.f12 = diks[DIK_F12] & 0x80;
    key.left = diks[DIK_LEFT] & 0x80;
    key.right = diks[DIK_RIGHT] & 0x80;
    key.up = diks[DIK_UP] & 0x80;
    key.down = diks[DIK_DOWN] & 0x80;
  }
  return key;
}

#define DEFAULT_MOUSE_SEVERITY (20.0f) // マウスの感度の基準値
#define MOUSE_MARGIN           (0.06f) // マウスの遊び 感度を１とし、n倍以下の時入力を0とする
static int g_mouseSeverity = DEFAULT_MOUSE_SEVERITY; //マウス感度

//マウスデバイスの再取得とキー情報の更新
//返り値のxyzはそれぞれ相対位置依存
//xyzはDEFAULT_MOUSE_SEVERITYを基準1.0とした割合の値
MOUSE GetMouseState()
{
  struct MOUSE mouse = {0};
  if(g_pMouseDevice == NULL)
    return mouse;
  DIMOUSESTATE2 dims = {0};
  if(FAILED(g_pMouseDevice->GetDeviceState(sizeof(DIMOUSESTATE2), &dims)))
  {
    g_pMouseDevice->Acquire();
  }
  mouse.x = (float)dims.lX / g_mouseSeverity;
  mouse.y = (float)dims.lY / g_mouseSeverity;
  mouse.z = (float)dims.lZ / g_mouseSeverity;
  mouse.button[0] = dims.rgbButtons[0];
  mouse.button[1] = dims.rgbButtons[1];
  mouse.button[2] = dims.rgbButtons[2];
  mouse.button[3] = dims.rgbButtons[3];
  mouse.button[4] = dims.rgbButtons[4];
  mouse.button[5] = dims.rgbButtons[5];
  mouse.button[6] = dims.rgbButtons[6];
  mouse.button[7] = dims.rgbButtons[7];

  if (mouse.x * mouse.x <= MOUSE_MARGIN * MOUSE_MARGIN) { mouse.x = 0.0f; }
  if (mouse.y * mouse.y <= MOUSE_MARGIN * MOUSE_MARGIN) { mouse.y = 0.0f; }
  if (mouse.z * mouse.z <= MOUSE_MARGIN * MOUSE_MARGIN) { mouse.z = 0.0f; }
  return mouse;
}

//マウス感度を決定する
//severityPercent : 基準値を100%とした割合 大きければ感度は高くなる
void SetMouseSeverity(int severityPercent)
{
  g_mouseSeverity = DEFAULT_MOUSE_SEVERITY * 100.0f / severityPercent;
}

#define STICK_MARGIN (0.06f)   //スティックの遊び　入力の最大を１とし、n倍以下の時入力をとらない

//ゲームパッドデバイスの再取得とキー情報の更新
JOY_STICK GetJoyStickState()
{
  struct JOY_STICK joyStick = {0};
  if(g_pJoyDevice == NULL)
    return joyStick;
  HRESULT hr = g_pJoyDevice->Acquire();
  if((hr == DI_OK) || (hr == S_FALSE))
  {
    struct DIJOYSTATE2 js = {0};
    g_pJoyDevice->GetDeviceState(sizeof(DIJOYSTATE2), &js);

    //スティックとボタンの入力値設定
    joyStick.leftStickX  = js.lX / (float)MOVE_SPEED;
    joyStick.leftStickY  = -js.lY / (float)MOVE_SPEED;
    joyStick.z           = js.lZ / (float)MOVE_SPEED;
    joyStick.rightStickX = js.lRx / (float)MOVE_SPEED;
    joyStick.rightStickY = -js.lRy / (float)MOVE_SPEED;
    joyStick.button[0]   = js.rgbButtons[0] & 0x80;
    joyStick.button[1]   = js.rgbButtons[1] & 0x80;
    joyStick.button[2]   = js.rgbButtons[2] & 0x80;
    joyStick.button[3]   = js.rgbButtons[3] & 0x80;
    joyStick.button[4]   = js.rgbButtons[4] & 0x80;
    joyStick.button[5]   = js.rgbButtons[5] & 0x80;
    joyStick.button[6]   = js.rgbButtons[6] & 0x80;
    joyStick.button[7]   = js.rgbButtons[7] & 0x80;
    joyStick.button[8]   = js.rgbButtons[8] & 0x80;
    joyStick.button[9]   = js.rgbButtons[9] & 0x80;
    joyStick.button[10]  = js.rgbButtons[10] & 0x80;
    joyStick.button[11]  = js.rgbButtons[11] & 0x80;
    joyStick.button[12]  = js.rgbButtons[12] & 0x80;
    joyStick.button[13]  = js.rgbButtons[13] & 0x80;
    joyStick.button[14]  = js.rgbButtons[14] & 0x80;
    joyStick.button[15]  = js.rgbButtons[15] & 0x80;
    joyStick.button[16]  = js.rgbButtons[16] & 0x80;
    joyStick.button[17]  = js.rgbButtons[17] & 0x80;
    joyStick.button[18]  = js.rgbButtons[18] & 0x80;
    joyStick.button[19]  = js.rgbButtons[19] & 0x80;
    joyStick.button[20]  = js.rgbButtons[20] & 0x80;
    joyStick.button[21]  = js.rgbButtons[21] & 0x80;
    joyStick.button[22]  = js.rgbButtons[22] & 0x80;
    joyStick.button[23]  = js.rgbButtons[23] & 0x80;

    //スティックの遊びを適用
    if( (joyStick.leftStickX * joyStick.leftStickX)  <= STICK_MARGIN * STICK_MARGIN) {joyStick.leftStickX = 0.0f; }             
    if( (joyStick.leftStickY * joyStick.leftStickY)  <= STICK_MARGIN * STICK_MARGIN) {joyStick.leftStickY = 0.0f; }             
    if(          (joyStick.z * joyStick.z)           <= STICK_MARGIN * STICK_MARGIN) {joyStick.z  = 0.0f;         }
    if((joyStick.rightStickX * joyStick.rightStickX) <= STICK_MARGIN * STICK_MARGIN) {joyStick.rightStickX = 0.0f;}
    if((joyStick.rightStickY * joyStick.rightStickY) <= STICK_MARGIN * STICK_MARGIN) {joyStick.rightStickY = 0.0f;}

    //十字キーの入力値設定
    if(!(LOWORD(js.rgdwPOV) == 0xFFFF))
    {
      if(js.rgdwPOV[0] == 0xffffffff)
      {
          joyStick.up    = false;
          joyStick.down  = false;
          joyStick.left  = false;
          joyStick.right = false;
      }
      else{
        float angle = (float)js.rgdwPOV[0] / 18000.0f * D3DX_PI - D3DX_PI / 2.0f;//右側を0度にするためπ/2を減算
        float x = cosf(angle);
        float y = sinf(angle);
        //xy成分から上下の判断
        //誤差のため0でなく0.1を使用
        if(y < -0.1f)
          joyStick.up = true;
        if(y > 0.1f)
          joyStick.down = true;
        if(x < -0.1f)
          joyStick.left = true;
        if(x > 0.1f)
          joyStick.right = true;
      }
    }
  }
  return joyStick;
}

//あたり判定
/*■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■*/

//スキンメッシュ用のバウンディングボックスの生成
//InitBBox呼び出し関数
bool InitBBoxAnimationMesh(MESHDATA* meshData, D3DXVECTOR3* pvMax, D3DXVECTOR3* pvMin)
{
  if (meshData == NULL)
  {
    return false;
  }

  HRESULT hr = NULL;
  LPDIRECT3DVERTEXBUFFER9 pVB = NULL;
  VOID* pVertices = NULL;
  D3DXVECTOR3 vMax = { 0.0f,0.0f,0.0f };
  D3DXVECTOR3 vMin = { 0.0f,0.0f,0.0f };
  LPD3DXMESHCONTAINER nowMeshContainer = meshData->pFrameRoot_->pMeshContainer;

  if (nowMeshContainer == NULL)
  {
    return false;
  }

  while(nowMeshContainer){
    //メッシュの頂点バッファーをロックする
    if (FAILED(nowMeshContainer->MeshData.pMesh->GetVertexBuffer(&pVB)))
    {
      return false;
    }
    if (FAILED(pVB->Lock(0, 0, &pVertices, 0)))
    {
      SAFE_FREE(pVB);
      return false;
    }
    D3DXVECTOR3 tmpMax, tmpMin;
    // メッシュ内頂点位置の最大と最小を検索する
    hr = D3DXComputeBoundingBox((D3DXVECTOR3*)pVertices, meshData->pMesh_->GetNumVertices(),
                                D3DXGetFVFVertexSize(meshData->pMesh_->GetFVF()), &tmpMax, &tmpMin);
    pVB->Unlock();
    SAFE_FREE(pVB);

    //maxを比べて更新
    vMax.x = max(vMax.x, tmpMax.x);
    vMax.y = max(vMax.y, tmpMax.y);
    vMax.z = max(vMax.z, tmpMax.z);
    //minを比べて更新
    vMin.x = min(vMin.x, tmpMin.x);
    vMin.y = min(vMin.y, tmpMin.y);
    vMin.z = min(vMin.z, tmpMin.z);

    //次のコンテナを参照
    nowMeshContainer = nowMeshContainer->pNextMeshContainer;
  }

  if (FAILED(hr))
  {
    return false;
  }
  *pvMax = vMax;
  *pvMin = vMin;
  return true;
}

//スキンメッシュ以外のバウンディングボックスの生成
//InitBBox呼び出し関数
bool InitBBoxNoAnimationMesh(MESHDATA* meshData, D3DXVECTOR3* pvMax, D3DXVECTOR3* pvMin)
{
  if (meshData == NULL)
  {
    return false;
  }
  if (meshData->pMesh_ == NULL)
  {
    return false;
  }

  HRESULT hr = NULL;
  LPDIRECT3DVERTEXBUFFER9 pVB = NULL;
  VOID* pVertices = NULL;
  D3DXVECTOR3 vMax, vMin;

  //メッシュの頂点バッファーをロックする
  if (FAILED(meshData->pMesh_->GetVertexBuffer(&pVB)))
  {
    return false;
  }
  if (FAILED(pVB->Lock(0, 0, &pVertices, 0)))
  {
    SAFE_FREE(pVB);
    return false;
  }
  // メッシュ内頂点位置の最大と最小を検索する
  hr = D3DXComputeBoundingBox((D3DXVECTOR3*)pVertices, meshData->pMesh_->GetNumVertices(),
                              D3DXGetFVFVertexSize(meshData->pMesh_->GetFVF()), &vMin, &vMax);
  pVB->Unlock();
  SAFE_FREE(pVB);

  if (FAILED(hr))
  {
    return false;
  }
  *pvMax = vMax;
  *pvMin = vMin;
  return true;
}

//ボックスの計算
//posType : enum POS_TYPE を参照
bool InitBBox(void* meshHandle, int posType)
{
  if(meshHandle == NULL)
  {
    return false;
  }
  MESHDATA* data = (MESHDATA*)meshHandle;
  D3DXVECTOR3 vMax, vMin;
  bool hr = NULL;

  if (data->isInitBBox_ == true)
  {
    return false; //生成済み
  }
  if (data->meshKind_ == ANIMATION_MESH)
  {
    //スキンメッシュの場合
    if (!InitBBoxAnimationMesh(data, &vMax, &vMin))
    {
      return false;
    }
  }
  else
  {
    //スキンメッシュ以外の場合
    if(!InitBBoxNoAnimationMesh(data, &vMax, &vMin))
      return false;
  }

  if(FAILED(hr))
  {
    return false;
  }
  //軸ベクトルと軸の長さ（この場合ボックスの辺の長さ）を初期化する
  data->BBox_.fLengthX = (vMax.x - vMin.x) / 2;
  data->BBox_.fLengthY = (vMax.y - vMin.y) / 2;
  data->BBox_.fLengthZ = (vMax.z - vMin.z) / 2;
  data->BBox_.posType = posType;

  data->isInitBBox_ = true;
  return true;
}

//ボックスの手動作成
//posType : enum POS_TYPE を参照
bool InitBBoxManual(void* meshHandle, float sizeX, float sizeY, float sizeZ, int posType)
{
  if (meshHandle == NULL)
  {
    return false;
  }
  MESHDATA* data = (MESHDATA*)meshHandle;
  D3DXVECTOR3 vMax, vMin;

  //軸ベクトルと軸の長さ（この場合ボックスの辺の長さ）を初期化する
  data->BBox_.fLengthX = sizeX;
  data->BBox_.fLengthY = sizeY;
  data->BBox_.fLengthZ = sizeZ;
  data->BBox_.posType = posType;

  data->isInitBBox_ = true;
  return true;
}

//メッシュデータからバウンディングボックスを取得
BBOX GetBBox(void* meshHandle,D3DXVECTOR3 scale)
{
  BBOX result = {0};
  if(meshHandle == NULL) { return result; }

  MESHDATA* data = (MESHDATA*)meshHandle;
  if (data->isInitBBox_ == false)
  {
    InitBBox(meshHandle, POS_TYPE_CENTER);
  }
  result = data->BBox_;
  result.fLengthX *= scale.x;
  result.fLengthY *= scale.y;
  result.fLengthZ *= scale.z;
  return result;
}

//「距離」が「２つの影の合計」より大きい場合（非衝突）はfalse  等しいか小さい場合（衝突）はtrue
// この関数は、OBBCollisionDetection関数のみに呼び出されるサブルーチン
BOOL CompareLength(BBOX* pboxA, BBOX* pboxB, D3DXVECTOR3* pvSeparate, D3DXVECTOR3* pvDistance)
{
  //”分離軸上での”ボックスＡの中心からボックスＢの中心までの距離
  FLOAT fDistance = fabsf(D3DXVec3Dot(pvDistance, pvSeparate));
  //分離軸上でボックスAの中心から最も遠いボックスAの頂点までの距離
  FLOAT fShadowA = 0;
  //分離軸上でボックスBの中心から最も遠いボックスBの頂点までの距離
  FLOAT fShadowB = 0;
  //それぞれのボックスの”影”を算出
  fShadowA = fabsf(D3DXVec3Dot(&pboxA->vAxisX, pvSeparate) * pboxA->fLengthX) +
    fabsf(D3DXVec3Dot(&pboxA->vAxisY, pvSeparate) * pboxA->fLengthY) +
    fabsf(D3DXVec3Dot(&pboxA->vAxisZ, pvSeparate) * pboxA->fLengthZ);

  fShadowB = fabsf(D3DXVec3Dot(&pboxB->vAxisX, pvSeparate) * pboxB->fLengthX) +
    fabsf(D3DXVec3Dot(&pboxB->vAxisY, pvSeparate) * pboxB->fLengthY) +
    fabsf(D3DXVec3Dot(&pboxB->vAxisZ, pvSeparate) * pboxB->fLengthZ);
  if(fDistance > fShadowA + fShadowB)
  {
    return false;
  }
  return true;
}

//OBBによる衝突判定。衝突している場合はtrue、離れている場合はfalse。
bool OBBCollisionDetection(D3DXVECTOR3 pos_A, float yawAngle_A, BBOX* BBox_A, D3DXVECTOR3 pos_B, float yawAngle_B, BBOX* BBox_B)
{
  //directX使用のために型変換
  D3DXVECTOR3 posA = CONVERT_VECTOR(pos_A);
  D3DXVECTOR3 posB = CONVERT_VECTOR(pos_B);
  D3DXMATRIX mRotation_A;
  D3DXMATRIX mRotation_B;
  D3DXMatrixRotationY(&mRotation_A, -yawAngle_A);
  D3DXMatrixRotationY(&mRotation_B, -yawAngle_B);
  //足元がpos_基準のモデルは中心に補正する
  if (BBox_A->posType == POS_TYPE_BOTTOM)
  {
    posA.y += BBox_A->fLengthY;
  }
  if (BBox_B->posType == POS_TYPE_BOTTOM)
  {
    posB.y += BBox_B->fLengthY;
  }

  //ボックスの距離ベクトル（中心と中心を結んだベクトル）
  D3DXVECTOR3 vDistance = posB - posA;
  //分離軸
  D3DXVECTOR3 vSeparate;

  //それぞれのローカル基底軸ベクトルに、それぞれの回転を反映させる
  BBox_A->vAxisX = D3DXVECTOR3(1, 0, 0);
  BBox_A->vAxisY = D3DXVECTOR3(0, 1, 0);
  BBox_A->vAxisZ = D3DXVECTOR3(0, 0, 1);

  BBox_B->vAxisX = D3DXVECTOR3(1, 0, 0);
  BBox_B->vAxisY = D3DXVECTOR3(0, 1, 0);
  BBox_B->vAxisZ = D3DXVECTOR3(0, 0, 1);

  D3DXVec3TransformCoord(&BBox_A->vAxisX, &BBox_A->vAxisX, &mRotation_A);
  D3DXVec3TransformCoord(&BBox_A->vAxisY, &BBox_A->vAxisY, &mRotation_A);
  D3DXVec3TransformCoord(&BBox_A->vAxisZ, &BBox_A->vAxisZ, &mRotation_A);

  D3DXVec3TransformCoord(&BBox_B->vAxisX, &BBox_B->vAxisX, &mRotation_B);
  D3DXVec3TransformCoord(&BBox_B->vAxisY, &BBox_B->vAxisY, &mRotation_B);
  D3DXVec3TransformCoord(&BBox_B->vAxisZ, &BBox_B->vAxisZ, &mRotation_B);

  //ボックスAのローカル基底軸を基準にした、”影”の算出（3パターン）
  {
    //X軸を分離軸とした場合
    if(!CompareLength(BBox_A, BBox_B, &BBox_A->vAxisX, &vDistance))
      return false;
    //Y軸を分離軸とした場合
    if(!CompareLength(BBox_A, BBox_B, &BBox_A->vAxisY, &vDistance))
      return false;
    //Z軸を分離軸とした場合
    if(!CompareLength(BBox_A, BBox_B, &BBox_A->vAxisZ, &vDistance))
      return false;
  }

  //ボックスBのローカル基底軸を基準にした、”影”の算出（3パターン）
  {
    //X軸を分離軸とした場合
    if(!CompareLength(BBox_A, BBox_B, &BBox_B->vAxisX, &vDistance)) return false;
    //Y軸を分離軸とした場合
    if(!CompareLength(BBox_A, BBox_B, &BBox_B->vAxisY, &vDistance)) return false;
    //Z軸を分離軸とした場合
    if(!CompareLength(BBox_A, BBox_B, &BBox_B->vAxisZ, &vDistance)) return false;
  }

  //お互いの基底軸同士の外積ベクトルを基準にした、”影”の算出（9パターン）
  {
    //ボックスAのX軸
    {
      //と　ボックスBのX軸との外積ベクトルを分離軸とした場合
      D3DXVec3Cross(&vSeparate, &BBox_A->vAxisX, &BBox_B->vAxisX);
      if(!CompareLength(BBox_A, BBox_B, &vSeparate, &vDistance)) return false;
      //と　ボックスBのY軸との外積ベクトルを分離軸とした場合
      D3DXVec3Cross(&vSeparate, &BBox_A->vAxisX, &BBox_B->vAxisY);
      if(!CompareLength(BBox_A, BBox_B, &vSeparate, &vDistance)) return false;
      //と　ボックスBのZ軸との外積ベクトルを分離軸とした場合
      D3DXVec3Cross(&vSeparate, &BBox_A->vAxisX, &BBox_B->vAxisZ);
      if(!CompareLength(BBox_A, BBox_B, &vSeparate, &vDistance)) return false;
    }

    //ボックスAのY軸
    {
      //と　ボックスBのX軸との外積ベクトルを分離軸とした場合
      D3DXVec3Cross(&vSeparate, &BBox_A->vAxisY, &BBox_B->vAxisX);
      if(!CompareLength(BBox_A, BBox_B, &vSeparate, &vDistance)) return false;
      //と　ボックスBのY軸との外積ベクトルを分離軸とした場合
      D3DXVec3Cross(&vSeparate, &BBox_A->vAxisY, &BBox_B->vAxisY);
      if(!CompareLength(BBox_A, BBox_B, &vSeparate, &vDistance)) return false;
      //と　ボックスBのZ軸との外積ベクトルを分離軸とした場合
      D3DXVec3Cross(&vSeparate, &BBox_A->vAxisY, &BBox_B->vAxisZ);
      if(!CompareLength(BBox_A, BBox_B, &vSeparate, &vDistance)) return false;
    }

    //ボックスAのZ軸
    {
      //と　ボックスBのX軸との外積ベクトルを分離軸とした場合
      D3DXVec3Cross(&vSeparate, &BBox_A->vAxisZ, &BBox_B->vAxisX);
      if(!CompareLength(BBox_A, BBox_B, &vSeparate, &vDistance)) return false;
      //と　ボックスBのY軸との外積ベクトルを分離軸とした場合
      D3DXVec3Cross(&vSeparate, &BBox_A->vAxisZ, &BBox_B->vAxisY);
      if(!CompareLength(BBox_A, BBox_B, &vSeparate, &vDistance)) return false;
      //と　ボックスBのZ軸との外積ベクトルを分離軸とした場合
      D3DXVec3Cross(&vSeparate, &BBox_A->vAxisZ, &BBox_B->vAxisZ);
      if(!CompareLength(BBox_A, BBox_B, &vSeparate, &vDistance)) return false;
    }
  }
  return true;
}


//第3引数で渡された指数のポリゴンの頂点を見つける(第1引数はD3DXVECTOR3型が3個以上の配列であるとする)
bool FindVerticesOnPoly(D3DXVECTOR3* pvVertices, LPD3DXMESH pMesh, DWORD dwPolyIndex)
{
  DWORD dwStride = pMesh->GetNumBytesPerVertex();
  DWORD dwVertexNum = pMesh->GetNumVertices();
  DWORD dwPolyNum = pMesh->GetNumFaces();
  WORD* pwPoly = NULL;
  pMesh->LockIndexBuffer(D3DLOCK_READONLY, (VOID**)&pwPoly);

  BYTE *pbVertices = NULL;
  FLOAT* pfVetices = NULL;
  LPDIRECT3DVERTEXBUFFER9 VB = NULL;
  pMesh->GetVertexBuffer(&VB);
  if(SUCCEEDED(VB->Lock(0, 0, (VOID**)&pbVertices, 0)))
  {
    pfVetices = (FLOAT*)&pbVertices[dwStride*pwPoly[dwPolyIndex * 3]];
    pvVertices[0].x = pfVetices[0];
    pvVertices[0].y = pfVetices[1];
    pvVertices[0].z = pfVetices[2];

    pfVetices = (FLOAT*)&pbVertices[dwStride*pwPoly[dwPolyIndex * 3 + 1]];
    pvVertices[1].x = pfVetices[0];
    pvVertices[1].y = pfVetices[1];
    pvVertices[1].z = pfVetices[2];

    pfVetices = (FLOAT*)&pbVertices[dwStride*pwPoly[dwPolyIndex * 3 + 2]];
    pvVertices[2].x = pfVetices[0];
    pvVertices[2].y = pfVetices[1];
    pvVertices[2].z = pfVetices[2];

    pMesh->UnlockIndexBuffer();
    VB->Unlock();
  }
  return S_OK;
}

//レイによる衝突判定　レイが相手メッシュと交差する場合は、pfDistanceに交点との距離を入れてtrueを返す
bool RayIntersect(float* pfDistance,int* dwIndex, D3DXVECTOR3* interPos, D3DXVECTOR3* normal, D3DXVECTOR3 rayStartPos, D3DXVECTOR3 rayVector, D3DXVECTOR3 pos_B, void* meshHandle_B, float objYawAngle, D3DXVECTOR3 scale)
{
  if(!meshHandle_B)
    return false;

  BOOL boHit = false;
  D3DXVECTOR3 startPos = CONVERT_VECTOR(rayStartPos);
  D3DXVECTOR3 rayVec = CONVERT_VECTOR(rayVector);
  MESHDATA* data_B = (MESHDATA*)meshHandle_B;

  //ワールドトランスフォームは”個々の物体ごとに”必要
  D3DXMATRIX mWorld;       //最終的なワールド行列
  D3DXMATRIX mInvWorld;    //最終的なワールド逆行列
  D3DXMATRIX mScale;       //スケーリング行列
  D3DXMATRIX mRotation;    //回転行列
  D3DXMATRIX mInvRotation; //回転逆行列
  D3DXMATRIX mTranslation; //平行移動行列

  //行列の初期化
  D3DXMatrixIdentity(&mWorld);
  D3DXMatrixIdentity(&mScale);
  D3DXMatrixIdentity(&mRotation);
  D3DXMatrixIdentity(&mTranslation);

  D3DXMatrixTranslation(&mTranslation, pos_B.x, pos_B.y, pos_B.z);//移動
  D3DXMatrixScaling(&mScale, scale.x, scale.y, scale.z);          //スケーリング
  D3DXMatrixRotationY(&mRotation, -objYawAngle);                  //回転
  mWorld = mScale * mRotation * mTranslation;                     //ワールド行列＝スケーリング×回転×移動
  D3DXMatrixInverse(&mInvWorld, NULL, &mWorld);                   //対象物のワールド逆行列
  D3DXMatrixInverse(&mInvRotation, NULL, &mRotation);             //対象物のローテーション逆行列

  D3DXVec3TransformCoord(&startPos, &startPos, &mInvWorld);
  D3DXVec3TransformCoord(&rayVec, &rayVec, &mInvRotation);

  if(data_B->meshKind_ == FBX_MESH)
  {
    D3DXVECTOR3 intersectionPos;
    D3DXVECTOR3 n;
    intersectionPos = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
    n = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
    //レイのスカラー
    float dis = sqrt(rayVector.x * rayVector.x + rayVector.y * rayVector.y + rayVector.z * rayVector.z);
    dis = sqrt(rayVec.x * rayVec.x + rayVec.y * rayVec.y + rayVec.z * rayVec.z);
    //レイを飛ばす
    boHit = data_B->fbxMeshData_->Raycast(startPos, rayVec, dis, intersectionPos, n);
    //交点との距離を求める
    D3DXVECTOR3 sub = intersectionPos - startPos;
    *pfDistance = sqrt(sub.x * sub.x + sub.y * sub.y + sub.z * sub.z);

    if(*pfDistance > dis)
    {
      boHit = false;
    }
    else
    {
      *dwIndex = boHit;
      D3DXVec3TransformCoord(&intersectionPos, &intersectionPos, &mWorld);
      *interPos = intersectionPos;
      *normal = n;
    }

    boHit = boHit > 0 ? true : false;
  }
  else
  {
    //D3DXIntersect(data_B->pMesh_, &vStart, &vDirection, &boHit, (DWORD*)dwIndex, NULL, NULL, pfDistance, NULL, NULL);
  }

  if(boHit == true)
  {
    return true;
  }
  return false;
}

//レイによる衝突判定　レイが相手メッシュと交差する場合は、pfDistanceに距離を、pvNormalに衝突面の法線を入れてtrueを返す
//rayStartPos  :レイの始まり地点の座標
//rayDir       :レイの向き
//meshHandle_B :対象のメッシュデータのハンドル
//yawAngle_B   :対象のy_軸基準の回転角度
//pos_B        :対象の基準座標
//scale_B      :対象の大きさ
//pfDistance   :レイの開始から交差地点までの距離を返す
//pvNormal     :レイが当たったポリゴンの法線ベクトル
//返り値 衝突した場合true
bool Collide(D3DXVECTOR3 rayStartPos, D3DXVECTOR3 rayDir, void* meshHandle_B, float yawAngle_B, D3DXVECTOR3 pos_B,D3DXVECTOR3 scale_B, float* pfDistance, D3DXVECTOR3* pvNormal)
{
  //正しいハンドルデータか判断する
  if( meshHandle_B == NULL ) { return false; }
  MESHDATA* pData = (MESHDATA*)meshHandle_B;
  if(pData->meshKind_ > MESH_KIND_MAX || pData->meshKind_ < 0) { return false; }

  BOOL boHit = false;
  D3DXMATRIX mWorld, mScale, mTranslation, mRotation;
  D3DXVECTOR3 L; //入射ベクトル（レイ）
  D3DXVECTOR3 vStart; //反射ベクトル（跳ね返る方向）
  L = CONVERT_VECTOR(rayDir);
  vStart = CONVERT_VECTOR(rayStartPos);
  D3DXVec3Normalize(&L, &L);

  // レイを当てるメッシュが動いていたり回転している場合でも対象のワールド行列の逆行列を用いれば正しくレイが当たる
  D3DXMatrixTranslation(&mTranslation, pos_B.x, pos_B.y, pos_B.z);//移動
  D3DXMatrixRotationY(&mRotation,-yawAngle_B);                    //回転
  D3DXMatrixScaling(&mScale, scale_B.x, scale_B.y, scale_B.z);    //スケーリング
  mWorld = mScale * mRotation * mTranslation;//ワールド行列＝スケーリング×回転×移動
  D3DXVec3TransformCoord(&vStart, &vStart, &mWorld);

  DWORD dwPolyIndex;
  D3DXVECTOR3 vDirection = vStart - L;
  D3DXIntersect(pData->pMesh_, &vStart, &L, &boHit, &dwPolyIndex, NULL, NULL, pfDistance, NULL, NULL);
  if(boHit)
  {
    //交差しているポリゴンの頂点を見つける
    D3DXVECTOR3 vVertex[3];
    FindVerticesOnPoly(vVertex, pData->pMesh_, dwPolyIndex);
    D3DXPLANE p;
    //その頂点から平面方程式を得る
    D3DXPlaneFromPoints(&p, &vVertex[0], &vVertex[1], &vVertex[2]);
    //平面方程式の係数が法線の成分
    pvNormal->x = p.a;
    pvNormal->y = p.b;
    pvNormal->z = p.c;

    return true;
  }
  return false;
}

//特定ポリゴンに対する反射各を求める
//meshHandle:反射物のメッシュデータハンドル
//index:反射物の反射するポリゴンの指数
//vIncidentDirection:入射角のベクトル
D3DXVECTOR3 Bounce(void* meshHandle, int index, D3DXVECTOR3 vIncidentDirection)
{
  MESHDATA* pData = (MESHDATA*)meshHandle;
  D3DXVECTOR3 L; //入射ベクトル（レイ）
  D3DXVECTOR3 N; //ポリゴンの法線
  D3DXVECTOR3 R; //反射ベクトル（跳ね返る方向）

  L = CONVERT_VECTOR(vIncidentDirection);
  L *= -1;

  //メッシュ内ポリゴンの3頂点を取得する
  D3DXVECTOR3 vVertex[3];
  FindVerticesOnPoly(vVertex, pData->pMesh_, index);
  //その３頂点を基に、平面方程式を計算する
  D3DXPLANE Plane;
  D3DXPlaneFromPoints(&Plane, &vVertex[0], &vVertex[1], &vVertex[2]);
  N.x = Plane.a;
  N.y = Plane.b;
  N.z = Plane.c;
  //反射角の計算
  R = (2 * D3DXVec3Dot(&N, &L) * N) - L;
  D3DXVec3Normalize(&R, &R);

  D3DXVECTOR3 result = {R.x, R.y, R.z};
  return result;
}

// L:入射ベクトル（レイ） N:ポリゴンの法線
D3DXVECTOR3 Slip(D3DXVECTOR3 L, D3DXVECTOR3 N)
{
  //DirectX用のベクトル変数に変換
  D3DXVECTOR3 vS; //滑りベクトル（滑る方向）
  D3DXVECTOR3 vL = CONVERT_VECTOR(L); //入射ベクトル（レイ)
  D3DXVECTOR3 vN = CONVERT_VECTOR(N); //ポリゴンの法線

  //滑りベクトル S=L-(N * L)/(|N|^2)*N
  vS = vL - ((D3DXVec3Dot(&vN, &vL)) / (pow(D3DXVec3Length(&vN), 2))) * vN;

  //ライブラリ用のベクトル変数に変換
  D3DXVECTOR3 S;
  S.x = vS.x;
  S.y = vS.y;
  S.z = vS.z;

  return S;
}

//ログの書き出し
/*■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■*/

#define DEBUG_FILE_NAME ("GAME_DEBUG.txt")
static int g_writeOutLogCallNum = 0; //ログ書き出し回数

//指定ファイル名にログを書き出す
void WriteOutLog(const char *fmt, ...)
{
  FILE* fp = NULL;

  //1回目の呼び出し時にログファイルの中身を空にする
  if(g_writeOutLogCallNum == 0)
  {
    fp = fopen(DEBUG_FILE_NAME, "w");
    if(fp == NULL) { MessageBox(NULL, "ログファイルの作成失敗", NULL, NULL); return; }

    time_t timer;
    struct tm *t_st;
    //曜日名テーブル
    char *wday[] = {"(日)","(月)","(火)","(水)","(木)","(金)","(土)"};
    /* 現在時刻の取得 */
    time(&timer);
    /* 現在時刻を構造体に変換 */
    t_st = localtime(&timer);
    //現在時刻の書き出し
    fprintf(fp, "%d年 ", t_st->tm_year + 1900);
    fprintf(fp, "%d月 ", t_st->tm_mon + 1);
    fprintf(fp, "%d日", t_st->tm_mday);
    fprintf(fp, "%s ", wday[t_st->tm_wday]);
    fprintf(fp, "%d時 ", t_st->tm_hour);
    fprintf(fp, "%d分 ", t_st->tm_min);
    fprintf(fp, "%d秒\n", t_st->tm_sec);

    //ファイルを閉じる
    fclose(fp);
  }

  //追加書き込みモードでファイルを開く
  fp = fopen(DEBUG_FILE_NAME, "a");
  if(fp == NULL) { MessageBox(NULL, "ログの書き込み失敗", NULL, NULL); return; }

  //フォーマット指定子から書き込む文字列の生成
  //バッファオーバーフローの可能性を減らすため大き目に配列を確保
  char string[0xffff];
  va_list list;
  va_start(list, fmt);
  vsprintf(string, fmt, list);
  va_end(list);

  //ファイルへの追加書き込み
  fputs("\n", fp);
  fputs(string, fp);

  //ファイルを閉じる
  fclose(fp);
  g_writeOutLogCallNum++;
}
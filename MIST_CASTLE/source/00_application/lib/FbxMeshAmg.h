#pragma once
#include <d3d9.h>
#include <d3dx9.h>
#include <map>
#include <fbxsdk.h>
#include "myDx9Lib.h"

class FBXMESHAMG
{
protected:
  // 固定シェーダー用材質
  D3DMATERIAL9 material;

  // 頂点構造体（シェーダーと一致）
  struct PolygonVertex
  {
    float x, y, z, w;     // 座標
    float wx, wy, wz, ww; // 重み(ダミー)
    float tu, tv;         // UV
    float nx, ny, nz;     // 法線
    float tx, ty, tz;     // 接線
    float bx, by, bz;     // 従法線
  };

  int NumMesh;
  int NumVertices;            //全頂点数
  //int NumFaces;             //全ポリゴン数
  PolygonVertex* Vertices;    //頂点
  PolygonVertex* VerticesSrc; //頂点元データ
  DWORD* Indices;             //三角形（頂点結び方）

  char FBXDir[128];
  int* MeshMaterial;
  int* MaterialFaces;
  IDirect3DTexture9** Textures;
  IDirect3DTexture9** NTextures;

  //	ボーン関連
  struct BONE
  {
    char	Name[128];
    D3DXMATRIX	OffsetMatrix;
    D3DXMATRIX	transform;
  };
  int NumBone;
  BONE Bone[256];

  int FindBone(const char* name);
  void LoadBone(FbxMesh* mesh);
  void LoadMeshAnim(FbxMesh* mesh);

  //	ウェイト関連
  struct WEIGHT
  {
    int count;
    int bone[4];
    float weight[4];
  };
  WEIGHT* Weights;

  int StartFrame;
  void Skinning(ANIMATION_DATA* animationData);	// ボーンによる変形

  static const int MOTION_MAX = 256;
  static const int BONE_MAX = 256;

  //	アニメーション
  struct Motion
  {
    int NumFrame;	// フレーム数	
    //D3DXMATRIX key[BONE_MAX][120];	// キーフレーム
    D3DXMATRIX* key[BONE_MAX];	// キーフレーム
  };
  // int MotionNo;

  int NumMotion;		// モーション数
  std::map<std::string, Motion> motion;	// モーションデータ
  void LoadKeyFrames(std::string name, int bone, FbxNode* bone_node);

  void Load(IDirect3DDevice9 *d3d_device, const char* filename);
  void LoadMaterial(IDirect3DDevice9 *d3d_device, int index, FbxSurfaceMaterial* material);

  void LoadPosition(FbxMesh* mesh);
  void LoadNormal(FbxMesh* mesh);
  void LoadUV(FbxMesh* mesh);
  void calcTangentBinormal(FbxMesh* mesh);
  void CalcTangentAndBinormal(D3DXVECTOR3* p0, D3DXVECTOR2* uv0,
                              D3DXVECTOR3* p1, D3DXVECTOR2* uv1,
                              D3DXVECTOR3* p2, D3DXVECTOR2* uv2,
                              D3DXVECTOR3* outTangent, D3DXVECTOR3* outBinormal);
  void LoadVertexColor(FbxMesh* mesh);

  void OptimizeVertices();

public:
  FBXMESHAMG();
  virtual ~FBXMESHAMG();

  void Update();

  void Render(ANIMATION_DATA* animationData, IDirect3DDevice9* d3d_device, ID3DXEffect* shaderEffect, LPDIRECT3DVERTEXDECLARATION9 VertexDeclaration, D3DXMATRIX matWorld, D3DXMATRIX matRotation, bool isShade);	//	描画
  static void PushRenderStack(IDirect3DDevice9* d3d_device, ID3DXEffect* shaderEffect, LPDIRECT3DVERTEXDECLARATION9 VertexDeclaration, LPDIRECT3DTEXTURE9 texture, LPDIRECT3DTEXTURE9 normalTex, D3DXMATRIX worldMatrix, D3DXMATRIX rotationMatrix, bool isShade, int numVertices, PolygonVertex* pVertices, DWORD* pIndices);
  static void RenderStack(bool isShade, IDirect3DDevice9* d3d_device, ID3DXEffect* shaderEffect, LPDIRECT3DVERTEXDECLARATION9 VertexDeclaration);

  void Play(ANIMATION_DATA* animationData, std::string name)
  {
    if(animationData == NULL)
      return;
    animationData->motionName = (char*)name.c_str();
    animationData->frame = 0.0f;
  }
  void Animate(ANIMATION_DATA* animationData, float sec);	// モーション再生

  void Create(IDirect3DDevice9 *d3d_device, const char* filename);
  void AddMotion(std::string name, const char* filename);
  //	pos_から(pos_+vec)にレイを発射 交点=out 面法線=normal
  //	当たらなければ-1がかえる
  int Raycast(const D3DXVECTOR3& pos, const D3DXVECTOR3& vec, float Dist, D3DXVECTOR3& out, D3DXVECTOR3& normal);

  //	モーション情報
  float Frame;              // 現在のフレーム
  std::string MotionName;   // 現在のモーション

  ////	姿勢情報
  //D3DXMATRIX transform;
  //D3DXVECTOR3 position;
  //D3DXVECTOR3 rotation;
  //D3DXVECTOR3 scale_;
  //	ボーン行列取得
  D3DXMATRIX GetBoneMatrix(int bone) { return Bone[bone].transform; }
};

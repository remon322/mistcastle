#pragma once

#include <d3dx9.h>

#define PI (3.14159f)
#define ANIMATION_ADVANCE_TIME (1.0f/60.0f)

//3D極座標ベクトル
struct POLAR_VECTOR
{
  float r;
  float theta_;
  float phi_;
};

//メッシュデータ関係
/*■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■*/

enum MESH_KIND
{
  MESH,           //アニメーションがないメッシュデータ
  ANIMATION_MESH, //アニメーションつきのメッシュデータ
  FBX_MESH,       //fbxファイルを読み込んだ時のメッシュデータ

  MESH_KIND_MAX,
};

//指定したXファイルを読み込む
//引数:Xファイルのディレクトリとファイル名
//    メッシュデータの種類(MESH_KIND列挙体を参照)
//戻り値:3Dデータのハンドル
void* LoadModel(LPCSTR fileDirectory,int meshKind);

//現在のhandleを破棄して指定したXファイルを読み込む
//handle        :上書きしたいメモリに入っているhandle
//fileDirectory :Xファイルのディレクトリ
//meshKind      :メッシュの種類(MESH_KINDを参照)
//animationData_:アニメーション情報
//戻り値:3Dデータのハンドル
void* ReloadXFile(void* handle, LPCSTR fileDirectory, int meshKind);

//FBXファイルのアニメーションをハンドルに読み込ませる
// controlName      : 制御用アニメーション名
// animationFileName: アニメーションデータ名(ディレクトリ)
// meshHandle       : 3Dデータのハンドル
void AddFbxAnimation(char* controlName, char* animationFileName, void* meshHandle);

//指定したハンドルのXファイルのメモリを解放する
void ReleaseXFile(void* handle);

//2Dテクスチャデータ関係
/*■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■*/

//2D用のテクスチャの読み込み
void* LoadTexture(char *fileDirectory);

//2D用の連番テクスチャの読み込み
void* LoadBlockTexture(char *fileDirectory, int nSplitX, int nSplitY);

//指定したハンドルのテクスチャファイルのメモリを解放する
void ReleaseTexture(void* handle);

//ウィンドウとDirectXの初期化
/*■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■*/

//以前に初期化されたすべてのオブジェクトを解放する
void Cleanup();

// Direct3Dの初期化
bool InitD3D(HWND hWnd);

//ウィンドウの大きさとウィンドウ名の決定
void SetWindowOption(int width, int height, char* name, bool isFullScreen);

//ウィンドウの作成
HWND InitWindow(HINSTANCE hInstance);

//生成したウィンドウの削除とメモリの解放
void DeleteWindow(HINSTANCE hInstance);

// シェーダーのエフェクトインターフェースを生成する
bool MakeShaderEffects();

//3Dのデバイスの取得
LPDIRECT3DDEVICE9 GetDevice();

//shaderの使用頂点情報の取得
LPD3DVERTEXELEMENT9 GetVertexElement();

//スキンアニメーション関係
/*■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■*/

//アニメーションに必要な情報
struct ANIMATION_DATA
{
  LPD3DXANIMATIONCONTROLLER pAnimController_;     //アニメーションコントローラオブジェクト
  LPD3DXANIMATIONSET*       ppAnimSet_;           //アニメーションセット
  int                       animNumMax_;          //アニメーション数

  float                     animSpeed;           //アニメーションスピード
  float                     nowTime;             //現在のアニメーション時間
  float                     prevTime;            //前回のアニメーション時間
  float                     loopRate;            //現在の１ループにかかる時間の倍率
  float                     prevLoopRate;        //前回の１ループにかかる時間の倍率
  float                     loopEndTime;         //１ループが終了するときの時間
  int                       shiftFrame;          //アニメーション切り替えフレーム数
  int                       endFrame;            //アニメーション終了フレーム数
  int                       animID;              //現在のアニメーションID
  int                       prevAnimID;          //前回のアニメーションID

  char*                     motionName;          //モーション名
  float                     frame;               //フレーム数
};

//メッシュデータからアニメーションデータをコピーする
//meshHandle : 任意のモデルデータのハンドル
//返り値      : コピーしたアニメーション情報
ANIMATION_DATA GetAnimationData(void* meshHandle);

//任意の番号のアニメーショントラックを有効にする
//animationData_ : アニメーション情報
//animID        : 切り替え後のアニメーション番号
//shiftFrame    : 切り替えにかける時間
//loopRate      : アニメーション速度倍率
void ChangeAnimation(ANIMATION_DATA* animationData, int animID, int shiftFrame, float loopRate);

//FBXデータのアニメーション切り替え
//animationData_ : アニメーション管理データ
//controlName : アニメーション管理名
//meshHandle  : メッシュデータハンドル
void ChangeFbxAnimation(ANIMATION_DATA* animationData, char* controlName, void* meshHandle);

//アニメーションを更新する
//animationData_ : アニメーション情報
//戻り値         : アニメーションのループ回数
int UpdateAnimation(ANIMATION_DATA* animationData);

//FBXデータのアニメーション更新
//animationData_ : アニメーション管理データ
//meshHandle  : メッシュデータハンドル
//rate        : 再生速度倍率
void UpdateFbxAnimation(ANIMATION_DATA* animationData, void* meshHandle, float rate);

//描画関係
/*■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■*/

//フォグの開始距離と完全濃くなる距離
void SetFogDistance(float start, float end);

//描画の開始と画面のリセット
bool BeginRender();

//描画の終了
void EndRender();

//光源をセットする colorはrgbaの順
void PushLightSource(D3DXVECTOR3 lightPos, D3DXVECTOR4 lightColor, float lightRange, float distanceCamera);

//光源設定
void SetupLights();

//プロジェクション行列の設定
void SetupMatrices(D3DXVECTOR3 eye,
                   D3DXVECTOR3 lookAt,
                   D3DXVECTOR3 upVec);

//3Dモデルの描画
//引数 x_,y_,z_      : ワールド座標のx_座標,y_座標,z_座標
//    scaleX,Y,Z : オブジェクトの拡縮倍率
//    yawAngle_   : y_軸を回転中心軸とした回転角度
//    meshHandle : 任意のメッシュデータのハンドル
//    animationData_ : アニメーション情報
//    isShade_    :　陰の付与(true時陰をつける)
void Render3D(float x, float y, float z, float scaleX, float scaleY, float scaleZ, float yawAngle, void* meshHandle, ANIMATION_DATA* animationData, bool isShade);

// 描画文字列のフォントサイズの設定
void SetFontSize(int size);

// 文字列の描画
// x_        : 描画する左上座標X
// y_        : 描画する左上座標Y
// szString : 描画する文字列
// color_    : 文字色(16進2桁ずつでr,g,b,aの順)
void DrawString(int x, int y, char *szString, int color);

// printf()のようにフォーマット指定子を利用し DrawString()を使う関数
void DrawPrintf(int x, int y, int nColor, const char *fmt, ...);

//ボックスを描画する(ほぼデバッグ用)
//pos_を中心にxyzSizeのボックスを生成する
void RenderBox(D3DXVECTOR3 pos, float xSize, float ySize, float zSize, float yawAngle, int nColor);

//球体を描画する(ほぼデバッグ用)
void RenderSphere(D3DXVECTOR3 pos, float r, int nColor);

//円柱を描画する(ほぼデバッグ用)
void RenderCylinder(D3DXVECTOR3 pos, float r, float length, int nColor);

//扇形柱を描画する(ほぼデバッグ用)
void RenderFanPillar(D3DXVECTOR3 pos, float r, float length, float fanAngle, D3DXVECTOR3 dir, int nColor);

//レイの描画
void RenderRay(D3DXVECTOR3 startPos, D3DXVECTOR3 endPos, int color);

// 2D画像の描画(画像の大きさそのまま)
void RenderTexture(int x, int y, void* textureHandle, int alpha);

// 2D画像の描画(右と下から削る)
void RenderTextureShave(int x, int y, void* textureHandle, int alpha, float shaveRateX, float shaveRateY);

// 2D連番画像の描画(画像の大きさそのまま)
void RenderBlockTexture(int x, int y, void* textureHandle, int alpha, int num);

// 2Dのボックスの描画
void Render2DBox(int x, int y, int sizeX, int sizeY, int color);

// 2D画像のビルボード描画(画像の大きさそのまま)
void RenderBillboard(D3DXVECTOR3 pos, D3DXVECTOR3 targetPos, void* textureHandle, int alpha);

// 2D画像のビルボード回転描画(画像の大きさそのまま)
void RenderBillboardRot(D3DXVECTOR3 pos, D3DXVECTOR3 targetPos, float rotAngle, void* textureHandle, int alpha);

// 2D画像のビルボード連番回転描画(画像の大きさそのまま)
void RenderBlockBillboardRot(D3DXVECTOR3 pos, D3DXVECTOR3 targetPos, float rotAngle, void* textureHandle, int alpha, int num);

// 2D画像のビルボード座標ずらし連番回転描画(画像の大きさそのまま)
void RenderBlockBillboardShiftRot(D3DXVECTOR3 centerPos, D3DXVECTOR3 shiftPos, D3DXVECTOR3 targetPos, float rotAngle, void* textureHandle, int alpha, int num);

//入力関係
/*■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■*/

//キーボードの入力情報
struct KEY_BOARD
{
  bool esc : 1;
  bool num0 : 1;
  bool num1 : 1;
  bool num2 : 1;
  bool num3 : 1;
  bool num4 : 1;
  bool num5 : 1;
  bool num6 : 1;
  bool num7 : 1;
  bool num8 : 1;
  bool num9 : 1;
  bool numPad0 : 1;
  bool numPad1 : 1;
  bool numPad2 : 1;
  bool numPad3 : 1;
  bool numPad4 : 1;
  bool numPad5 : 1;
  bool numPad6 : 1;
  bool numPad7 : 1;
  bool numPad8 : 1;
  bool numPad9 : 1;
  bool plus  : 1;
  bool minus : 1;
  bool slash : 1;
  bool asterisk : 1;
  bool tab : 1;
  bool a : 1;
  bool b : 1;
  bool c : 1;
  bool d : 1;
  bool e : 1;
  bool f : 1;
  bool g : 1;
  bool h : 1;
  bool i : 1;
  bool j : 1;
  bool k : 1;
  bool l : 1;
  bool n : 1;
  bool m : 1;
  bool o : 1;
  bool p : 1;
  bool q : 1;
  bool r : 1;
  bool s : 1;
  bool t : 1;
  bool u : 1;
  bool v : 1;
  bool w : 1;
  bool x : 1;
  bool y : 1;
  bool z : 1;
  bool rEnter : 1;
  bool lCtrl : 1;
  bool lShift : 1;
  bool space : 1;
  bool alt : 1;
  bool f1 : 1;
  bool f2 : 1;
  bool f3 : 1;
  bool f4 : 1;
  bool f5 : 1;
  bool f6 : 1;
  bool f7 : 1;
  bool f8 : 1;
  bool f9 : 1;
  bool f10 : 1;
  bool f11 : 1;
  bool f12 : 1;
  bool up : 1;
  bool down : 1;
  bool left : 1;
  bool right : 1;
};

//マウスの入力情報
struct MOUSE
{
  float x;
  float y;
  float z;
  bool button[8];
};

//ゲームパッドの入力情報
struct JOY_STICK
{
  float leftStickX;
  float leftStickY;
  float z;
  float rightStickX;
  float rightStickY;
  bool up : 1;
  bool down : 1;
  bool left : 1;
  bool right : 1;
  bool button[24];
};

//ダイレクトインプットの初期化関数
bool InitDinput(HWND hWnd);

//キーデバイスの再取得とキー情報の更新
KEY_BOARD GetKeyBoardState();

//マウスデバイスの再取得とキー情報の更新
//返り値のxyzはそれぞれ相対位置なので増加値となる
MOUSE GetMouseState();

//ゲームパッドデバイスの再取得とキー情報の更新
JOY_STICK GetJoyStickState();

//あたり判定
/*■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■*/

enum POS_TYPE
{
  POS_TYPE_BOTTOM,
  POS_TYPE_CENTER,
};

//あたり判定用のボックス
struct BBOX
{
  int posType;
  D3DXVECTOR3 vMax;
  D3DXVECTOR3 vMin;
  D3DXVECTOR3 vAxisX;
  D3DXVECTOR3 vAxisY;
  D3DXVECTOR3 vAxisZ;
  float fLengthX;
  float fLengthY;
  float fLengthZ;
};

//ボックスの計算およびボックスを視認可能にするためにボックスメッシュを作成する
bool InitBBox(void* meshHandle, int posType);

//ボックスの手動作成
bool InitBBoxManual(void* meshHandle, float sizeX, float sizeY, float sizeZ, int posType);

//メッシュデータからバウンディングボックスを取得
BBOX GetBBox(void* meshHandle, D3DXVECTOR3 scale);

//OBBによる衝突判定。衝突している場合はtrue、離れている場合はfalse
bool OBBCollisionDetection(D3DXVECTOR3 pos_A, float yawAngle_A, BBOX* BBox_A, D3DXVECTOR3 pos_B, float yawAngle_B, BBOX* BBox_B);

//レイによる衝突判定　レイが相手メッシュと交差する場合は、pfDistanceに距離を入れてtrueを返す
bool RayIntersect(float* pfDistance, int* dwIndex, D3DXVECTOR3* interPos, D3DXVECTOR3* normal, D3DXVECTOR3 rayStartPos, D3DXVECTOR3 rayVector, D3DXVECTOR3 pos_B, void* meshHandle_B, float objYawAngle, D3DXVECTOR3 scale);

//レイによる衝突判定　レイが相手メッシュと交差する場合は、pfDistanceに距離を、pvNormalに衝突面の法線を入れてtrueを返す
//rayStartPos  :レイの始まり地点の座標
//rayDir       :レイの向き
//meshHandle_B :対象のメッシュデータのハンドル
//yawAngle_B   :対象のy_軸基準の回転角度
//pos_B        :対象の基準座標
//scale_B      :対象の大きさ
//pfDistance   :レイの開始から交差地点までの距離を返す
//pvNormal     :レイが当たったポリゴンの法線ベクトル
//返り値 衝突した場合true
bool Collide(D3DXVECTOR3 rayStartPos, D3DXVECTOR3 rayDir, void* meshHandle_B, float yawAngle_B, D3DXVECTOR3 pos_B, D3DXVECTOR3 scale_B, float* pfDistance, D3DXVECTOR3* pvNormal);

//特定ポリゴンに対する反射各を求める
//meshHandle         : 反射物のメッシュデータハンドル
//index              : 反射物の反射するポリゴンの指数
//vIncidentDirection : 入射角のベクトル
D3DXVECTOR3 Bounce(void* meshHandle, int index, D3DXVECTOR3 vIncidentDirection);

//ポリゴンに対するすべり方向を求める
//L : 入射ベクトル（レイ）
//N : ポリゴンの法線
D3DXVECTOR3 Slip(D3DXVECTOR3 L, D3DXVECTOR3 N);

//ログの書き出し
/*■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■*/

//指定ファイル名にログを書き出す
void WriteOutLog(const char *fmt, ...);